/**
 * © Copyright Lewis Maitland 2015
 * This file is part of Toco.
 * 
 * Toco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Toco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Toco.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.toco;

public class Constants
{
    public enum DeviceType {
        DESKTOP, TABLET, MOBILE
    }

    public enum UserType {
       USER, ADMIN
    }

    /**
     * These define the URL parmaeters used in the route definition strings (e.g. '{userId}').
     */
    public class Url
    {
        public static final String GROUP_ID         = "groupId";
        public static final String APP_ID           = "appId";
        public static final String USER_ID          = "userId";
        public static final String DEVICE_ID        = "deviceId";
        public static final String MOVIE_ID         = "movieId";
        public static final String SONG_ID          = "songId";
        public static final String PICTURE_ID       = "pictureId";
        public static final String META_ID          = "metaId";
        public static final String META_TYPE        = "metaType";
        public static final String ALBUM_ID         = "albumId";
    }

    public class Validation {
        public static final String MAC_REGEX        = "([a-f0-9]{12})";
        public static final String MAC_ERROR        = "invalid MAC address";
    }

    /**
     * These define the route names used in naming each route definitions.  These names are used
     * to retrieve URL patterns within the controllers by name to create links in responses.
     */
    public class Routes
    {
        public static final String APP_SINGLE         = "apps.single";
        public static final String APP_COLLECTION     = "apps.collection";

        public static final String USER_SINGLE        = "users.single";
        public static final String USER_COLLECTION    = "users.collection";

        public static final String DEVICE_SINGLE      = "devices.single";
        public static final String DEVICE_COLLECTION  = "devices.collection";

        public static final String GROUP_SINGLE       = "groups.single";
        public static final String GROUP_COLLECTION   = "groups.collection";

        public static final String CONTENT_SINGLE     = "content.single";
        public static final String CONTENT_COLLECTION = "content.collection";

        public static final String META_SINGLE        = "meta.single";
    }

    public class UPnP {
        public static final String TOCO_CREATOR       = "toco";
    }

    public class Database {
        public static final String NAME               = "toco.db";
        public static final String TEST_NAME          = "test.db";
    }
}
