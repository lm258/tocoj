/**
 * © Copyright Lewis Maitland 2015
 * This file is part of Toco.
 * 
 * Toco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Toco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Toco.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.toco;

import static org.jboss.netty.handler.codec.http.HttpHeaders.Names.ACCEPT;
import static org.jboss.netty.handler.codec.http.HttpHeaders.Names.AUTHORIZATION;
import static org.jboss.netty.handler.codec.http.HttpHeaders.Names.CONTENT_TYPE;
import static org.jboss.netty.handler.codec.http.HttpHeaders.Names.LOCATION;
import static org.jboss.netty.handler.codec.http.HttpHeaders.Names.REFERER;
import static org.restexpress.Flags.Auth.PUBLIC_ROUTE;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.concurrent.TimeUnit;
import java.net.SocketAddress;

import org.jboss.netty.channel.Channel;

import org.restexpress.Flags;
import org.restexpress.RestExpress;
import org.restexpress.exception.BadRequestException;
import org.restexpress.exception.ConflictException;
import org.restexpress.exception.NotFoundException;
import org.restexpress.pipeline.SimpleConsoleLogMessageObserver;

import org.toco.config.Configuration;
import org.toco.serialization.SerializationProvider;

import org.restexpress.util.Environment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.strategicgains.repoexpress.exception.DuplicateItemException;
import com.strategicgains.repoexpress.exception.InvalidObjectIdException;
import com.strategicgains.repoexpress.exception.ItemNotFoundException;
import com.strategicgains.restexpress.plugin.cache.CacheControlPlugin;
import com.strategicgains.restexpress.plugin.cors.CorsHeaderPlugin;
import com.strategicgains.restexpress.plugin.swagger.SwaggerPlugin;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class Main
{
    private static final String SERVICE_NAME = "Toco Media Server";
    private static final Logger LOG = LoggerFactory.getLogger(SERVICE_NAME);

    public static void main(String[] args) throws Exception
    {
        RestExpress server = initializeServer(args);
        server.awaitShutdown();
    }

    public static RestExpress initializeServer(String[] args) throws IOException
    {
        RestExpress.setSerializationProvider(new SerializationProvider());

        Configuration config = loadEnvironment(args);
        RestExpress server = new RestExpress()
                .setName(SERVICE_NAME)
                .setBaseUrl(config.getBaseUrl())
                .setExecutorThreadCount(config.getExecutorThreadPoolSize())
                .addMessageObserver(new SimpleConsoleLogMessageObserver());

        Routes.define(config, server);
        configurePlugins(config, server);
        mapExceptions(server);
        server.bind(config.getPort());
        return server;
    }

    private static void configurePlugins(Configuration config,
        RestExpress server)
    {
        new SwaggerPlugin()
            .flag(Flags.Auth.PUBLIC_ROUTE)
            .register(server);

        new CacheControlPlugin()
                .register(server);

        new CorsHeaderPlugin("*")
            .flag(PUBLIC_ROUTE)
            .allowHeaders(CONTENT_TYPE, ACCEPT, AUTHORIZATION, REFERER, LOCATION)
            .exposeHeaders(LOCATION)
            .register(server);
    }

    private static void mapExceptions(RestExpress server)
    {
        server
            .mapException(ItemNotFoundException.class,    NotFoundException.class)
            .mapException(DuplicateItemException.class,   ConflictException.class)
            .mapException(InvalidObjectIdException.class, BadRequestException.class);
    }

    private static Configuration loadEnvironment(String[] args)
    throws FileNotFoundException, IOException
    {
        if (args.length > 0) {
            return Environment.from(args[0], Configuration.class);
        }

        return Environment.fromDefault(Configuration.class);
    }
}
