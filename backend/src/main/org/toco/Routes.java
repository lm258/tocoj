/**
 * © Copyright Lewis Maitland 2015
 * This file is part of Toco.
 * 
 * Toco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Toco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Toco.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.toco;

import org.jboss.netty.handler.codec.http.HttpMethod;
import org.restexpress.RestExpress;
import org.toco.config.Configuration;

public abstract class Routes
{
    public static void define(Configuration config, RestExpress server)
    {
        /**
         * DEVICES ROUTES
         * Routes here are defined for all devices controller REST handling
        */
        server.uri("/api/devices/index.{format}", config.getDeviceController())
            .action("readAll", HttpMethod.GET)
            .method(HttpMethod.POST)
            .name(Constants.Routes.APP_COLLECTION);

        server.uri("/api/devices/{deviceId}.{format}", config.getDeviceController())
            .method(HttpMethod.GET, HttpMethod.PUT, HttpMethod.DELETE)
            .name(Constants.Routes.APP_SINGLE);

        /**
         * APPS ROUTES
         * Routes here are defined for all apps controller REST handling
        */
        server.uri("/api/apps/index.{format}", config.getAppController())
            .action("readAll", HttpMethod.GET)
            .method(HttpMethod.POST)
            .name(Constants.Routes.APP_COLLECTION);

        server.uri("/api/apps/{appId}.{format}", config.getAppController())
            .method(HttpMethod.GET, HttpMethod.PUT, HttpMethod.DELETE)
            .name(Constants.Routes.APP_SINGLE);

        /**
         * USERS ROUTES
         * Routes here are defined for all users controller REST handling
        */
        server.uri("/api/users/index.{format}", config.getUserController())
            .action("readAll", HttpMethod.GET)
            .method(HttpMethod.POST)
            .name(Constants.Routes.USER_COLLECTION);

        server.uri("/api/users/{userId}.{format}", config.getUserController())
            .method(HttpMethod.GET, HttpMethod.PUT, HttpMethod.DELETE)
            .name(Constants.Routes.USER_SINGLE);

        /**
         * GROUPS ROUTES
         * Routes here are defined for all groups controller REST handling
        */
        server.uri("/api/groups/index.{format}", config.getGroupController())
            .action("readAll", HttpMethod.GET)
            .method(HttpMethod.POST)
            .name(Constants.Routes.GROUP_COLLECTION);

        server.uri("/api/groups/{groupId}.{format}", config.getGroupController())
            .method(HttpMethod.GET, HttpMethod.PUT, HttpMethod.DELETE)
            .name(Constants.Routes.GROUP_SINGLE);

        /**
         * MOVIES ROUTES
         * Routes here are defined for all movies controller REST handling
        */
        server.uri("/api/movies/index.{format}", config.getMovieController())
            .action("readAll", HttpMethod.GET)
            .method(HttpMethod.POST)
            .name(Constants.Routes.CONTENT_COLLECTION);

        server.uri("/api/movies/{movieId}.{format}", config.getMovieController())
            .method(HttpMethod.GET, HttpMethod.PUT, HttpMethod.DELETE)
            .name(Constants.Routes.CONTENT_SINGLE);

        /**
         * MUSIC ROUTES
         * Routes here are defined for all music controller REST handling
        */
        server.uri("/api/music/index.{format}", config.getMusicController())
            .action("readAll", HttpMethod.GET)
            .method(HttpMethod.POST)
            .name(Constants.Routes.CONTENT_COLLECTION);

        server.uri("/api/music/{songId}.{format}", config.getMusicController())
            .method(HttpMethod.GET, HttpMethod.PUT, HttpMethod.DELETE)
            .name(Constants.Routes.CONTENT_SINGLE);

        server.uri("/api/music/albums/index.{format}", config.getMusicController())
            .action("readAllAlbum", HttpMethod.GET)
            .name(Constants.Routes.CONTENT_COLLECTION);

        server.uri("/api/music/albums/{albumId}.{format}", config.getMusicController())
            .action("readAlbum", HttpMethod.GET)
            .name(Constants.Routes.CONTENT_COLLECTION);

        /**
         * PICTURES ROUTES
         * Routes here are defined for all picture controller REST handling
        */
        server.uri("/api/pictures/index.{format}", config.getPictureController())
            .action("readAll", HttpMethod.GET)
            .method(HttpMethod.POST)
            .name(Constants.Routes.CONTENT_COLLECTION);

        server.uri("/api/pictures/{pictureId}.{format}", config.getPictureController())
            .method(HttpMethod.GET, HttpMethod.PUT, HttpMethod.DELETE)
            .name(Constants.Routes.CONTENT_SINGLE);

        server.uri("/api/pictures/albums/index.{format}", config.getPictureController())
            .action("readAllAlbum", HttpMethod.GET)
            .name(Constants.Routes.CONTENT_COLLECTION);

        server.uri("/api/pictures/albums/{albumId}.{format}", config.getPictureController())
            .action("readAlbum", HttpMethod.GET)
            .name(Constants.Routes.CONTENT_COLLECTION);

        server.uri("/api/meta/{metaType}/{metaId}.{format}", config.getMetaController())
            .method(HttpMethod.GET, HttpMethod.PUT, HttpMethod.DELETE)
            .name(Constants.Routes.META_SINGLE);

        /*Regex route used to handle default webserver behaviour*/
        server.regex("(/web[a-zA-Z/.0-9-?=&]*)|(/)", config.getStaticContentController())
            .noSerialization();
    }
}
