/**
 * © Copyright Lewis Maitland 2015
 * This file is part of Toco.
 * 
 * Toco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Toco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Toco.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.toco;

/**
 * Sanitize class is used for sanitization of various
 * different types of input data.
 * @author Lewis Maitland
*/
public class Sanitize {

    /**
     * This function is used for filtering mac addresses.
     * All characters except for valid mac address (except delimiters)
     * are removed and lower cased.
    */
    public static String mac( String mac ) {
        return (mac == null) ? null : mac.toLowerCase().replaceAll("[^a-f0-9]*", "");
    }
}