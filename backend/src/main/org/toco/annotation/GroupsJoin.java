package org.toco.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Groups annotation is used for settings specific
 * grouping information for content and users.
*/
@Retention(RetentionPolicy.RUNTIME)
public @interface GroupsJoin {
    public String foreignKey();
    public Class  joinClass();
}