/**
 * © Copyright Lewis Maitland 2015
 * This file is part of Toco.
 * 
 * Toco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Toco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Toco.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.toco.config;

import java.util.Properties;
import org.restexpress.RestExpress;

import org.toco.controller.*;
import org.toco.persistence.*;
import org.toco.service.*;
import org.fourthline.cling.model.NetworkAddress;
import java.util.*;

import org.restexpress.util.Environment;
import java.sql.Connection;
import java.sql.DriverManager;
import org.sormula.Database;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.toco.Constants;

import org.toco.mediaserver.MediaServer;
import org.fourthline.cling.UpnpService;
import org.fourthline.cling.UpnpServiceImpl;

public class Configuration
extends Environment
{
    private static final String SERVICE_NAME = "Configuration: ";
    private static final Logger LOG = LoggerFactory.getLogger(SERVICE_NAME);


    private static final String DEFAULT_EXECUTOR_THREAD_POOL_SIZE = "20";
    private static final String PORT_PROPERTY = "port";
    private static final String BASE_URL_PROPERTY = "base.url";
    private static final String EXECUTOR_THREAD_POOL_SIZE = "executor.threadPool.size";
    private static final String DATABASE_PROPERTY = "database";

    private int port;
    private String baseUrl;
    private int executorThreadPoolSize;
    private String webDirectory;
    private String databaseName;

    private StaticContentController staticContentController;
    private AppController appController;
    private UserController userController;
    private DeviceController deviceController;
    private GroupController groupController;
    private MovieController movieController;
    private MusicController musicController;
    private PictureController pictureController;
    private MetaController metaController;

    public static String BASE_URL = "http://localhost:8080";

    //Exception added here
    @Override
    protected void fillValues(Properties p)
    {
        this.port = Integer.parseInt(p.getProperty(PORT_PROPERTY, String.valueOf(RestExpress.DEFAULT_PORT)));
        this.databaseName = p.getProperty(DATABASE_PROPERTY, Constants.Database.NAME);
        this.baseUrl = p.getProperty(BASE_URL_PROPERTY, "http://localhost:" + String.valueOf(port));
        this.executorThreadPoolSize = Integer.parseInt(p.getProperty(EXECUTOR_THREAD_POOL_SIZE, DEFAULT_EXECUTOR_THREAD_POOL_SIZE));
        initialize();
    }

    //Exception added here
    private void initialize()
    {
        Database database = null;

        try {            
            Class.forName("org.sqlite.JDBC");
            Connection connection = DriverManager.getConnection("jdbc:sqlite:" + databaseName);
            database = new Database( connection );

            AppRepository appRepository = new AppRepository( database );
            AppService appService = new AppService( appRepository );
            appController = new AppController( appService );

            DeviceRepository deviceRepository = new DeviceRepository( database );
            DeviceService deviceService = new DeviceService( deviceRepository );
            deviceController = new DeviceController( deviceService );

            GroupRepository groupRepository = new GroupRepository( database );
            GroupService groupService = new GroupService( groupRepository );
            groupController = new GroupController( groupService );

            UserRepository userRepository = new UserRepository( database );
            UserService userService = new UserService( userRepository );
            userController = new UserController( userService );

            SongRepository musicRepository = new SongRepository( database );
            SongService musicService = new SongService( musicRepository );
            MusicAlbumRepository musicAlbumRepository = new MusicAlbumRepository( database );
            MusicAlbumService musicAlbumService = new MusicAlbumService( musicAlbumRepository );
            musicController = new MusicController( musicService, musicAlbumService );

            MovieRepository movieRepository = new MovieRepository( database );
            MovieService movieService = new MovieService( movieRepository );
            movieController = new MovieController( movieService );

            PictureRepository pictureRepository = new PictureRepository( database );
            PictureService pictureService = new PictureService( pictureRepository );
            PictureAlbumRepository pictureAlbumRepository = new PictureAlbumRepository( database );
            PictureAlbumService pictureAlbumService = new PictureAlbumService( pictureAlbumRepository );
            pictureController = new PictureController( pictureService, pictureAlbumService );

            Map<String, SQLRepository> handlerMap = new HashMap<String, SQLRepository>();
            handlerMap.put( "movie",    movieRepository );
            handlerMap.put( "picture" , pictureRepository );
            handlerMap.put( "song" ,    musicRepository );

            staticContentController = new StaticContentController(
                handlerMap,
                database
            );

            MediaServer mediaServer = new MediaServer();
            final UpnpService upnpService = new UpnpServiceImpl();
            Runtime.getRuntime().addShutdownHook(
                new Thread() {
                    @Override
                    public void run() {
                        upnpService.shutdown();
                    }
                }
            );
            List<NetworkAddress> interfaces = upnpService.getRouter().getActiveStreamServers(null);
            BASE_URL = "http://" + interfaces.get(0).getAddress().getHostAddress() + ":" + this.port + "/";
            upnpService.getRegistry().addDevice(mediaServer.createDevice( database ));

        }catch( Exception e ){
            /*We Need to exit the program if we couldnt start/connect to the database*/
            LOG.error("Exception: " + e.getMessage() );
            System.exit(0);
        }
        metaController = new MetaController();
    }

    public int getPort()
    {
        return port;
    }
    
    public String getBaseUrl()
    {
        return baseUrl;
    }
    
    public int getExecutorThreadPoolSize()
    {
        return executorThreadPoolSize;
    }

    public StaticContentController getStaticContentController() {
        return staticContentController;
    }

    public AppController getAppController() {
        return appController;
    }

    public UserController getUserController() {
        return userController;
    }

    public DeviceController getDeviceController() {
        return deviceController;
    }

    public GroupController getGroupController() {
        return groupController;
    }

    public MovieController getMovieController() {
        return movieController;
    }

    public MusicController getMusicController() {
        return musicController;
    }

    public PictureController getPictureController() {
        return pictureController;
    }

    public MetaController getMetaController() {
        return metaController;
    }
}
