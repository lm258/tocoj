/**
 * © Copyright Lewis Maitland 2015
 * This file is part of Toco.
 * 
 * Toco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Toco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Toco.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.toco.controller;

import java.util.List;

import org.restexpress.Request;
import org.restexpress.Response;
import org.restexpress.common.query.QueryFilter;
import org.restexpress.common.query.QueryOrder;
import org.restexpress.common.query.QueryRange;
import org.restexpress.query.QueryFilters;
import org.restexpress.query.QueryOrders;
import org.restexpress.query.QueryRanges;

import org.toco.Constants;
import org.toco.domain.App;
import org.toco.service.AppService;

/**
 * AppController is used to provide CRUD functionality
 * to the REST API. Apps can be read singularly or in bulk
 * ONLY. Apps cannot be created/deleted or updated using
 * the REST API.
 * @author Lewis Maitland
 */
public class AppController
{
    private AppService service;

    /**
     * Default Constructor
     * @param AppService is the service used to provide database interaction for apps
    */
    public AppController(AppService appService) {
        super();
        this.service = appService;
    }

    /**
     * Read a single app from the database and then return that
     * app.
     * @param Request the client request to the system
     * @param Response response to be sent to client
     * @retrun App the app that was read
    */
    public App read(Request request, Response response) {
        String id  = request.getHeader( Constants.Url.APP_ID, "No App ID supplied" );
        App app    = service.read( Integer.parseInt(id) );

        return app;
    }

    /**
     * Read a all apps from the database and then return them
     * @param Request the client request to the system
     * @param Response response to be sent to client
     * @retrun List<App> of all apps based on query
    */
    public List<App> readAll(Request request, Response response) {
        QueryFilter filter = QueryFilters.parseFrom(request);
        QueryOrder  order  = QueryOrders.parseFrom(request);
        QueryRange  range  = QueryRanges.parseFrom(request, 20);
        List<App>   apps   = service.readAll(filter, range, order);

        long count = service.count(filter);
        response.setCollectionResponse(range, apps.size(), count);

        return apps;
    }

    /**
     * This is just a dummy function.
     * AppController does not allow for app manipulation through
     * its interface.
    */
    public void create(Request request, Response response) {
    }

    /**
     * This is just a dummy function.
     * AppController does not allow for app manipulation through
     * its interface.
    */
    public void update(Request request, Response response) {
    }

    /**
     * This is just a dummy function.
     * AppController does not allow for app manipulation through
     * its interface.
    */
    public void delete(Request request, Response response) {
    }
}
