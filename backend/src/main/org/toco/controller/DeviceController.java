/**
 * © Copyright Lewis Maitland 2015
 * This file is part of Toco.
 * 
 * Toco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Toco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Toco.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.toco.controller;

import java.util.List;

import org.jboss.netty.handler.codec.http.HttpMethod;
import org.restexpress.Request;
import org.restexpress.Response;
import org.restexpress.common.query.QueryFilter;
import org.restexpress.common.query.QueryOrder;
import org.restexpress.common.query.QueryRange;
import org.restexpress.query.QueryFilters;
import org.restexpress.query.QueryOrders;
import org.restexpress.query.QueryRanges;

import org.toco.Constants;
import org.toco.domain.Device;
import org.toco.service.DeviceService;

/**
 * DeviceController is used to provide CRUD functionality
 * to the REST API for devices. DeviceController can be 
 * read singularly or in bulk ONLY. Devices can be 
 * created/deleted also.
 * @author Lewis Maitland
 */
public class DeviceController
{
    private DeviceService service;

    /**
     * Default Constructor
     * @param DeviceService is the service used to provide database interaction for devices
    */
    public DeviceController(DeviceService deviceService) {
        super();
        this.service = deviceService;
    }

    /**
     * Read a single device from the database and then return that
     * device.
     * @param Request the client request to the system
     * @param Response response to be sent to client
     * @retrun Device the device that was read
    */
    public Device read(Request request, Response response) {
        String id  = request.getHeader( Constants.Url.DEVICE_ID, "No Device ID supplied" );
        Device device  = service.read( Integer.parseInt(id) );

        return device;
    }

    /**
     * Read a all devices from the database and then return them.
     * @param Request the client request to the system
     * @param Response response to be sent to client
     * @retrun List<Device> of all devices based on query
    */
    public List<Device> readAll(Request request, Response response) {
        QueryFilter filter = QueryFilters.parseFrom(request);
        QueryOrder  order  = QueryOrders.parseFrom(request);
        QueryRange  range  = QueryRanges.parseFrom(request, 20);
        List<Device>  devices  = service.readAll(filter, range, order);

        long count = service.count(filter);
        response.setCollectionResponse(range, devices.size(), count);

        return devices;
    }

    /**
     * Create a device in the database
     * @param Request request should contain device as payload
     * @param Response response to be sent to client
    */
    public Device create(Request request, Response response) {
        Device device  = request.getBodyAs(Device.class, "Device details not provided");
        Device saved = service.create(device);

        response.setResponseCreated();
        return saved;
    }

    /**
     * Update a device in the database
     * @param Request request should contain parameter deviceId and device as payload
     * @param Response response to be sent to client
    */
    public void update(Request request, Response response) {
        String id = request.getHeader(Constants.Url.DEVICE_ID, "No Device ID supplied");
        Device device = request.getBodyAs(Device.class, "Resource details not provided");

        device.setId( Integer.parseInt(id) );
        service.update(device);
        response.setResponseNoContent();
    }

    /**
     * Delete a device from the database
     * @param Request request should contain parameter deviceId
     * @param Response response to be sent to client
    */
    public void delete(Request request, Response response) {
        String id = request.getHeader(Constants.Url.DEVICE_ID, "No Device ID supplied");
        service.delete( Integer.parseInt(id) );
        response.setResponseNoContent();
    }
}
