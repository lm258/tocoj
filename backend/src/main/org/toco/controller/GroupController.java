/**
 * © Copyright Lewis Maitland 2015
 * This file is part of Toco.
 * 
 * Toco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Toco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Toco.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.toco.controller;

import java.util.List;

import org.jboss.netty.handler.codec.http.HttpMethod;
import org.restexpress.Request;
import org.restexpress.Response;
import org.restexpress.common.query.QueryFilter;
import org.restexpress.common.query.QueryOrder;
import org.restexpress.common.query.QueryRange;
import org.restexpress.query.QueryFilters;
import org.restexpress.query.QueryOrders;
import org.restexpress.query.QueryRanges;

import org.toco.Constants;
import org.toco.domain.Group;
import org.toco.service.GroupService;

/**
 * GroupController is used to provide CRUD operations
 * to the REST API for groups. Groups can be created
 * updated, read and deleted.
 * @author Lewis Maitland
 */
public class GroupController
{
    private GroupService service;

    /**
     * Default Constructor
     * @param GroupService is the service used to provide database interaction for groups
    */
    public GroupController(GroupService groupService)
    {
        super();
        this.service = groupService;
    }

    /**
     * Read a single group from the database and then return that
     * group.
     * @param Request the client request to the system
     * @param Response response to be sent to client
     * @retrun Group the group that was read
    */
    public Group read(Request request, Response response) {
        String id  = request.getHeader( Constants.Url.GROUP_ID, "No Group ID supplied" );
        Group group  = service.read( Integer.parseInt(id) );

        return group;
    }

    /**
     * Read a all groups from the database and then return them.
     * @param Request the client request to the system
     * @param Response response to be sent to client
     * @retrun List<Group> of all groups based on query
    */
    public List<Group> readAll(Request request, Response response) {
        QueryFilter filter = QueryFilters.parseFrom(request);
        QueryOrder  order  = QueryOrders.parseFrom(request);
        QueryRange  range  = QueryRanges.parseFrom(request, 20);
        List<Group>  groups  = service.readAll(filter, range, order);

        long count = service.count(filter);
        response.setCollectionResponse(range, groups.size(), count);

        return groups;
    }

    /**
     * Create a group in the database
     * @param Request request should contain group as payload
     * @param Response response to be sent to client
    */
    public Group create(Request request, Response response) {
        Group group  = request.getBodyAs(Group.class, "Group details not provided");
        Group saved = service.create(group);

        response.setResponseCreated();
        return saved;
    }

    /**
     * Update a group in the database
     * @param Request request should contain parameter groupId and group as payload
     * @param Response response to be sent to client
    */
    public void update(Request request, Response response) {
        String id = request.getHeader(Constants.Url.GROUP_ID, "No Group ID supplied");
        Group group = request.getBodyAs(Group.class, "Resource details not provided");

        group.setId( Integer.parseInt(id) );
        service.update(group);
        response.setResponseNoContent();
    }

    /**
     * Delete a group from the database
     * @param Request request should contain parameter groupId
     * @param Response response to be sent to client
    */
    public void delete(Request request, Response response) {
        String id = request.getHeader(Constants.Url.GROUP_ID, "No Group ID supplied");
        service.delete( Integer.parseInt(id) );
        response.setResponseNoContent();
    }
}
