package org.toco.controller;

import java.util.Map;
import java.util.HashMap;
import java.util.List;
import org.toco.meta.*;
import org.restexpress.Request;
import org.restexpress.Response;
import org.toco.domain.*;
import org.toco.Constants;


public class MetaController {

    Map<String, MetaHandler> metaHandlers = new HashMap<String, MetaHandler>();

    /**
     * Default Constructor
    */
    public MetaController() {
        metaHandlers.put( "movies", new MovieMetaHandler() );
    }

    /**
     * Read a peice of meta data from the correct data base
     * @param Request the client request to the system
     * @param Response response to be sent to client
     * @retrun Movie the movie that was read
    */
    public Object read(Request request, Response response) {
        String id    = request.getHeader( Constants.Url.META_ID,   "No Meta ID supplied" );
        String type  = request.getHeader( Constants.Url.META_TYPE, "No Meta Type supplied" );

        if( type.compareTo("movies") == 0 ) {
            Movie m = new Movie();
            metaHandlers.get("movies").populateObject( (Object)m, id );
            return m;
        } else {
            throw new RuntimeException( "Invalid Meta type supplied" );
        }
    }

    /**
     * This is just a dummy function.
     * MetaController does not allow for meta readall through
     * its interface.
     * NOTE: This may be implemented for 'bulk' meta assigning
    */
    public List<Object> readAll(Request request, Response response) {
        throw new RuntimeException( "Function not allowed for MetaController" );
    }

    /**
     * This is just a dummy function.
     * MetaController does not allow for meta manipulation through
     * its interface.
    */
    public Object create(Request request, Response response) {
        throw new RuntimeException( "Function not allowed for MetaController" );
    }

    /**
     * This is just a dummy function.
     * MetaController does not allow for meta manipulation through
     * its interface.
    */
    public void update(Request request, Response response) {
        throw new RuntimeException( "Function not allowed for MetaController" );
    }

    /**
     * This is just a dummy function.
     * MetaController does not allow for meta manipulation through
     * its interface.
    */
    public void delete(Request request, Response response) {
        throw new RuntimeException( "Function not allowed for MetaController" );
    }
}