/**
 * © Copyright Lewis Maitland 2015
 * This file is part of Toco.
 * 
 * Toco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Toco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Toco.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.toco.controller;

import java.util.List;

import org.jboss.netty.handler.codec.http.HttpMethod;
import org.restexpress.Request;
import org.restexpress.Response;
import org.restexpress.common.query.QueryFilter;
import org.restexpress.common.query.QueryOrder;
import org.restexpress.common.query.QueryRange;
import org.restexpress.query.QueryFilters;
import org.restexpress.query.QueryOrders;
import org.restexpress.query.QueryRanges;

import org.toco.Constants;
import org.toco.domain.Movie;
import org.toco.service.MovieService;

/**
 * 
 * @author Lewis Maitland
 */
public class MovieController
{
    private MovieService service;

    /**
     * Default Constructor
     * @param MovieService is the service used to provide database interaction for movies
    */
    public MovieController(MovieService movieService) {
        super();
        this.service = movieService;
    }

    /**
     * Read a single movie from the database and then return that
     * movie.
     * @param Request the client request to the system
     * @param Response response to be sent to client
     * @retrun Movie the movie that was read
    */
    public Movie read(Request request, Response response) {
        String id  = request.getHeader( Constants.Url.MOVIE_ID, "No Movie ID supplied" );
        Movie movie  = service.read( Integer.parseInt(id) );

        return movie;
    }

    /**
     * Read a all movies from the database and then return them.
     * @param Request the client request to the system
     * @param Response response to be sent to client
     * @retrun List<Movie> of all movies based on query
    */
    public List<Movie> readAll(Request request, Response response) {
        QueryFilter    filter    = QueryFilters.parseFrom(request);
        QueryOrder     order     = QueryOrders.parseFrom(request);
        QueryRange     range     = QueryRanges.parseFrom(request, 20);
        List<Movie>     movies   = service.readAll(filter, range, order);

        long count = service.count(filter);
        response.setCollectionResponse(range, movies.size(), count);
        return movies;
    }

    /**
     * Create a movie in the database
     * @param Request request should contain movie as payload
     * @param Response response to be sent to client
    */
    public Movie create(Request request, Response response) {
        Movie movie  = request.getBodyAs(Movie.class, "Movie details not provided");
        Movie saved = service.create(movie);

        response.setResponseCreated();
        return saved;
    }

    /**
     * Update a movie in the database
     * @param Request request should contain parameter groupId and movie as payload
     * @param Response response to be sent to client
    */
    public void update(Request request, Response response) {
        String id = request.getHeader(Constants.Url.MOVIE_ID, "No Movie ID supplied");
        Movie movie = request.getBodyAs(Movie.class, "Resource details not provided");

        movie.setId( Integer.parseInt(id) );
        service.update(movie);
        response.setResponseNoContent();
    }

    /**
     * Delete a movie from the database
     * @param Request request should contain parameter movieId
     * @param Response response to be sent to client
    */
    public void delete(Request request, Response response) {
        String id = request.getHeader(Constants.Url.MOVIE_ID, "No Movie ID supplied");
        service.delete( Integer.parseInt(id) );
        response.setResponseNoContent();
    }
}
