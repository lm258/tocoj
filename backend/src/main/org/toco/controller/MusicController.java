/**
 * © Copyright Lewis Maitland 2015
 * This file is part of Toco.
 * 
 * Toco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Toco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Toco.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.toco.controller;

import java.util.List;

import org.jboss.netty.handler.codec.http.HttpMethod;
import org.restexpress.Request;
import org.restexpress.Response;
import org.restexpress.common.query.QueryFilter;
import org.restexpress.common.query.QueryOrder;
import org.restexpress.common.query.QueryRange;
import org.restexpress.query.QueryFilters;
import org.restexpress.query.QueryOrders;
import org.restexpress.query.QueryRanges;

import org.toco.Constants;
import org.toco.domain.Song;
import org.toco.domain.MusicAlbum;
import org.toco.service.SongService;
import org.toco.service.MusicAlbumService;

/**
 * 
 * @author Lewis Maitland
 */
public class MusicController
{
    private SongService service;
    private MusicAlbumService albumService;

    /**
     * Default Constructor
     * @param MusicService is the service used to provide database interaction for songs
    */
    public MusicController(SongService songService, MusicAlbumService albumService) {
        super();
        this.service = songService;
        this.albumService = albumService;
    }

    /**
     * Read a single song from the database and then return that
     * song.
     * @param Request the client request to the system
     * @param Response response to be sent to client
     * @retrun Song the song that was read
    */
    public Song read(Request request, Response response) {
        String id  = request.getHeader( Constants.Url.SONG_ID, "No Song ID supplied" );
        Song song  = service.read( Integer.parseInt(id) );

        return song;
    }

    /**
     * Read a single album from the database and then return that
     * album.
     * @param Request the client request to the system
     * @param Response response to be sent to client
     * @retrun MusicAlbum the album that was read
    */
    public MusicAlbum readAlbum(Request request, Response response) {
        String id         = request.getHeader( Constants.Url.ALBUM_ID, "No MusicAlbum ID supplied" );
        MusicAlbum album  = albumService.read( Integer.parseInt(id) );

        return album;
    }

    /**
     * Read a all songs from the database and then return them.
     * @param Request the client request to the system
     * @param Response response to be sent to client
     * @retrun List<Song> of all songs based on query
    */
    public List<Song> readAll(Request request, Response response) {
        QueryFilter    filter    = QueryFilters.parseFrom(request);
        QueryOrder     order     = QueryOrders.parseFrom(request);
        QueryRange     range     = QueryRanges.parseFrom(request, 20);
        List<Song>     songs     = service.readAll(filter, range, order);

        long count = service.count(filter);
        response.setCollectionResponse(range, songs.size(), count);

        return songs;
    }

    /**
     * Read a all albums from the database and then return them.
     * @param Request the client request to the system
     * @param Response response to be sent to client
     * @retrun List<MusicAlbum> of all albums based on query
    */
    public List<MusicAlbum> readAllAlbum(Request request, Response response) {
        QueryFilter      filter    = QueryFilters.parseFrom(request);
        QueryOrder       order     = QueryOrders.parseFrom(request);
        QueryRange       range     = QueryRanges.parseFrom(request, 20);
        List<MusicAlbum> albums    = albumService.readAll(filter, range, order);

        long count = service.count(filter);
        response.setCollectionResponse(range, albums.size(), count);

        return albums;
    }

    /**
     * Create a song in the database
     * @param Request request should contain song as payload
     * @param Response response to be sent to client
    */
    public Song create(Request request, Response response) {
        Song song  = request.getBodyAs(Song.class, "Song details not provided");
        Song saved = service.create(song);

        response.setResponseCreated();
        return saved;
    }

    /**
     * Update a song in the database
     * @param Request request should contain parameter groupId and song as payload
     * @param Response response to be sent to client
    */
    public void update(Request request, Response response) {
        String id = request.getHeader(Constants.Url.SONG_ID, "No Song ID supplied");
        Song song = request.getBodyAs(Song.class, "Resource details not provided");

        song.setId( Integer.parseInt(id) );
        service.update(song);
        response.setResponseNoContent();
    }

    /**
     * Delete a song from the database
     * @param Request request should contain parameter songId
     * @param Response response to be sent to client
    */
    public void delete(Request request, Response response) {
        String id = request.getHeader(Constants.Url.SONG_ID, "No Song ID supplied");
        service.delete( Integer.parseInt(id) );
        response.setResponseNoContent();
    }
}
