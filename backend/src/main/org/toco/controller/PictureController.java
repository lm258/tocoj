/**
 * © Copyright Lewis Maitland 2015
 * This file is part of Toco.
 * 
 * Toco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Toco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Toco.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.toco.controller;

import java.util.List;

import org.jboss.netty.handler.codec.http.HttpMethod;
import org.restexpress.Request;
import org.restexpress.Response;
import org.restexpress.common.query.QueryFilter;
import org.restexpress.common.query.QueryOrder;
import org.restexpress.common.query.QueryRange;
import org.restexpress.query.QueryFilters;
import org.restexpress.query.QueryOrders;
import org.restexpress.query.QueryRanges;

import org.toco.Constants;
import org.toco.domain.Picture;
import org.toco.domain.PictureAlbum;
import org.toco.service.PictureService;
import org.toco.service.PictureAlbumService;

/**
 * 
 * @author Lewis Maitland
 */
public class PictureController
{
    private PictureService service;
    private PictureAlbumService albumService;

    /**
     * Default Constructor
     * @param PictureService is the service used to provide database interaction for pictures
    */
    public PictureController(PictureService pictureService, PictureAlbumService pictureAlbumService) {
        super();
        this.service = pictureService;
        this.albumService = pictureAlbumService;
    }

    /**
     * Read a single picture from the database and then return that
     * picture.
     * @param Request the client request to the system
     * @param Response response to be sent to client
     * @retrun Picture the picture that was read
    */
    public Picture read(Request request, Response response) {
        String id  = request.getHeader( Constants.Url.PICTURE_ID, "No Picture ID supplied" );
        Picture picture  = service.read( Integer.parseInt(id) );

        return picture;
    }

    /**
     * Read a single picture album from the database and then return that
     * picture album.
     * @param Request the client request to the system
     * @param Response response to be sent to client
     * @retrun PictureAlbum the album that was read
    */
    public PictureAlbum readAlbum(Request request, Response response) {
        String id             = request.getHeader( Constants.Url.ALBUM_ID, "No Picture ID supplied" );
        PictureAlbum album  = albumService.read( Integer.parseInt(id) );

        return album;
    }

    /**
     * Read a all pictures from the database and then return them.
     * @param Request the client request to the system
     * @param Response response to be sent to client
     * @retrun List<Picture> of all pictures based on query
    */
    public List<Picture> readAll(Request request, Response response) {
        QueryFilter    filter    = QueryFilters.parseFrom(request);
        QueryOrder     order     = QueryOrders.parseFrom(request);
        QueryRange     range     = QueryRanges.parseFrom(request, 20);
        List<Picture>  pictures  = service.readAll(filter, range, order);

        long count = service.count(filter);
        response.setCollectionResponse(range, pictures.size(), count);

        return pictures;
    }

    /**
     * This function is used to fetch all of the alubms from the database
     * Decided to put this in the picture controller to save creating
     * another full controller for only handling the pictures. 
    */
    public List<PictureAlbum> readAllAlbum(Request request, Response response) {
        QueryFilter         filter  = QueryFilters.parseFrom(request);
        QueryOrder          order   = QueryOrders.parseFrom(request);
        QueryRange          range   = QueryRanges.parseFrom(request, 20);
        List<PictureAlbum>  albums  = albumService.readAll(filter, range, order);

        long count = albumService.count(filter);
        response.setCollectionResponse(range, albums.size(), count);

        return albums;
    }

    /**
     * Create a picture in the database
     * @param Request request should contain picture as payload
     * @param Response response to be sent to client
    */
    public Picture create(Request request, Response response) {
        Picture picture  = request.getBodyAs(Picture.class, "Picture details not provided");
        Picture saved = service.create(picture);

        response.setResponseCreated();
        return saved;
    }

    /**
     * Update a picture in the database
     * @param Request request should contain parameter groupId and picture as payload
     * @param Response response to be sent to client
    */
    public void update(Request request, Response response) {
        String id = request.getHeader(Constants.Url.PICTURE_ID, "No Picture ID supplied");
        Picture picture = request.getBodyAs(Picture.class, "Resource details not provided");

        picture.setId( Integer.parseInt(id) );
        service.update(picture);
        response.setResponseNoContent();
    }

    /**
     * Delete a picture from the database
     * @param Request request should contain parameter pictureId
     * @param Response response to be sent to client
    */
    public void delete(Request request, Response response) {
        String id = request.getHeader(Constants.Url.PICTURE_ID, "No Picture ID supplied");
        service.delete( Integer.parseInt(id) );
        response.setResponseNoContent();
    }
}
