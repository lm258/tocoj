/**
 * © Copyright Lewis Maitland 2015
 * This file is part of Toco.
 * 
 * Toco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Toco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Toco.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.toco.controller;

import org.jboss.netty.handler.codec.http.HttpMethod;
import org.restexpress.Request;
import org.restexpress.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.toco.Constants;
import org.toco.Main;
import org.toco.MimeTypes;
import java.util.Map;
import java.util.HashMap;
import org.toco.persistence.SQLRepository;
import org.toco.domain.*;
import org.toco.mediaserver.DeviceHandler;
import org.sormula.Database;

import org.jboss.netty.buffer.ChannelBuffers;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.FilenameUtils;
import java.io.FileInputStream;
import org.restexpress.exception.*;

/**
 * StaticContentController is used to provide static
 * content such as web pages,css and javascript to
 * clients. Although currently loading from an external
 * folder this should be moved to a java resource and
 * additional support for plugins should be thought out.
*/
public class StaticContentController {

    private String webDirectory;
    private static final Logger LOG = LoggerFactory.getLogger( StaticContentController.class.getName() );
    private Map<String, SQLRepository> handlerMap;
    private DeviceHandler deviceHandler;

    public StaticContentController( String webDirectory, Map<String, SQLRepository> handlerMap, Database database ) throws Exception {
        super();
        this.webDirectory = webDirectory;
        this.handlerMap   = handlerMap;
        deviceHandler     = new DeviceHandler( database );
    }

    public StaticContentController( Map<String, SQLRepository> handlerMap, Database database ) throws Exception {
        this( "web", handlerMap, database );
    }

    /**
     * This is just a dummy function.
     * StaticContentController does not allow for data manipulation through
     * its interface.
     * NOTE: In the future this could be used for uploads
    */
    public void create(Request request, Response response){};

    /**
     * This is just a dummy function.
     * StaticContentController does not allow for data manipulation through
     * its interface.
    */
    public void delete(Request request, Response response){};

    /**
     * This is just a dummy function.
     * StaticContentController does not allow for data manipulation through
     * its interface.
    */
    public void update(Request request, Response response){};

    /**
     * Read function is used to read a peice of static content
     * from the webserver side. Default folder is "web" where
     * static content can be stored.
    */
    public void read(Request request, Response response)
    {
        /*If they have just put a forward slash then we need to redirect to the homepage*/
        if( request.getPath().compareTo("/") == 0 ) {
            response.setResponseCode(302);
            response.addHeader("Location", "/web/index.html");
            return;
        }

        /*Get the path and file extension*/
        String path     = request.getPath().replaceAll( "/web/", "web/" ).split("\\?")[0];
        String ext      = FilenameUtils.getExtension( path );
        String basename = FilenameUtils.getBaseName( path );

        /*Handle zee reaquest*/

        /*check if we are getting movies etc*/
        if (path.matches(".*/content/fetch/movie/.*")) {
            path = ((Movie)handlerMap.get( "movie" ).read( Integer.parseInt(basename) )).getFile();

            if( !deviceHandler.requestHasAccess( request, Integer.parseInt(basename), Movie.class ) ) {
                throw new ForbiddenException("Access to this file is restricted.");
            }
        } else if (path.matches(".*/content/fetch/song/.*")) {
            path = ((Song)handlerMap.get( "song" ).read( Integer.parseInt(basename) )).getFile();
            if( !deviceHandler.requestHasAccess( request, Integer.parseInt(basename), Song.class ) ) {
                throw new ForbiddenException("Access to this file is restricted.");
            }
        } else if (path.matches(".*/content/fetch/picture/.*")) {
            path = ((Picture)handlerMap.get( "picture" ).read( Integer.parseInt(basename) )).getFile();
            if( !deviceHandler.requestHasAccess( request, Integer.parseInt(basename), Picture.class ) ) {
                throw new ForbiddenException("Access to this file is restricted.");
            }
        }

        try {
            byte[] bytes = IOUtils.toByteArray( new FileInputStream( path ) );
            response.setContentType( MimeTypes.getMimeType( ext ) );
            response.setBody(ChannelBuffers.wrappedBuffer(bytes));
        } catch ( Exception e ) {
            StaticContentController.LOG.warn( "Error: " + e.getMessage() );
            response.setResponseCode(404);
            response.setContentType( "text/html" );
            response.setBody( new String("<h1>Not found</h1><p>The web page you are trying to reach cannot be found.<p>") );
        }
    }

}