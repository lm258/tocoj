/**
 * © Copyright Lewis Maitland 2015
 * This file is part of Toco.
 * 
 * Toco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Toco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Toco.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.toco.controller;

import java.util.List;

import org.jboss.netty.handler.codec.http.HttpMethod;
import org.restexpress.Request;
import org.restexpress.Response;
import org.restexpress.common.query.QueryFilter;
import org.restexpress.common.query.QueryOrder;
import org.restexpress.common.query.QueryRange;
import org.restexpress.query.QueryFilters;
import org.restexpress.query.QueryOrders;
import org.restexpress.query.QueryRanges;

import org.toco.Constants;
import org.toco.domain.User;
import org.toco.service.UserService;

/**
 * 
 * @author Lewis Maitland
 */
public class UserController
{
    private UserService service;

    /**
     * Default Constructor
     * @param UserService is the service used to provide database interaction for users
    */
    public UserController(UserService userService) {
        super();
        this.service = userService;
    }

    /**
     * Read a single user from the database and then return that
     * user.
     * @param Request the client request to the system
     * @param Response response to be sent to client
     * @retrun User the user that was read
    */
    public User read(Request request, Response response) {
        String id  = request.getHeader( Constants.Url.USER_ID, "No User ID supplied" );
        User user  = service.read( Integer.parseInt(id) );

        return user;
    }

    /**
     * Read a all users from the database and then return them.
     * @param Request the client request to the system
     * @param Response response to be sent to client
     * @retrun List<User> of all users based on query
    */
    public List<User> readAll(Request request, Response response) {
        QueryFilter filter = QueryFilters.parseFrom(request);
        QueryOrder  order  = QueryOrders.parseFrom(request);
        QueryRange  range  = QueryRanges.parseFrom(request, 20);
        List<User>  users  = service.readAll(filter, range, order);

        long count = service.count(filter);
        response.setCollectionResponse(range, users.size(), count);

        return users;
    }

    /**
     * Create a user in the database
     * @param Request request should contain user as payload
     * @param Response response to be sent to client
    */
    public User create(Request request, Response response) {
        User user  = request.getBodyAs(User.class, "User details not provided");
        User saved = service.create(user);

        response.setResponseCreated();
        return saved;
    }

    /**
     * Update a user in the database
     * @param Request request should contain parameter groupId and user as payload
     * @param Response response to be sent to client
    */
    public void update(Request request, Response response) {
        String id = request.getHeader(Constants.Url.USER_ID, "No User ID supplied");
        User user = request.getBodyAs(User.class, "Resource details not provided");

        user.setId( Integer.parseInt(id) );
        service.update(user);
        response.setResponseNoContent();
    }

    /**
     * Delete a user from the database
     * @param Request request should contain parameter userId
     * @param Response response to be sent to client
    */
    public void delete(Request request, Response response) {
        String id = request.getHeader(Constants.Url.USER_ID, "No User ID supplied");
        service.delete( Integer.parseInt(id) );
        response.setResponseNoContent();
    }
}
