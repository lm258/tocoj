/**
 * © Copyright Lewis Maitland 2015
 * This file is part of Toco.
 * 
 * Toco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Toco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Toco.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.toco.domain;

import com.strategicgains.syntaxe.annotation.StringValidation;
import org.sormula.annotation.Column;
import org.sormula.annotation.Row;
import org.sormula.annotation.Where;
import org.sormula.annotation.WhereField;
import org.sormula.annotation.Wheres;

/**
 * This is class is used to model an App in the Toco CMS.
 * Additional apps can be added to the system using the plugin
 * folder.
 * @author Lewis Maitland
 */
public class App
{
    @Column(identity=true)
    private Integer id;

    @StringValidation(required=true)
    private String icon;

    @StringValidation(required=true)
    private String app;

    @StringValidation(required=true)
    private String name;
    
    public App() {
    }

    /**
     * Function used to get the id of the app
     * @return id id of the app
    */
    public Integer getId() {
        return id;
    }

    /**
     * Function used to set the id of the app
     * @param id id of app
    */
    public void setId( Integer id ) {
        this.id = id;
    }

    /**
     * Function used to get the icon of the app
     * @return icon icon of the app
    */
    public String getIcon() {
        return icon;
    }

    /**
     * Function used to set the icon of the app
     * @param icon icon of the app
    */
    public void setIcon( String icon ) {
        this.icon = icon;
    }

    /**
     * Function used to get the the app
     * @return app the app
    */
    public String getApp() {
        return this.app;
    }

    /**
     * Function used to set the app
     * @return app change the app
    */
    public void setApp( String app ) {
        this.app = app;
    }

    /**
     * Function used to get the name of the app
     * @return name name of the app
    */
    public String getName() {
        return this.name;
    }

    /**
     * Function used to set the name of the app
     * @param name name of the app
    */
    public void setName( String name ) {
        this.name = name;
    }
}