/**
 * © Copyright Lewis Maitland 2015
 * This file is part of Toco.
 * 
 * Toco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Toco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Toco.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.toco.domain;

import com.strategicgains.syntaxe.annotation.StringValidation;
import com.strategicgains.syntaxe.annotation.RegexValidation;
import org.sormula.annotation.Column;
import org.sormula.annotation.Where;
import org.sormula.annotation.EnumType;
import org.toco.Constants.DeviceType;
import org.toco.Constants;
import org.toco.Sanitize;

/**
 * This class is used to model a device.
 * Devices are physical devices that are/have been
 * connected to Toco at some point. Devices are
 * used to map content to a user.
 * @author Lewis Maitland
 */
@Where(name="userId", fieldNames="userId")
public class Device
{
    @Column(identity=true)
    private Integer id;

    @RegexValidation(pattern=Constants.Validation.MAC_REGEX, message=Constants.Validation.MAC_ERROR)
    private String mac;
    private Integer userId;

    @StringValidation(required=true)
    private String name;

    @EnumType(defaultEnumName="DESKTOP")
    private DeviceType type;
    
    public Device() {
    }

    /**
     * Get the ID of this device in the database
     * @return Returns the ID
    */
    public Integer getId(){
        return this.id;
    }

    /**
     * Set the ID of this device in the database
     * @param ID the new ID
    */
    public void setId( Integer id ) {
        this.id = id;
    }

    /**
     * Get the mac of this device
     * @return Returns the mac
    */
    public String getMac() {
        return mac;
    }

    /**
     * Set the mac of this device.
     * The MAC address will be sanatized according MAC sanitization rules
     * @param mac the new mac
    */
    public void setMac( String mac ) {
        this.mac = Sanitize.mac( mac );
    }

    /**
     * Get the userId of this device
     * @return Returns the userId
    */
    public Integer getUserId() {
        return userId;
    }

    /**
     * Set the userId of this device.
     * @param userId the new userId
    */
    public void setUserId( Integer userId ) {
        this.userId = userId;
    }

    /**
     * Get the name of this device
     * @return Returns the name
    */
    public String getName() {
        return name;
    }

    /**
     * Set the name of this device.
     * @param name the new name
    */
    public void setName( String name ) {
        this.name = name;
    }

    /**
     * Get the type of this device
     * @return Returns the type
    */
    public DeviceType getType() {
        return type;
    }

    /**
     * Set the type of this device.
     * @param type the new type
    */
    public void setType( DeviceType type ) {
        this.type = type;
    }
}