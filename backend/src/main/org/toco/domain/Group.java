/**
 * © Copyright Lewis Maitland 2015
 * This file is part of Toco.
 * 
 * Toco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Toco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Toco.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.toco.domain;

import com.strategicgains.syntaxe.annotation.StringValidation;
import com.strategicgains.syntaxe.annotation.RegexValidation;
import org.sormula.annotation.Column;
import org.sormula.annotation.Where;
import org.sormula.annotation.Wheres;
import org.sormula.annotation.Row;

/**
 * Group class is used to model a group in Toco.
 * Groups are used to bridge the link between content
 * and users of the system.
 * @author Lewis Maitland
 */
@Row(tableName="\"group\"")
@Wheres({
    @Where(name="id", fieldNames="id"),
    @Where(name="groupId", fieldNames="groupId")
})
public class Group
{
    @Column(identity=true)
    private Integer id;

    @StringValidation(required=true)
    private String name;
    
    public Group() {
    }

    /**
     * Get the ID of this group
     * @return Returns the ID
    */
    public Integer getId(){
        return this.id;
    }

    /**
     * Set the ID of this group
     * @param ID the new ID
    */
    public void setId( Integer id ) {
        this.id = id;
    }

    /**
     * Get the name of this group
     * @return Returns the name
    */
    public String getName() {
        return name;
    }

    /**
     * Set the name of this group
     * @param name the new name
    */
    public void setName( String name ) {
        this.name = name;
    }
}