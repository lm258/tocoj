/**
 * © Copyright Lewis Maitland 2015
 * This file is part of Toco.
 * 
 * Toco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Toco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Toco.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.toco.domain;
import org.toco.annotation.GroupsJoin;
import java.util.List;
import java.util.ArrayList;
import org.sormula.annotation.*;
import org.sormula.annotation.cascade.OneToManyCascade;
import org.sormula.annotation.cascade.SelectCascade;
import com.strategicgains.syntaxe.annotation.StringValidation;
import com.strategicgains.syntaxe.annotation.ChildValidation;
import org.fourthline.cling.support.model.item.*;
import org.fourthline.cling.support.model.container.Container;
import org.fourthline.cling.support.model.Res;
import org.toco.mediaserver.DIDLItem;
import org.seamless.util.MimeType;
import org.fourthline.cling.support.model.Person;
import org.toco.config.Configuration;
import org.apache.commons.io.FilenameUtils;

public class Movie implements DIDLItem {
    
    @Column(identity=true)
    private Integer id;

    @StringValidation(required=true)
    private String title;
    
    private Integer year;
    private String file;
    private String plot;
    private String rating;
    private String genre;
    private String country;
    private Integer metaScore;
    private Integer runtime;
    private String director;
    private String imdbID;
    private String icon;
    private String mimeType;
    private Long size;
    private Long bitrate;
    private Long length;

    @ChildValidation
    @Transient
    @GroupsJoin(foreignKey="movieId", joinClass=MovieGroup.class)
    private List<Group> groups;

    public Integer getId() {
        return id;
    }

    public void setId( Integer id ) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle( String title ) {
        this.title = title;
    }

    public void setYear( Integer year ) {
        this.year = year;
    }

    public Integer getYear() {
        return year;
    }

    public String getFile() {
        return file;
    }

    public void setFile( String file ){
        this.file = file;
    }

    public String getPlot() {
        return plot;
    }

    public void setPlot( String plot ) {
        this.plot = plot;
    }

    public List<Group> getGroups() {
        return groups;
    }

    public void setGroups( List<Group> groups ) {
        this.groups = groups;
    }

    public String getRating() {
        return rating;
    }

    public void setRating( String rating ) {
        this.rating = rating;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre( String genre ) {
        this.genre = genre;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry( String country ) {
        this.country = country;
    }

    public Integer getMetaScore() {
        return metaScore;
    }
    
    public void setMetaScore( Integer metaScore ) {
        this.metaScore = metaScore;
    }

    public void setRuntime( Integer runtime ) {
        this.runtime = runtime;
    }

    public Integer getRuntime() {
        return runtime;
    }

    public void setDirector( String director ) {
        this.director = director;
    }

    public String getDirector() {
        return director;
    }

    public void setImdbID( String id ) {
        this.imdbID = id;
    }

    public String getImdbID() {
        return imdbID;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon( String icon ) {
        this.icon = icon;
    }

    public void setMimeType( String mimeType ){
        this.mimeType = mimeType;
    }

    public String getMimeType(){
        return mimeType;
    }

    public void setSize( Long size ) {
        this.size = size;
    }

    public Long getSize() {
        return size;
    }

    public void setBitrate( Long bitrate ) {
        this.bitrate = bitrate;
    }

    public Long getBitrate() {
        return bitrate;
    }

    public Long getLength(){
        return length;
    }

    public void setLength( Long length ) {
        this.length = length;
    }

    /**
     * This function is used to convert this
     * song into an item for a UPnP response.
     * @param Container parent is our parent container
     * @return Item the UPnP item
    */
    @Override
    public Item toItem( Container parent ) {

        Res res = new Res(
                new MimeType( "video", getMimeType() ),
                getSize(),
                String.format("%02d:%02d:%02d", getLength()/3600, (getLength()%3600)/60, (getLength()%60)),
                getBitrate(),
                Configuration.BASE_URL+"web/content/fetch/movie/" + id + "." + FilenameUtils.getExtension( file )
        );
        VideoItem item = new VideoItem(
            getDIDLId(),
            parent,
            this.getTitle(),
            getDirector(),
            res
        );
        item.setGenres( getGenre().split(",") );
        item.setLongDescription( getPlot() );
        item.setRating( getRating().toString() );
        String[] directors = getDirector().split(",");
        Person[] pDirectors = new Person[ directors.length ];
        for(int i=0; i< directors.length; i++ ){
            pDirectors[i] = new Person( directors[i] );
        }
        item.setDirectors( pDirectors );
        return item;
    }

    /**
     * This function is used to get the
     * DIDL UPnP ID we will use in the UPnP
     * response. Its a bit crude at the moment
     * but is enough to work.
     * @return String of the new id
    */
    @Override
    public String getDIDLId() {
        return  Integer.toString(
            new String( getId() + this.getClass().getName() ).hashCode() 
        );
    }
}