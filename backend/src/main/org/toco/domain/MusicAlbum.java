/**
 * © Copyright Lewis Maitland 2015
 * This file is part of Toco.
 * 
 * Toco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Toco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Toco.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.toco.domain;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import org.sormula.annotation.*;
import org.sormula.annotation.cascade.OneToManyCascade;
import org.sormula.annotation.cascade.SelectCascade;
import com.strategicgains.syntaxe.annotation.StringValidation;
import com.strategicgains.syntaxe.annotation.ChildValidation;
import org.toco.mediaserver.DIDLContainer;
import org.fourthline.cling.support.model.container.*;
import org.fourthline.cling.support.model.item.Item;
import org.fourthline.cling.support.model.DIDLContent;
import org.restexpress.common.query.QueryFilter;
import org.toco.persistence.GroupFilter;

/**
 * MusicAlbum class is used to model a music album in Toco.
 * Music albums contain a list of songs, songs can be fetch
 * for an album using the music controller.
 * @author Lewis Maitland
*/
public class MusicAlbum implements DIDLContainer {
    
    @Column(identity=true)
    private Integer id;

    @StringValidation(required=true)
    private String name;

    /*These fields are optional and not required*/
    private String genre;
    private String artist;
    private Integer year;

    @ChildValidation
    @OneToManyCascade(
        name="albumMusic",
        foreignKeyValueFields="albumId",
        selects=@SelectCascade(sourceParameterFieldNames="id", targetWhereName="albumId")
    )
    private List<Song> songs;

    /**
     * This function is used to get the ID
     * @return ID
    */
    public Integer getId() {
        return id;
    }

    /**
     * This function is used to set the id
     * @param Integer id
    */
    public void setId( Integer id ) {
        this.id = id;
    }

    /**
     * This function is used to get the name
     * @return name
    */
    public String getName() {
        return name;
    }

    /**
     * This function is used to set the name
     * @param String name
    */
    public void setName( String name ) {
        this.name = name;
    }

    /**
     * This function is used to get the songs
     * @return List<Song> songs
    */
    public List<Song> getSongs() {
        return songs;
    }

    /**
     * This function is used to set the songs
     * @param List<Song> songs
    */
    public void setSongs( List<Song> songs ) {
        this.songs = songs;
    }

    /**
     * This function is used to get the genre
     * @return genre
    */
    public String getGenre() {
        return genre;
    }

    /**
     * This function is used to set the genre
     * @param String genre
    */
    public void setGenre( String genre ) {
        this.genre = genre;
    }

    /**
     * This function is used to get the artist
     * @return artist
    */
    public String getArtist(){
        return artist;
    }

    /**
     * This function is used to set the artist
     * @param String artist
    */
    public void setArtist( String artist ) {
        this.artist = artist;
    }

    /**
     * This function is used to set the year
     * @param Integer year
    */
    public void setYear( Integer year ) {
        this.year = year;
    }

    /**
     * This function is used to get the year
     * @return year
    */
    public Integer getYear() {
        return year;
    }

    /**
     * This function is used to convert this album
     * into a suitable DIDLContainer that can be
     * used for creating UPnP responses.
     * @return Container the new container
    */
    public Container toContainer( Container parent ) {

        /*Create our music album*/
        Album albumCont = new Album(
            getDIDLId(), 
            parent, getName(), 
            getArtist(), 
            songs.size()
        );

        /*Set our songs as items*/
        List<Item> items = new ArrayList<Item>();
        for( Song song : songs ){
            items.add( song.toItem( albumCont ) );
        }
        albumCont.setItems( items );
        
        return albumCont;
    }

    /**
     * This function is used to set the songs
     * of this object.
     * @param List of items
    */
    public void setItems( List items ) {
        setSongs( items );
    }

    /**
     * This allows us to find one of the items in this
     * MusicContainer by its ID. Essentially we can use
     * the ID to build our UPnP tree.
     * UPDATE: Not using this method as at this point we are
     * at lowest point in the tree.
     * @param String id of the song
     * @param DIDLContent didl didl content to add to
    */
    @Override
    public void findById( String id, Container parent, DIDLContent didl, QueryFilter filter ){
    }

    /**
     * This function is used for getting a unique
     * ID that the system can use to identify folders
     * in the UPnP response.
    */
    @Override
    public String getDIDLId() {
        return  Integer.toString(
            new String( getId() + this.getClass().getName() ).hashCode() 
        );
    }
}