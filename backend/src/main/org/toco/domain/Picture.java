/**
 * © Copyright Lewis Maitland 2015
 * This file is part of Toco.
 * 
 * Toco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Toco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Toco.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.toco.domain;
import org.toco.annotation.GroupsJoin;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import org.sormula.annotation.*;
import org.sormula.annotation.cascade.OneToManyCascade;
import org.sormula.annotation.cascade.SelectCascade;
import com.strategicgains.syntaxe.annotation.StringValidation;
import com.strategicgains.syntaxe.annotation.ChildValidation;
import org.sormula.annotation.Where;
import org.sormula.annotation.Wheres;
import org.fourthline.cling.support.model.item.*;
import org.fourthline.cling.support.model.container.Container;
import org.fourthline.cling.support.model.Res;
import org.toco.mediaserver.DIDLItem;
import org.seamless.util.MimeType;
import org.toco.config.Configuration;
import org.apache.commons.io.FilenameUtils;

@Wheres({
    @Where(name="albumId", fieldNames="albumId")
})
public class Picture implements DIDLItem {
    
    @Column(identity=true)
    private Integer id;

    @StringValidation(required=true)
    private String name;

    @StringValidation(required=true)
    private Date date;

    private String file;
    private String description;
    private Integer albumId;
    private String mimeType;
    private Long size;

    @ChildValidation
    @Transient
    @GroupsJoin(foreignKey="pictureId", joinClass=PictureGroup.class)
    private List<Group> groups;

    public Integer getId() {
        return id;
    }

    public void setId( Integer id ) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName( String name ) {
        this.name = name;
    }

    public void setDate( Date date ) {
        this.date = date;
    }

    public Date getDate() {
        return date;
    }

    public String getFile() {
        return file;
    }

    public void setFile( String file ){
        this.file = file;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription( String description ) {
        this.description = description;
    }

    public List<Group> getGroups() {
        return groups;
    }

    public void setGroups( List<Group> groups ) {
        this.groups = groups;
    }

    public Integer getAlbumId() {
        return albumId;
    }

    public void setAlbumId( Integer id ) {
        this.albumId = id;
    }
    
    public void setMimeType( String mimeType ){
        this.mimeType = mimeType;
    }

    public String getMimeType(){
        return mimeType;
    }

    public void setSize( Long size ) {
        this.size = size;
    }

    public Long getSize() {
        return size;
    }

    /**
     * This function is used to convert this
     * song into an item for a UPnP response.
     * @param Container parent is our parent container
     * @return Item the UPnP item
    */
    @Override
    public Item toItem( Container parent ) {
        ImageItem item = new ImageItem(
            getDIDLId(),
            parent,
            getName(),
            "",
            new Res(
                new MimeType( "image", getMimeType() ),
                getSize(),
                Configuration.BASE_URL+"web/content/fetch/picture/" + id + "." + FilenameUtils.getExtension( file )
            )
        );
        item.setLongDescription( getDescription() );
        return item;
    }

    /**
     * This function is used to get the
     * DIDL UPnP ID we will use in the UPnP
     * response. Its a bit crude at the moment
     * but is enough to work.
     * @return String of the new id
    */
    @Override
    public String getDIDLId() {
        return  Integer.toString(
            new String( getId() + this.getClass().getName() ).hashCode() 
        );
    }
}