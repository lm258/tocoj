/**
 * © Copyright Lewis Maitland 2015
 * This file is part of Toco.
 * 
 * Toco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Toco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Toco.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.toco.domain;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import org.sormula.annotation.*;
import org.sormula.annotation.cascade.OneToManyCascade;
import org.sormula.annotation.cascade.SelectCascade;
import com.strategicgains.syntaxe.annotation.StringValidation;
import com.strategicgains.syntaxe.annotation.ChildValidation;
import org.toco.mediaserver.DIDLContainer;
import org.fourthline.cling.support.model.container.*;
import org.fourthline.cling.support.model.item.Item;
import org.fourthline.cling.support.model.DIDLContent;
import org.restexpress.common.query.QueryFilter;
import org.toco.persistence.GroupFilter;

/**
 * PictureAlbum is used for modelling albums of
 * pictures. This class contains the list of all
 * pictures the album contains.
 * @author Lewis Maitland
*/
public class PictureAlbum implements DIDLContainer {
    
    @Column(identity=true)
    private Integer id;

    @StringValidation(required=true)
    private String name;

    @ChildValidation
    @OneToManyCascade(
        name="albumPictures",
        foreignKeyValueFields="albumId",
        selects=@SelectCascade(sourceParameterFieldNames="id", targetWhereName="albumId")
    )
    private List<Picture> pictures;

    /**
     * This function is used to get the ID
     * @return ID
    */
    public Integer getId() {
        return id;
    }

    /**
     * This function is used to set the id
     * @param Integer id
    */
    public void setId( Integer id ) {
        this.id = id;
    }

    /**
     * This function is used to get the name
     * @return name
    */
    public String getName() {
        return name;
    }

    /**
     * This function is used to set the name
     * @param String name
    */
    public void setName( String name ) {
        this.name = name;
    }

    /**
     * This function is used to get the pictures
     * @return pictures
    */   
    public List<Picture> getPictures() {
        return pictures;
    }

    /**
     * This function is used to set the pictures
     * @param List<Picture> pictures
    */
    public void setPictures( List<Picture> pictures ) {
        this.pictures = pictures;
    }

    /**
     * This function is used to convert this album
     * into a suitable DIDLContainer that can be
     * used for creating UPnP responses.
     * @return Container the new container
    */
    public Container toContainer( Container parent ) {
        /*Create our music album*/
        Album albumCont = new Album(
            getDIDLId(), 
            parent, getName(), 
            "", 
            pictures.size()
        );

        /*Set our songs as items*/
        List<Item> items = new ArrayList<Item>();
        for( Picture picture : pictures ){
            items.add( picture.toItem( albumCont ) );
        }
        albumCont.setItems( items );

        return albumCont;
    }

    /**
     * This function is used to set the songs
     * of this object.
     * @param List of items
    */
    public void setItems( List items ) {
        setPictures( items );
    }

    /**
     * This allows us to find one of the items in this
     * MusicContainer by its ID. Essentially we can use
     * the ID to build our UPnP tree.
     * UPDATE: Not using this method as at this point we are
     * at lowest point in the tree.
     * @param String id of the picture
     * @param DIDLContent didl didl content to add to
    */
    @Override
    public void findById( String id, Container parent, DIDLContent didl, QueryFilter filter  ){
    }

    /**
     * This function is used for getting a unique
     * ID that the system can use to identify folders
     * in the UPnP response.
    */
    @Override
    public String getDIDLId() {
        return  Integer.toString(
            new String( getId() + this.getClass().getName() ).hashCode() 
        );
    }
}