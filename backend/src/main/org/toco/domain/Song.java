/**
 * © Copyright Lewis Maitland 2015
 * This file is part of Toco.
 * 
 * Toco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Toco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Toco.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.toco.domain;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import org.toco.annotation.GroupsJoin;
import org.sormula.annotation.*;
import org.sormula.annotation.cascade.OneToManyCascade;
import org.sormula.annotation.cascade.SelectCascade;
import com.strategicgains.syntaxe.annotation.StringValidation;
import com.strategicgains.syntaxe.annotation.ChildValidation;
import org.fourthline.cling.support.model.item.*;
import org.fourthline.cling.support.model.container.Container;
import org.fourthline.cling.support.model.Res;
import org.toco.mediaserver.DIDLItem;
import org.seamless.util.MimeType;
import org.toco.config.Configuration;
import org.apache.commons.io.FilenameUtils;

@Wheres({
    @Where(name="albumId", fieldNames="albumId")
})
public class Song implements DIDLItem {
    
    @Column(identity=true)
    private Integer id;

    private String artist;
    private String genre;

    @StringValidation(required=true)
    private String title;
    private Integer year;
    private String file;
    private String icon;
    private Integer albumId;
    private String mimeType;
    private Long size;
    private Long bitrate;
    private Long length;


    @ChildValidation
    @Transient
    @GroupsJoin(foreignKey="songId", joinClass=SongGroup.class)
    private List<Group> groups;

    public Integer getId() {
        return id;
    }

    public void setId( Integer id ) {
        this.id = id;
    }
    
    public String getGenre() {
        return genre;
    }

    public void setGenre( String genre ) {
        this.genre = genre;
    }

    public String getArtist(){
        return artist;
    }

    public void setArtist( String artist ) {
        this.artist = artist;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle( String title ) {
        this.title = title;
    }

    public void setYear( Integer year ) {
        this.year = year;
    }

    public Integer getYear() {
        return year;
    }

    public String getFile() {
        return file;
    }

    public void setFile( String file ){
        this.file = file;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon( String icon ) {
        this.icon = icon;
    }

    public List<Group> getGroups() {
        return groups;
    }

    public void setGroups( List<Group> groups ) {
        this.groups = groups;
    }

    public Integer getAlbumId() {
        return albumId;
    }

    public void setAlbumId( Integer albumId ) {
        this.albumId = albumId;
    }

    public void setMimeType( String mimeType ){
        this.mimeType = mimeType;
    }

    public String getMimeType(){
        return mimeType;
    }

    public void setSize( Long size ) {
        this.size = size;
    }

    public Long getSize() {
        return size;
    }

    public void setBitrate( Long bitrate ) {
        this.bitrate = bitrate;
    }

    public Long getBitrate() {
        return bitrate;
    }

    public Long getLength(){
        return length;
    }

    public void setLength( Long length ) {
        this.length = length;
    }

    /**
     * This function is used to convert this
     * song into an item for a UPnP response.
     * @param Container parent is our parent container
     * @return Item the UPnP item
    */
    @Override
    public Item toItem( Container parent ) {
        MusicTrack item = new MusicTrack(
            getDIDLId(),
            parent,
            getTitle(),
            getArtist(),
            ((getAlbumId() == null || parent == null) ? "" : parent.getTitle()),
            getArtist(),
            new Res(
                new MimeType( "audio", getMimeType() ),
                getSize(),
                String.format("%02d:%02d:%02d", getLength()/3600, (getLength()%3600)/60, (getLength()%60)),
                getBitrate(), 
                Configuration.BASE_URL+"web/content/fetch/song/" + id + "." + FilenameUtils.getExtension( file )
            )
        );
        item.setGenres( getGenre().split(",") );

        return item;
    }

    /**
     * This function is used to get the
     * DIDL UPnP ID we will use in the UPnP
     * response. Its a bit crude at the moment
     * but is enough to work.
     * @return String of the new id
    */
    @Override
    public String getDIDLId() {
        return  Integer.toString(
            new String( getId() + this.getClass().getName() ).hashCode() 
        );
    }
}