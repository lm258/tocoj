/**
 * © Copyright Lewis Maitland 2015
 * This file is part of Toco.
 * 
 * Toco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Toco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Toco.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.toco.domain;

import org.toco.annotation.GroupsJoin;
import com.github.jmkgreen.morphia.annotations.Entity;
import com.strategicgains.syntaxe.annotation.StringValidation;
import com.strategicgains.syntaxe.annotation.ChildValidation;
import org.sormula.annotation.*;
import java.util.List;
import java.util.ArrayList;
import org.sormula.annotation.cascade.*;
import org.toco.Constants.UserType;

/**
 * This is a sample entity identified by a MongoDB ObjectID (instead of a UUID).
 * It also contains createdAt and updatedAt properties that are automatically maintained
 * by the persistence layer (SampleOidEntityRepository).
 * @author Lewis Maitland
 */
public class User
{
    @Column(identity=true)
    private Integer id;

    @StringValidation(required=true, minLength=5, maxLength=10)
    private String username;

    @StringValidation(required=true, minLength=5, maxLength=25)
    private String password;

    @StringValidation(required=true, minLength=5, maxLength=10)
    private String realname;

    @EnumType(defaultEnumName="USER")
    private UserType type;

    /*Default avatar is always 1*/
    private Integer avatarId = 1;

    @ChildValidation
    @OneToManyCascade(foreignKeyValueFields="userId",
        selects=@SelectCascade(sourceParameterFieldNames="id", targetWhereName="userId")
    )
    private List<Device> devices = new ArrayList<Device>();

    @ChildValidation
    @Transient
    @GroupsJoin(foreignKey="userId", joinClass=UserGroup.class)
    private List<Group> groups;

    /**
     * Get the ID of this object in the database
     * @return Returns the ID
    */
    public Integer getId() {
        return id;
    }

    /**
     * Set the ID of this object in the database
     * @param ID the new ID
    */
    public void setId( Integer id ) {
        this.id = id;
    }

    public List<Device> getDevices() {
        return devices;
    }

    public void setDevices( List<Device> devices ){
        this.devices = devices;
    }

    public List<Group> getGroups() {
        return groups;
    }

    public void setGroups( List<Group> groups ){
        this.groups = groups;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername( String username ) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword( String password ) {
        this.password = password;
    }

    public String getRealname() {
        return realname;
    }

    public void setRealname( String realname ) {
        this.realname = realname;
    }

    public UserType getType() {
        return type;
    }

    public void setType( UserType type ) {
        this.type = type;
    }

    public Integer getAvatarId() {
        return avatarId;
    }

    public void setAvatarId( Integer id ){
        this.avatarId = id;
    }
}