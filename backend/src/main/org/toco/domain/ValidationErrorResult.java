/**
 * © Copyright Lewis Maitland 2015
 * This file is part of Toco.
 * 
 * Toco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Toco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Toco.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.toco.domain;

import java.util.Map;
import java.util.UUID;
import org.restexpress.domain.ErrorResult;
import org.restexpress.Response;
import com.strategicgains.syntaxe.ValidationException;

/**
 * Validation error result extends the RestExpress ErrorResult class.
 * This class is used to generate a Validation error rest which contains
 * the validation errors to display to the user using a response.
 * @author Lewis Maitland
*/
public class ValidationErrorResult extends ErrorResult {
    private Map<String, Object> errors;

    /**
     * Default costructor when no errors are present
    */
    public ValidationErrorResult( UUID errorId, int httpResponseCode, String errorMessage, String errorType ) {
        this( errorId, httpResponseCode, errorMessage, errorType, null );
    }

    /**
     * Default costructor when errors are to be included
    */
    public ValidationErrorResult( UUID errorId, int httpResponseCode, String errorMessage, String errorType, Map<String, Object> errors ) {
        super( errorId, httpResponseCode, errorMessage, errorType );
        this.errors = errors;
    }

    /**
     * Creates an ValidationErrorResult from the Response, optionally including the root cause exception name
     * as exceptionType.
     * 
     * @param response the RestExpress Response.
     * @return an ValidationErrorResult instance.
     */
    public static ValidationErrorResult fromResponse(Response response)
    {
        if (response.hasException())
        {
            Throwable exception = response.getException();
            String message      = exception.getMessage();
            String causeName    = exception.getClass().getSimpleName();

            Map<String, Object> errors = ((ValidationException)response.getException()).getErrors();
            return new ValidationErrorResult( UUID.randomUUID(), response.getResponseStatus().getCode(), message, causeName, errors );
        }
        return new ValidationErrorResult(UUID.randomUUID(), 0 , null, null);
    }
}