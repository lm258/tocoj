/**
 * © Copyright Lewis Maitland 2015
 * This file is part of Toco.
 * 
 * Toco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Toco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Toco.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.toco.domain;

import org.sormula.annotation.Column;
import org.sormula.annotation.Where;
import org.sormula.annotation.Wheres;
import org.sormula.annotation.Row;
import org.sormula.annotation.cascade.OneToOneCascade;
import org.sormula.annotation.cascade.SelectCascade;
import com.strategicgains.syntaxe.annotation.ChildValidation;

@Wheres({
    @Where(name="movieId", fieldNames="movieId"),
    @Where(name="movieIdGroupId", fieldNames={"groupId", "movieId"})
})
public class MovieGroup {

    @Column(identity=true)
    private Integer id;
    private Integer movieId;
    private Integer groupId;

    public Integer getId() {
        return id;
    }

    public void setId( Integer id ) {
        this.id = id;
    }

    public Integer getMovieId() {
        return movieId;
    }

    public void setMovieId( Integer movieId ) {
        this.movieId = movieId;
    }

    public void setGroupId( Integer groupId ) {
        this.groupId = groupId;
    }

    public Integer getGroupId() {
        return groupId;
    }
}