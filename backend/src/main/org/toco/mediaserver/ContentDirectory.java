/**
 * © Copyright Lewis Maitland 2015
 * This file is part of Toco.
 * 
 * Toco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Toco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Toco.  If not, see <http://www.gnu.org/licenses/>.
*/

package org.toco.mediaserver;

import org.fourthline.cling.support.contentdirectory.ContentDirectoryErrorCode;
import org.fourthline.cling.support.contentdirectory.ContentDirectoryException;
import org.fourthline.cling.support.contentdirectory.DIDLParser;
import org.fourthline.cling.support.model.BrowseFlag;
import org.fourthline.cling.support.model.BrowseResult;
import org.fourthline.cling.support.model.DIDLContent;
import org.fourthline.cling.support.model.DIDLObject;
import org.fourthline.cling.support.model.SortCriterion;
import org.fourthline.cling.model.profile.RemoteClientInfo;
import org.seamless.util.MimeType;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;

import org.toco.domain.*;
import org.sormula.Database;
import java.sql.DriverManager;
import org.fourthline.cling.support.model.container.Container;
import java.util.*;
import org.restexpress.common.query.*;
import org.toco.persistence.GroupFilter;
import org.fourthline.cling.model.profile.RemoteClientInfo;
import org.fourthline.cling.support.model.container.StorageFolder;

import java.util.logging.Logger;

/**
 * ContentDirectory is used to serve the directories
 * contained within Toco to a client UPnP device.
 * @author Lewis Maitland
 */
public class ContentDirectory extends AbstractContentDirectoryService {

    final private static Logger log = Logger.getLogger(ContentDirectory.class.getName());
    private List<DIDLContainer> rootChildren = new ArrayList<DIDLContainer>();
    private StorageFolder root;
    private DeviceHandler deviceHandler;


    /** 
     * Default constructor
    */
    public ContentDirectory(Database database) {

        try{
            /*Create our music Container*/
            MediaContainer musicCont =  new MediaContainer(
                "Music",
                database,
                MusicAlbum.class,
                Song.class
            );
            rootChildren.add( musicCont );

            /*Create our music Container*/
            MediaContainer pictureCont =  new MediaContainer(
                "Pictures",
                database,
                PictureAlbum.class,
                Picture.class
            );
            rootChildren.add( pictureCont );

            /*Create our music Container*/
            MediaContainer movieCont =  new MediaContainer(
                "Movies",
                database,
                Movie.class
            );
            rootChildren.add( movieCont );
            root = new StorageFolder("0", "", "root", "toco", null, null);
            for( DIDLContainer cont : rootChildren ){
                root.addContainer( cont.toContainer(root) );
            }

            deviceHandler = new DeviceHandler( database );
        }catch( Exception e ){
            throw new RuntimeException( e );
        }
    }

    /**
     * This function is used to find a particular peice
     * of content using its content id.
     * @param String id of the content
     * @param QueryFilter query for search
     * @param Integer[] groups for filtering content
     * @return DIDLContent upnp listing for client
    */
    public DIDLContent findById( String id, QueryFilter filter ) {
        DIDLContent didl = new DIDLContent();
        if( id.equals("0") ){
            for( DIDLContainer cont : rootChildren ){
                didl.addContainer( cont.toContainer(null) );
            }
        } else {
            for( DIDLContainer cont : rootChildren ){
                cont.findById( id, null, didl, filter );
            }
        }
        return didl;
    }

    /**
     * This function is used to browse Tocos upnp
     * database for content items and containers.
     * @param objectID is the id of the object
     * @param BroswFlag browseFlag
     * @param long firstResult
     * @param long maxResults
     * @param SortCriterion orderBy
     * @param RemoteClientInfo clientInfo
     * @return BrowseResult of the browse
     * @throws ContentDirectoryException
    */
    @Override
    public BrowseResult browse(String objectID, BrowseFlag browseFlag,
                               String filter,
                               long firstResult, long maxResults,
                               SortCriterion[] orderby, RemoteClientInfo clientInfo) throws ContentDirectoryException {
        try {
            /*Sample set of group Ids*/
            DIDLContent didl;
            if( browseFlag.equals(BrowseFlag.DIRECT_CHILDREN) ){
                QueryFilter filterObj = new QueryFilter();
                filterObj.addCriteria( "groups", FilterOperator.CONTAINS, new Integer[]{17}/*deviceHandler.getClientGroups( clientInfo )*/ );
                didl = findById( objectID, filterObj );
            } else {
                didl = new DIDLContent();
                didl.addContainer( root );
            }
            return new BrowseResult(new DIDLParser().generate(didl), didl.getCount(), didl.getCount());
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new ContentDirectoryException(
                    ContentDirectoryErrorCode.CANNOT_PROCESS,
                    "Exception! : " + ex.toString()
            );
        }
    }
}

