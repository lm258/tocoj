/**
 * © Copyright Lewis Maitland 2015
 * This file is part of Toco.
 * 
 * Toco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Toco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Toco.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.toco.mediaserver;
import org.fourthline.cling.support.model.container.Container;
import org.fourthline.cling.support.model.DIDLContent;
import org.restexpress.common.query.QueryFilter;
import org.toco.persistence.GroupFilter;
import java.util.List;

/**
 * DIDLContainer is an abstract interface used
 * by tocos content directory service to
 * convert container objects such as albums etc
 * into UPnP compatable containers.
 * @author Lewis Maitland
*/
public interface DIDLContainer {

    /**
     * toContainer will convert the implermenting
     * class into a didl compatible container
     * @return Container a container of this class
    */
    public Container toContainer( Container parent );

    /**
     * Find an element me contain by its DIDL Id
     * @param String id is the didl ID to find
     * @param Container parent is our parent container
     * @param DIDLContent is the didl contant handler
    */
    public void findById( String id, Container parent, DIDLContent didl, QueryFilter filter );

    /**
     * This function is used to set the items for
     * a container.
     * @param List of items
    */
    public void setItems( List items );

    /**
     * Returns the normal database ID of this object
     * @return Object id of object
    */
    public Object getId();

    /**
     * Returns a DIDL compatable ID for the object
     * @return String a didl compatable id
    */
    public String getDIDLId();
}