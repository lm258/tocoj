/**
 * © Copyright Lewis Maitland 2015
 * This file is part of Toco.
 * 
 * Toco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Toco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Toco.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.toco.mediaserver;

import org.fourthline.cling.support.model.item.Item;
import org.fourthline.cling.support.model.container.Container;

/**
 * DIDL Item is an abstract interface used
 * in tocos UPnP content directory for creating
 * UPnP items from classes.
 * @author Lewis Maitland
*/
public interface DIDLItem {

    /**
     * toItem will convert the implementing class
     * into a didl comptable item.
     * @return Item a didl item of the class
    */
    public Item toItem( Container parent );

    /**
     * Returns a DIDL compatable ID for the object
     * @return String a didl compatable id
    */
    public String getDIDLId();
}