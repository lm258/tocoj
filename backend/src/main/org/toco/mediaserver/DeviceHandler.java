package org.toco.mediaserver;

import org.toco.Sanitize;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import org.fourthline.cling.model.profile.RemoteClientInfo;
import org.sormula.Database;
import org.toco.domain.User;
import org.toco.domain.Device;
import org.toco.domain.Group;
import org.toco.domain.UserGroup;
import org.sormula.Table;
import org.toco.persistence.UserRepository;
import org.toco.persistence.DeviceRepository;
import org.restexpress.Request;
import java.util.*;
import java.lang.reflect.Field;
import org.toco.annotation.GroupsJoin;
import org.restexpress.common.util.StringUtils;

public class DeviceHandler {
    private Database database;
    private UserRepository userRepository;
    private DeviceRepository deviceRepository;

    /**
     * Default constructor takes database handler
    */
    public DeviceHandler( Database database ) throws Exception {
        this.database = database;
        userRepository = new UserRepository( database );
        deviceRepository = new DeviceRepository( database );
    }

    /**
     * This function is used to get the user given a mac
     * address
     * @param String mac mac address of the device
     * @return User that has the given MAC address
    */
    public User findUserByMAC( String mac ) {
        try{
            List<Device> devices = deviceRepository.selectAllCustom(
                "WHERE `mac` = ?",
                new Object[]{ mac }
            );
            return userRepository.read( devices.get(0).getUserId() );
        } catch( Exception e ) {
            return null;
        }
    }

    private String resolveIpAddress( String ip ) throws Exception {
        String mac  = "";
        String command = "cat /proc/net/arp | awk '{ if($1 == \""+ ip +"\") print $4; }'";
        Process p;
        p = Runtime.getRuntime().exec(new String[]{
            "/bin/bash", "-c", "cat /proc/net/arp | awk '{ if($1 == \""+ ip +"\") print $4; }'"
        });
        p.waitFor();
        BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
        mac = reader.readLine();
        return mac;
    }

    /**
     * This function is used to check if a Request
     * address has access to a particular peice of content
     * @param Request request to the content controller
     * @param mediaId of the item the user is trying to access
     * @param Class of the conten groups eg MovieGroups.class
    */
    public boolean requestHasAccess( Request request, Integer mediaId, Class itemClass ) {
        String ip = request.getRemoteAddress().getAddress().getHostAddress();
        try {
            String mac = resolveIpAddress( ip );
            if( mac != null && hasField( itemClass, "groups" ) ) {

                //1. get users group ids
                //2. check mediaGroups table has one of the users groupIds
                User user            = findUserByMAC( Sanitize.mac(mac) );
                Field groupField     = itemClass.getDeclaredField( "groups" );
                GroupsJoin groupJoin = groupField.getAnnotation( GroupsJoin.class );

                Table groupTable  = database.getTable( groupJoin.joinClass() );
                String foreignKey = groupJoin.foreignKey();

                Integer[] ids = new Integer[ user.getGroups().size() ];
                for( int i=0; i < user.getGroups().size(); i++ ) {
                    ids[i] =  user.getGroups().get(i).getId();
                }

                List<Object> objs = groupTable.selectAllCustom(
                    "WHERE `" + foreignKey + "` = ? AND `groupId` IN ("+
                    StringUtils.join(",", (Object [])ids ) + ")",
                    new Object[]{mediaId}
                );

                return (objs.size() > 0);
            }
        } catch( Exception e ){
            throw new RuntimeException( e );
        }
        return false;
    }

    /**
     * This is a helper function used to check if a class
     * has a field
    */
    public boolean hasField( Class e, String field ) {
        try{
            e.getDeclaredField( field );
            return true;
        }catch( Exception ex ){
            System.out.println( "CLASS: " + e.getName() + ", " + ex );
            return false;
        }
    }

    /**
     * This function is used to get the group ids
     * for a given clientInfo UPnP object.
     * @param RemoteClientInfo clientInfo of the client
     * @return Integer[] array of group ids for client
    */
    public Integer[] getClientGroups( RemoteClientInfo clientInfo ) {
        String ip   = clientInfo.getRemoteAddress().getHostAddress();
        String name = clientInfo.getRequestUserAgent();
        String mac  = "";
        try {
            mac = resolveIpAddress( ip );

            if( mac != null ) {
                User user = findUserByMAC( Sanitize.mac(mac) );
                if( user != null ) {
                    Integer[] ids = new Integer[ user.getGroups().size() ];
                    for( int i=0; i < user.getGroups().size(); i++ ) {
                        ids[i] =  user.getGroups().get(i).getId();
                    }
                    return ids;
                }
            }
            return new Integer[]{};
        }catch( Exception e ){
            throw new RuntimeException(e);
        }
    }   
}