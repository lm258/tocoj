/**
 * © Copyright Lewis Maitland 2015
 * This file is part of Toco.
 * 
 * Toco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Toco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Toco.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.toco.mediaserver;

import org.sormula.Database;
import org.toco.persistence.SQLRepository;
import org.fourthline.cling.support.model.item.Item;
import org.fourthline.cling.support.model.container.Container;
import org.fourthline.cling.support.model.Res;
import org.restexpress.common.query.*;
import org.toco.Constants;
import org.fourthline.cling.support.model.DIDLContent;
import java.util.*;
import org.toco.persistence.GroupFilter;
import java.lang.reflect.Field;

/**
 * This class is intended to bring a generic handler
 * that can be used to provide class specific media
 * container persistence.
 * @author Lewis Maitland
*/
public class MediaContainer extends Container implements DIDLContainer {

    public static final Class CLASS = new Class("object.container.storageFolder");
    private SQLRepository containerHandler;
    private SQLRepository itemHandler;

    /**
     * Generic MediaContainer Constructor is
     * used to create the connections to the database.
    */
    public MediaContainer( String name, Database database, java.lang.Class containerClass, java.lang.Class itemClass ) throws Exception {
        super("1", "0", name, Constants.UPnP.TOCO_CREATOR, CLASS, null);

        if( containerClass != null ){
            containerHandler = new SQLRepository( database, containerClass );
        }

        if( itemClass != null ) {
            itemHandler = new SQLRepository( database, itemClass );
        }
        setId( getDIDLId() );
    }

    /**
     * This constructor allows only item handlers to be present
     * in this mediacontainer.
    */
    public MediaContainer( String name, Database database, java.lang.Class itemClass ) throws Exception {
        this( name, database, null , itemClass );
    }

    public QueryFilter copyFilter( QueryFilter filter ){
        try {
            Field filtersField = QueryFilter.class.getDeclaredField( "filters" );
            filtersField.setAccessible(true);
            List newList = new ArrayList( (List)filtersField.get( filter ) );
            return new QueryFilter( newList );
        } catch( Exception e ) {
            return new QueryFilter();
        }
    }

    /**
     * This function is used to fetch the subcontainers for this
     * container object. This will usually involve getting the
     * Albums that are contained inside. In the future 
     * @return List<Container> returns a list of container
    */
    public List<Container> getContainers( QueryFilter filter ) {
    List<Container> albumContainers = new ArrayList<Container>();

        if( containerHandler == null ){
            return albumContainers;
        }
        List albums = containerHandler.readAll( null, null, null );
        
        for( Object album : albums ) {
            QueryFilter itemFilter = copyFilter( filter );
            itemFilter.addCriteria( "albumId", FilterOperator.EQUALS, ((DIDLContainer)album).getId() );
            List items = itemHandler.readAll( itemFilter, null, null );
            ((DIDLContainer)album).setItems( items );
            albumContainers.add( ((DIDLContainer)album).toContainer( this ) );
        }
        return albumContainers;
    }

    /** 
     * Get all items that are not in the list.
     * 
    */
    public List<Item> getItems( QueryFilter filter ) {
        List<Item> itemList = new ArrayList<Item>();

        if( itemHandler == null ){
            return itemList;
        }

        QueryFilter queryFilter = copyFilter( filter );
        if( containerHandler != null ){
            queryFilter.addCriteria( "albumId", FilterOperator.EQUALS, null);
        }

        List items = itemHandler.readAll( queryFilter, null, null );
        for( Object item : items ){
            itemList.add( ((DIDLItem)item).toItem(this) );
        }
        return itemList;
    }

    /**
     * getDIDLId returns the DIDL Id for this
     * MediaContainer.
     * @return String returns the DIDL id
    */
    @Override
    public String getDIDLId() {
        return Integer.toString( new String( getTitle() ).hashCode() );
    }

    /**
     * Converts this MediaContainer to
     * a normal container.
     * @return Container this object as a container
    */
    @Override
    public Container toContainer( Container parent ) {
        return this;
    }

    public void setItems( List items ){
    }

    /**
     * findById will find the object or return the container
     * itself if the id is a match for that object
     * @param String id is the id of the object
     * @param Container parent the parent of this container
     * @param DIDLContant the didl contant to add the items to
    */
    @Override
    public void findById( String id, Container parent, DIDLContent didl, QueryFilter filter ){

        if( id.equals(getDIDLId()) ) {
            didl.setContainers( getContainers( filter ) );
            didl.setItems( getItems( filter ) );
            return;
        }

        List<Container> conts = getContainers( filter );
        for( Container cont : conts ){
            if( cont.getId().equals(id) ){
                didl.setItems( cont.getItems() );
            }
        }
    }
}