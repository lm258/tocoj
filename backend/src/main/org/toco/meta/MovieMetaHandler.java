/**
 * © Copyright Lewis Maitland 2015
 * This file is part of Toco.
 * 
 * Toco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Toco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Toco.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.toco.meta;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Map;
import java.net.URLEncoder;
import java.net.URL;
import org.toco.domain.Movie;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * This class is used to populate a movie object
 * with the relevant data from the OMDB API.
 * OMDB is an open darabase api allowing the
 * retreival of ovie information.
*/
public class MovieMetaHandler implements MetaHandler<Movie> {

    /**
     * populateMovie will fetch movie information from the OMBD and then
     * populate the corresponding movie with the correct details.
    */
    public void populateObject( Movie movie, String imdbId ) {
        try{
            ObjectMapper mapper = new ObjectMapper();
            InputStream input = new URL("http://www.omdbapi.com/?i=" + URLEncoder.encode(imdbId, "UTF-8") + "&plot=short&r=json").openStream();
            Map<String, String> data = mapper.readValue( new InputStreamReader(input, "UTF-8"), Map.class);

            if( data.get("Response").compareTo("True") != 0 ) {
                throw new RuntimeException( "Incorrect IMDB ID Supplied" );
            } else {
                movie.setYear( Integer.parseInt(data.get("Year")) );
                movie.setPlot( data.get("Plot") );
                movie.setTitle( data.get("Title") );
                movie.setRuntime( Integer.parseInt(data.get("Runtime").replaceAll("[^\\d.]", "")) );
                movie.setIcon( data.get("Poster") );
                movie.setRating( data.get("Rating") );
                movie.setGenre( data.get("Genre") );
                movie.setCountry( data.get("Country") );
                movie.setDirector( data.get("Director") );
                movie.setMetaScore( Integer.parseInt(data.get("Metascore")) );
            }
        }catch( Exception e ) {
            throw new RuntimeException( e );
        }

    }
}