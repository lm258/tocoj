/**
 * © Copyright Lewis Maitland 2015
 * This file is part of Toco.
 * 
 * Toco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Toco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Toco.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.toco.persistence;

import java.util.*;

/**
 * GroupFilter is used to apply group based filtering
 * to database search queries. This object can be
 * passed into the query builder to generate queries
 * that will use such filters. This is faster than
 * selectAll'ing and then manually checking the groups
 * in the results.
 * @author Lewis Maitland
*/
public class GroupFilter {

    private List<Object> ids;
    private String group;
    private String field;
    private String compare = "id";

    public GroupFilter( String field, String group, Integer... ids ){
        this.field = field;
        this.group = group;
        this.ids = new ArrayList<Object>( Arrays.asList(ids) );
    }

    public GroupFilter( String field, String group, String compare, Integer... ids ){
        this.field = field;
        this.group = group;
        this.compare = compare;
        this.ids = new ArrayList<Object>( Arrays.asList(ids) );
    }

    public void setIds( Integer[] ids ){
        this.ids = new ArrayList<Object>( Arrays.asList(ids) );
    }

    public String getField() {
        return field;
    }

    public String getGroup(){
        return group;
    }

    public String getCompare() {
        return compare;
    }
    
    public Object[] getIds() {
        return ids.toArray();
    }
}