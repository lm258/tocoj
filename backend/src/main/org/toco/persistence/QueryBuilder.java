/**
 * © Copyright Lewis Maitland 2015
 * This file is part of Toco.
 * 
 * Toco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Toco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Toco.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.toco.persistence;

import org.restexpress.common.query.QueryFilter;
import org.restexpress.common.query.QueryOrder;
import org.restexpress.common.query.QueryRange;
import org.restexpress.common.query.*;
import org.toco.annotation.GroupsJoin;
import java.util.*;
import org.restexpress.common.util.StringUtils;
import java.lang.StringBuilder;
import java.lang.reflect.Field;

/**
 * QueryBuilder is used to build SQL queries
 * for use with sormula based on the RestExpress
 * query objects.
 * NOTE: Vulnrabilities DO exist within this peice of
 * code that need addressing. Escaping AKA prepared statements
 * should be used!!
 * @author Lewis Maitland
*/
public class QueryBuilder {

    public static class ComponentIterator<E> implements OrderCallback, FilterCallback {
        List<E> compontents = new ArrayList<E>();

        public void orderBy( OrderComponent component ) {
            compontents.add( (E)component );
        }

        public void filterOn( FilterComponent component ){
            compontents.add( (E)component );
        }

        public List<E> getComponents(){
            return compontents;
        }
    }

    public static String enumToOperator( FilterOperator operator ) {
        switch( operator ){
            case NOT_EQUALS:               return "<> ?";
            case STARTS_WITH:              return "LIKE ? || '%'";
            case CONTAINS:                 return "LIKE '%' || ? || '%'";
            case LESS_THAN:                return "< ?";
            case LESS_THAN_OR_EQUAL_TO:    return "<= ?";
            case GREATER_THAN:             return "> ?";
            case GREATER_THAN_OR_EQUAL_TO: return ">= ?";
            default:                       return "= ?";
        }
    }

    /**
     * This is a helper function used to check if a class
     * has a field
    */
    public static boolean hasField( Class e, String field ) {
        try {
            e.getDeclaredField( field );
            return true;
        } catch( Exception ex ) {
            return false;
        }
    }


    /**
     * Pass stringbulder and an array by refrence to store the query and the corrosponding
     * prepared statement objects
    */
    public static void buildQuery( Class rowClass, QueryFilter filter, QueryRange range, QueryOrder order, StringBuilder sb, List<Object> preparedList ) throws Exception {
        /**
         * This part is used to handle the filtering
        */
        if( filter != null ) {
            ComponentIterator<FilterComponent> filterIterator = new ComponentIterator<FilterComponent>();
            filter.iterate( filterIterator );
            List<FilterComponent> filterList = filterIterator.getComponents();
            StringBuilder sbFilter = new StringBuilder();
            String append = "";

            for( FilterComponent filterComp : filterList ){
                sbFilter.append(append);
                append = " AND ";
                if( filterComp.getField().equals("groups") ) {
                    if( hasField( rowClass, "groups" ) ) {
                        Field groupField     = rowClass.getDeclaredField( "groups" );
                        GroupsJoin groupJoin = groupField.getAnnotation( GroupsJoin.class );
                        sbFilter.append(
                            " id IN ( SELECT `"+ groupJoin.foreignKey() +"` FROM `"+groupJoin.joinClass().getSimpleName()
                            +"` WHERE groupId IN ("+ StringUtils.join(",", (Object [])filterComp.getValue() ) +") )"
                        );
                    } else {
                        append = "";
                    }
                } else if( filterComp.getValue() == null ) {
                    sbFilter.append(" `" + filterComp.getField() + "` IS NULL");
                } else {
                    sbFilter.append(" `" + filterComp.getField() + "` " + enumToOperator(filterComp.getOperator()) );
                    preparedList.add( filterComp.getValue() );
                }
            }
            if( sbFilter.length() > 0 ){
                sb.append( "WHERE " );
                sb.append( sbFilter );
            }
        }

        /**
         * This part is used to handle the ordering
        */
        if( order != null ) {
            ComponentIterator<OrderComponent> orderIterator = new ComponentIterator<OrderComponent>();
            order.iterate( orderIterator );
            List<OrderComponent> orderList = orderIterator.getComponents();

            if( orderList.size() > 0 ) {
                sb.append(" ORDER BY");
            }
            String append = "";
            for( OrderComponent orderCom : orderList ){
                sb.append(append);
                append = ", ";
                sb.append(" `" + orderCom.getFieldName() + ((orderCom.isAscending()) ? "` ASC" : " DESC"));
            }
        }

        /**
         * This section is used for adding the range onto the query (If it exists!)
        */
        if( range != null ) {
            if( range.hasLimit() ){
                sb.append(" LIMIT ?");
                preparedList.add( range.getLimit() );
            }
            if( range.hasOffset() ) {
                sb.append(" OFFSET ?");
                preparedList.add( range.getOffset() );
            }
        }
    }
}