/**
 * © Copyright Lewis Maitland 2015
 * This file is part of Toco.
 * 
 * Toco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Toco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Toco.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.toco.persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.List;
import java.util.ArrayList;
import java.lang.reflect.Field;
import org.toco.domain.Group;
import org.sormula.Database;
import org.sormula.Table;
import com.strategicgains.repoexpress.domain.Identifier;
import org.restexpress.common.query.QueryFilter;
import org.restexpress.common.query.QueryOrder;
import org.restexpress.common.query.QueryRange;
import org.restexpress.common.query.FilterOperator;
import java.lang.StringBuilder;
import org.toco.persistence.QueryBuilder;
import org.toco.annotation.GroupsJoin;
import org.restexpress.common.util.StringUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class is used to create a database connection.
 * The class will be used as a parent for objects who want to
 * connect to the database. It will offer helper functions to
 * quickly save and retreive data.
 * @author Lewis Maitland
*/
public class SQLRepository<R> extends Table<R> {
  
    private static final String SERVICE_NAME = "SQLRepository: ";
    private static final Logger LOG = LoggerFactory.getLogger(SERVICE_NAME);

    /**
     * Instantiates the database object for storing, retreiving
     * and manipulating classes.
     * @param database - The name of the database to connect to]
     * @exception Throws an excpetion if it couldnt connect to database
    */
    public SQLRepository( Database database, Class type ) throws Exception {
        super(database, type);
    }

    /**
     * This function is used to create a new
     * entity in the database.
     * @return R new object
     * @thorws RuntimeException if object did not save
    */
    public R create(R entity)
    {
        try {
            this.save( entity );
            if( hasField( getRowClass(), "groups") ){
                Field idField = getRowClass().getDeclaredField( "id" );
                idField.setAccessible(true);
                Field groupField = getRowClass().getDeclaredField( "groups" );
                groupField.setAccessible(true);
                GroupsJoin groupJoin = groupField.getAnnotation( GroupsJoin.class );
                Table joinTable    = getDatabase().getTable( groupJoin.joinClass() );
                Table groupTable   = getDatabase().getTable( Group.class );
                List<Group> groups = (List)groupField.get( entity );
                List groupJoinList = new ArrayList();
                if( groups != null ) {
                    for( Group group : groups ){
                        Object groupObject = groupJoin.joinClass().newInstance();
                        Field groupFieldId = groupJoin.joinClass().getDeclaredField( "groupId" );
                        groupFieldId.setAccessible(true);
                        Field groupFieldFK = groupJoin.joinClass().getDeclaredField( groupJoin.foreignKey() );
                        groupFieldFK.setAccessible(true);
                        groupFieldId.set( groupObject, group.getId() );
                        groupFieldFK.set( groupObject, idField.get( entity ) );
                        groupJoinList.add( groupObject );
                    }
                    joinTable.saveAll( groupJoinList );
                }
            }
            return entity;
        } catch( Exception e ) {
            throw new RuntimeException(e);
        }
    }

    /**
     * This function is used to read an
     * entity from the database.
     * @return R read object
     * @thorws RuntimeException if object could not be read
    */
    public R read(int id)
    {
        try {
            R obj = this.select( id );

            if( hasField( getRowClass(), "groups" ) ) {
                Field groupField = getRowClass().getDeclaredField( "groups" );
                groupField.setAccessible(true);
                GroupsJoin groupJoin = groupField.getAnnotation( GroupsJoin.class );
                String joinTable = getDatabase().getTable( groupJoin.joinClass() ).getTableName();
                Table groupTable = getDatabase().getTable( Group.class );
                groupField.set(
                    obj,
                    groupTable.selectAllCustom(
                        "WHERE id IN (SELECT groupId FROM "+ joinTable +" WHERE "+groupJoin.foreignKey()+"=?)", 
                        new Object[] {id}
                    )
                );
            }
            return obj;
        }catch( Exception e ){
            throw new RuntimeException(e);
        }
    }

    /**
     * This function is used to update an
     * entity in the database.
     * @return int objects effected
     * @thorws RuntimeException if object did not save
    */
    @Override
    public int update(R entity)
    {
        try{
            super.update( entity );
            if( hasField( getRowClass(), "groups" ) ) {
                Field idField = getRowClass().getDeclaredField( "id" );
                idField.setAccessible(true);
                Field groupField = getRowClass().getDeclaredField( "groups" );
                groupField.setAccessible(true);
                GroupsJoin groupJoin = groupField.getAnnotation( GroupsJoin.class );

                Table joinTable    = getDatabase().getTable( groupJoin.joinClass() );
                List  groups     = joinTable.selectAllCustom(
                    "WHERE "+groupJoin.foreignKey()+" = ?",
                    new Object[]{ idField.get( entity ) }
                );
                joinTable.deleteAll( groups );

                List<Group> groupz = (List)groupField.get( entity );
                List groupJoinList = new ArrayList();
                if( groupz != null ) {
                    for( Group group : groupz ){
                        Object groupObject = groupJoin.joinClass().newInstance();
                        Field groupFieldId = groupJoin.joinClass().getDeclaredField( "groupId" );
                        groupFieldId.setAccessible(true);
                        Field groupFieldFK = groupJoin.joinClass().getDeclaredField( groupJoin.foreignKey() );
                        groupFieldFK.setAccessible(true);
                        groupFieldId.set( groupObject, group.getId() );
                        groupFieldFK.set( groupObject, idField.get( entity ) );
                        groupJoinList.add( groupObject );
                    }
                    joinTable.saveAll( groupJoinList );
                }
            }
        } catch( Exception e ) {
            throw new RuntimeException(e);
        }
        return 0;
    }

    /**
     * This function is used to delete an
     * entity from the database.
     * @param Integer id
     * @thorws RuntimeException if object did not delete
    */
    public void delete(int id)
    {
        try {
            super.delete( id );
            if( hasField( getRowClass(), "groups" ) ) {
                Field groupField     = getRowClass().getDeclaredField( "groups" );
                GroupsJoin groupJoin = groupField.getAnnotation( GroupsJoin.class );

                Table groupTable = getDatabase().getTable( groupJoin.joinClass() );
                List  groups     = groupTable.selectAllCustom(
                    "WHERE "+groupJoin.foreignKey()+" = ?",
                    new Object[]{ id }
                );
                groupTable.deleteAll( groups );
            }
        }catch( Exception e ){
            throw new RuntimeException(e);
        }
    }

    /**
     * This function is used to read a multiple
     * entities from the database.
     * @return List<R> entities
     * @thorws RuntimeException if objects could not be read
    */
    public List<R> readAll( QueryFilter filter, QueryRange range, QueryOrder order )
    {
        try{
            StringBuilder sb = new StringBuilder();
            List<Object>  ol = new ArrayList<Object>();
            QueryBuilder.buildQuery( getRowClass(), filter, range, order, sb, ol);
            return this.selectAllCustom( sb.toString(), ol.toArray() );
        }catch( Exception e ){
            throw new RuntimeException( e );
        }
    }

    /**
     * This function is used to count the entities
     * in the database given a query
     * @return long number of entities
     * @thorws RuntimeException if database query failed
    */
    public long count(QueryFilter filter)
    {
        try {
            return this.selectCount();
        }catch( Exception e ){
            throw new RuntimeException(e);
        }
    }

    /**
     * This is a helper function used to check if a class
     * has a field
    */
    public boolean hasField( Class e, String field ) {
        try{
            e.getDeclaredField( field );
            return true;
        }catch( Exception ex ){
            return false;
        }
    }

}