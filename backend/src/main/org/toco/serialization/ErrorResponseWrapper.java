/**
 * © Copyright Lewis Maitland 2015
 * This file is part of Toco.
 * 
 * Toco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Toco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Toco.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.toco.serialization;

import org.restexpress.domain.ErrorResult;
import org.restexpress.response.ResponseWrapper;
import org.restexpress.Response;
import org.toco.domain.ValidationErrorResult;
import com.strategicgains.syntaxe.ValidationException;

/**
 * ErrorResponseWrapper implements the ResponseWrapper interface.
 * This class is used to check for validation exceptions and then
 * Return the appropriate validation error result. Otherwise
 * the default behavour of ErrorResponseWrapper is used.
 * @author Lewis Maitland
*/
public class ErrorResponseWrapper implements ResponseWrapper {

    @Override
    public Object wrap(Response response) {

        if (addsBodyContent(response)) {
            if( response.getException() instanceof ValidationException ){
                return (ValidationErrorResult) ValidationErrorResult.fromResponse(response);
            }
            return ErrorResult.fromResponse(response);
        }
        
        return response.getBody();
    }

    @Override
    public boolean addsBodyContent(Response response) {
        if (response.hasException()) {
            return true;
        }

        int code = response.getResponseStatus().getCode();

        if (code >= 400 && code < 600) {
            return true;
        }
        
        return false;
    }
}