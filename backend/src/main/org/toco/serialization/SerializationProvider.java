/**
 * © Copyright Lewis Maitland 2015
 * This file is part of Toco.
 * 
 * Toco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Toco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Toco.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.toco.serialization;

import org.restexpress.response.ResponseWrapper;
import org.restexpress.serialization.AbstractSerializationProvider;
import org.restexpress.serialization.SerializationProcessor;
import org.restexpress.serialization.xml.XstreamXmlProcessor;
import org.restexpress.serialization.json.JacksonJsonProcessor;

/**
 * SerializationProvider is used to provide serialization
 * and deserialization to Toco. ErrorResponseWrapper for
 * handling validatio exceptions per field is added here.
 * @author Lewis Maitland
*/
public class SerializationProvider
extends AbstractSerializationProvider
{
	private static final SerializationProcessor JSON_SERIALIZER = new JacksonJsonProcessor();
	private static final SerializationProcessor XML_SERIALIZER  = new XstreamXmlProcessor();
	private static final ResponseWrapper RESPONSE_WRAPPER       = new ErrorResponseWrapper();

	public SerializationProvider()
    {
	    super();
	    add(JSON_SERIALIZER, RESPONSE_WRAPPER );
	    add(XML_SERIALIZER, RESPONSE_WRAPPER);
    }

	public static SerializationProcessor json()
	{
		return JSON_SERIALIZER;
	}

	public static SerializationProcessor xml()
	{
		return XML_SERIALIZER;
	}
}
