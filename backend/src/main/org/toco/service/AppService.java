/**
 * © Copyright Lewis Maitland 2015
 * This file is part of Toco.
 * 
 * Toco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Toco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Toco.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.toco.service;

import java.util.List;

import org.restexpress.common.query.QueryFilter;
import org.restexpress.common.query.QueryOrder;
import org.restexpress.common.query.QueryRange;

import org.toco.domain.App;
import org.toco.persistence.AppRepository;

import com.strategicgains.syntaxe.ValidationEngine;

/**
 * This is the 'service' or 'business logic' layer, where business logic, syntactic and semantic
 * domain validation occurs, along with calls to the persistence layer.
 */
public class AppService
{
    private AppRepository apps;
    
    public AppService(AppRepository apps)
    {
        super();
        this.apps = apps;
    }

    public App create(App app)
    {
        ValidationEngine.validateAndThrow(app);
        return apps.create(app);
    }

    public App read(int id)
    {
        return apps.read(id);
    }

    public void update(App app)
    {
        ValidationEngine.validateAndThrow(app);
        apps.update(app);
    }

    public void delete(int id)
    {
        apps.delete(id);
    }

    public List<App> readAll(QueryFilter filter, QueryRange range, QueryOrder order)
    {
        return apps.readAll(filter, range, order);
    }

    public long count(QueryFilter filter)
    {
        return apps.count(filter);
    }
}
