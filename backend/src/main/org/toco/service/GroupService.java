/**
 * © Copyright Lewis Maitland 2015
 * This file is part of Toco.
 * 
 * Toco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Toco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Toco.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.toco.service;

import java.util.List;

import org.restexpress.common.query.QueryFilter;
import org.restexpress.common.query.QueryOrder;
import org.restexpress.common.query.QueryRange;

import org.toco.domain.Group;
import org.toco.persistence.GroupRepository;

import com.strategicgains.syntaxe.ValidationEngine;

/**
 * This is the 'service' or 'business logic' layer, where business logic, syntactic and semantic
 * domain validation occurs, along with calls to the persistence layer.
 */
public class GroupService
{
    private GroupRepository groups;
    
    public GroupService(GroupRepository groups)
    {
        super();
        this.groups = groups;
    }

    public Group create(Group group)
    {
        ValidationEngine.validateAndThrow(group);
        return groups.create(group);
    }

    public Group read(int id)
    {
        return groups.read(id);
    }

    public void update(Group group)
    {
        ValidationEngine.validateAndThrow(group);
        groups.update(group);
    }

    public void delete(int id)
    {
        groups.delete(id);
    }

    public List<Group> readAll(QueryFilter filter, QueryRange range, QueryOrder order)
    {
        return groups.readAll(filter, range, order);
    }

    public long count(QueryFilter filter)
    {
        return groups.count(filter);
    }
}
