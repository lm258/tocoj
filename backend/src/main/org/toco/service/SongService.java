/**
 * © Copyright Lewis Maitland 2015
 * This file is part of Toco.
 * 
 * Toco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Toco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Toco.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.toco.service;

import java.util.List;

import org.restexpress.common.query.QueryFilter;
import org.restexpress.common.query.QueryOrder;
import org.restexpress.common.query.QueryRange;

import org.toco.domain.Song;
import org.toco.persistence.SongRepository;

import com.strategicgains.syntaxe.ValidationEngine;

/**
 * This is the 'service' or 'business logic' layer, where business logic, syntactic and semantic
 * domain validation occurs, along with calls to the persistence layer.
 */
public class SongService
{
    private SongRepository songs;
    
    public SongService(SongRepository songs)
    {
        super();
        this.songs = songs;
    }

    public Song create(Song song)
    {
        ValidationEngine.validateAndThrow(song);
        return songs.create(song);
    }

    public Song read(int id)
    {
        return songs.read(id);
    }

    public void update(Song song)
    {
        ValidationEngine.validateAndThrow(song);
        songs.update(song);
    }

    public void delete(int id)
    {
        songs.delete(id);
    }

    public List<Song> readAll(QueryFilter filter, QueryRange range, QueryOrder order)
    {
        return songs.readAll(filter, range, order);
    }

    public long count(QueryFilter filter)
    {
        return songs.count(filter);
    }
}
