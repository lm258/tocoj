/**
 * © Copyright Lewis Maitland 2015
 * This file is part of Toco.
 * 
 * Toco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Toco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Toco.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.toco;

import static org.junit.Assert.assertEquals;
import org.toco.domain.*;
import org.jboss.netty.handler.codec.http.DefaultHttpRequest;
import org.jboss.netty.handler.codec.http.HttpMethod;
import org.jboss.netty.handler.codec.http.HttpRequest;
import org.jboss.netty.handler.codec.http.HttpVersion;
import org.restexpress.*;
import org.restexpress.serialization.DefaultSerializationProvider;
import java.util.*;
import org.toco.Constants.*;

/**
 * Helper class used for various tasks such as creating requests.
 * assetEquals typical toco objects etc.
*/
public class Helper {

    /**
     * Helper function to make a request
    */
    public static Request makeRequest(HttpMethod method, String url) {
        HttpRequest httpRequest = new DefaultHttpRequest(HttpVersion.HTTP_1_1, method, url);
        return new Request(httpRequest, null, new DefaultSerializationProvider() );
    }

    /**
     * This function is used to build a group
    */
    public static Group makeGroup( String name ) {
        Group group = new Group();
        group.setName( name );
        return group;
    }

     /**
     * This function is used to build a group with id
    */
    public static Group makeGroup( Integer id, String name ) {
        Group group = makeGroup( name );
        group.setId( id );
        return group;
    }

    /**
     * This function is used to assert a group
    */
    public static void assertEqualsGroup( Group one, Group two, Boolean compareId ){
        if( compareId ){
            assertEquals( "Group Id:", one.getId(), two.getId() );
        }
        assertEquals( "Group Name:",one.getName(), two.getName() );
    }

    /**
     * Helper class to make a device
    */
    public static Device makeDevice( String name, String mac, DeviceType type ) {
        Device device = new Device();
        device.setName(name);
        device.setType(type);
        device.setMac(mac);
        return device;
    }

    /**
     * Make a device with ID
    */
    public static Device makeDevice( Integer id, String name, String mac, DeviceType type ){
        Device device = makeDevice( name, mac, type );
        device.setId( id );
        return device;
    }

    public static Movie makeMovie(
        String title,
        Integer year,
        String file,
        String plot,
        String rating,
        String genre,
        String country,
        Integer metaScore,
        Integer runtime,
        String director,
        String imdbID,
        String icon,
        String mimeType,
        Long size,
        Long bitrate,
        Long length,
        List<Group> groups) {
        Movie movie = new Movie();
        movie.setTitle( title );
        movie.setYear( year );
        movie.setFile( file );
        movie.setPlot( plot );
        movie.setRating( rating );
        movie.setGenre( genre);
        movie.setCountry( country );
        movie.setMetaScore( metaScore );
        movie.setRuntime( runtime );
        movie.setDirector( director );
        movie.setImdbID( imdbID );
        movie.setIcon( icon );
        movie.setMimeType( mimeType );
        movie.setSize( size );
        movie.setBitrate( bitrate );
        movie.setLength( length );
        movie.setGroups( groups );
        return movie;
    }
    
    public static Movie makeMovie(
        String title,
        Integer year,
        String file,
        String plot,
        String rating,
        String genre,
        List<Group> groups) {
        Movie movie = makeMovie( title, year, file, plot, rating, genre, "UK", 1, 1, "", "", "", "", 1L, 1L, 1L, groups );
        return movie;
    }

    public static Song makeSong(
        String artist,
        String genre,
        String title,
        Integer year,
        String file,
        String icon,
        Integer albumId,
        String mimeType,
        Long size,
        Long bitrate,
        Long length,
        List<Group> groups ) {
        Song song = new Song();
        song.setArtist( artist );
        song.setGenre( genre );
        song.setTitle( title );
        song.setYear( year );
        song.setFile( file );
        song.setIcon( icon );
        song.setAlbumId( albumId );
        song.setMimeType( mimeType );
        song.setSize( size );
        song.setBitrate( bitrate );
        song.setLength( length );
        song.setGroups( groups );
        return song;
    }

    public static Song makeSong(
        String title,
        Integer albumId,
        List<Group> groups ) {
        Song song = makeSong( "", "", title, 0, "", "", albumId, "", 1L, 1L, 1L, groups );
        return song;
    }

    public static Picture makePicture(
        String name,
        Date date,
        String file,
        String description,
        Integer albumId,
        String mimeType,
        Long size,
        List<Group> groups
    ) {
        Picture picture = new Picture();
        picture.setName( name );
        picture.setDate( null );
        picture.setFile( file );
        picture.setDescription( description );
        picture.setAlbumId( albumId );
        picture.setMimeType( mimeType );
        picture.setSize( size );
        picture.setGroups( groups );
        return picture;
    }

    public static Picture makePicture(
        String name,
        Integer albumId,
        List<Group> groups ) {
        Picture picture = makePicture(  name, null, "", "", albumId, "", 1L, groups );
        return picture;
    }

    /**
     * Helper function to assert a device
    */
    public static void assertEqualsDevice( Device one, Device two, Boolean compareId ){
        if( compareId ){
            assertEquals( "Device Id", one.getId(), two.getId() );
        }
        assertEquals( "Device Name: ", one.getName(), two.getName() );
        assertEquals( "Device MAC: ", one.getMac(), two.getMac() );
        assertEquals( "Device Type: ", one.getType(), two.getType() );
    }

    /**
     * Helper function used to create a user for testing
    */
    public static User makeUser( String username, String password, String realname, UserType type ) {
        User user = new User();
        user.setUsername( username );
        user.setPassword( password );
        user.setRealname( realname );
        user.setType( type );
        return user;
    }

    /**
     * Make a user with an ID
    */
    public static User makeUser( Integer id, String username, String password, String realname, UserType type ){
        User user = makeUser( username, password, realname, type );
        user.setId( id );
        return user;
    }

    /**
     * Make a user with devices and groups
    */
    public static User makeUser( String username, String password, String realname, UserType type, List<Device> devices, List<Group> groups ){
        User user = makeUser( username, password, realname, type );
        user.setDevices( devices );
        user.setGroups( groups );
        return user;
    }

    /**
     * Assert equals a user
    */
    public static void assertEqualsUser( User one, User two, Boolean compareId ){
        if( compareId ) {
            assertEquals( "User Id: ", one.getId(), two.getId() );
        }
        assertEquals( "Username: ", one.getUsername(), two.getUsername() );
        assertEquals( "Password: ", one.getPassword(), two.getPassword() );
        assertEquals( "Realname: ", one.getRealname(), two.getRealname() );
        assertEquals( "Type: ", one.getType(), two.getType() );
    }

    /**
     * Generate user groups as an array
    */
    public static Integer[] userGroupsAsArray( User user ) {
        Integer[] ids = new Integer[ user.getGroups().size() ];
        for( int i=0; i < user.getGroups().size(); i++ ) {
            ids[i] =  user.getGroups().get(i).getId();
        }
        return ids;
    }

    /**
     * Make a picture album
    */
    public static PictureAlbum makePictureAlbum(String name) {
        PictureAlbum album = new PictureAlbum();
        album.setName( name );
        return album;
    }

    /**
     * Make a picture album with id
    */
    public static PictureAlbum makePictureAlbum(Integer id, String name) {
        PictureAlbum album = new PictureAlbum();
        album.setName( name );
        album.setId( id );
        return album;
    }
}