/**
 * © Copyright Lewis Maitland 2015
 * This file is part of Toco.
 * 
 * Toco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Toco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Toco.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.toco.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.toco.Helper.*;

import org.junit.*;
import org.junit.rules.ExpectedException;
import org.restexpress.serialization.SerializationProcessor;
import org.restexpress.serialization.json.JacksonJsonProcessor;
import org.toco.controller.*;
import org.toco.persistence.*;
import org.toco.service.*;
import org.toco.domain.*;
import java.sql.*;
import org.restexpress.*;
import org.toco.Constants;
import org.toco.Constants.*;
import org.sormula.Database;
import org.jboss.netty.handler.codec.http.HttpMethod;
import org.jboss.netty.buffer.ByteBufferBackedChannelBuffer;
import java.util.*;
import org.sormula.Table;
import com.strategicgains.syntaxe.ValidationException;

public class DeviceControllerTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private static Database database;
    private static SerializationProcessor processor = new JacksonJsonProcessor();
    private static DeviceController deviceController;
    private static DeviceRepository deviceRepository;
    private static final String GOOD_MAC = "112211221122";

    @BeforeClass
    public static void beforeClass() throws Exception {
        Class.forName("org.sqlite.JDBC");
        Connection connection = DriverManager.getConnection("jdbc:sqlite:test.db");
        database = new Database( connection );
        deviceRepository = new DeviceRepository( database );
        DeviceService deviceService = new DeviceService( deviceRepository );
        deviceController = new DeviceController( deviceService );
    }

    @AfterClass
    public static void afterClass() {
        database.close();
    }

    @Before
    public void beforeEach() {
        thrown.none();
    }


    @After
    public void afterEach() throws Exception {
        deviceRepository.deleteAll();
    }

    @Test 
    public void validateName() {
        thrown.expect( ValidationException.class );
        Response response = new Response();
        Request request   = makeRequest(HttpMethod.POST, "/api/devices/index.json");
        Device device     = makeDevice( null, GOOD_MAC, DeviceType.DESKTOP );

        /*Create device*/
        request.setBody( new ByteBufferBackedChannelBuffer( processor.serialize(device) ) );
        deviceController.create( request, response );
    }

    @Test 
    public void validateBadMac() {
        thrown.expect( ValidationException.class );
        Response response = new Response();
        Request request   = makeRequest(HttpMethod.POST, "/api/devices/index.json");
        Device device     = makeDevice( "test", "a", DeviceType.DESKTOP );

        /*Create device*/
        request.setBody( new ByteBufferBackedChannelBuffer( processor.serialize(device) ) );
        deviceController.create( request, response );
    }

    @Test 
    public void validateNoMac() {
        thrown.expect( ValidationException.class );
        Response response = new Response();
        Request request   = makeRequest(HttpMethod.POST, "/api/devices/index.json");
        Device device     = makeDevice( "test", null, DeviceType.DESKTOP );

        /*Create device*/
        request.setBody( new ByteBufferBackedChannelBuffer( processor.serialize(device) ) );
        deviceController.create( request, response );
    }

    @Test 
    public void create() {
        Response response = new Response();
        Request request   = makeRequest(HttpMethod.POST, "/api/devices/index.json");
        Device device     = makeDevice( "test", GOOD_MAC, DeviceType.DESKTOP );

        /*Create device*/
        request.setBody( new ByteBufferBackedChannelBuffer( processor.serialize(device) ) );
        Device after = deviceController.create( request, response );
        assertEqualsDevice( device, after, false );
    }

    @Test 
    public void createDuplicateName() {
        thrown.expect( RuntimeException.class );
        Response response = new Response();
        Request request   = makeRequest(HttpMethod.POST, "/api/devices/index.json");
        Device device     = makeDevice( "test", GOOD_MAC, DeviceType.DESKTOP );

        /*Create device*/
        request.setBody( new ByteBufferBackedChannelBuffer( processor.serialize(device) ) );
        deviceController.create( request, response );

        device.setMac( "112211221121" );
        request.setBody( new ByteBufferBackedChannelBuffer( processor.serialize(device) ) );
        deviceController.create( request, response );
    }

    @Test 
    public void createDuplicateMac() {
        thrown.expect( RuntimeException.class );
        Response response = new Response();
        Request request   = makeRequest(HttpMethod.POST, "/api/devices/index.json");
        Device device     = makeDevice( "test", GOOD_MAC, DeviceType.DESKTOP );

        /*Create device*/
        request.setBody( new ByteBufferBackedChannelBuffer( processor.serialize(device) ) );
        deviceController.create( request, response );

        device.setName("test2");
        request.setBody( new ByteBufferBackedChannelBuffer( processor.serialize(device) ) );
        deviceController.create( request, response );
    }


    @Test 
    public void read() throws Exception {
        Response response = new Response();
        Request request   = makeRequest(HttpMethod.GET, "/api/devices/index.json");
        Device device     = makeDevice( 1, "test", GOOD_MAC, DeviceType.DESKTOP );
        deviceRepository.insertNonIdentity( device );

        request.addHeader( "deviceId", "1" );
        Device read = deviceController.read( request, response );
        assertEqualsDevice( device, read, true );
    }

    @Test 
    public void readMultiple() throws Exception {
        Response response = new Response();
        Request request   = makeRequest(HttpMethod.GET, "/api/devices/index.json");

        List<Device> devices     = new ArrayList<Device>();
        List<Device> devicesRead = new ArrayList<Device>();
        for(int i=0; i < 8; i++ ){
            devices.add( makeDevice( i+1, "test"+i, "11221122112"+(i+1), DeviceType.DESKTOP ) );
        }
        deviceRepository.insertNonIdentityAll( devices );

        devicesRead = deviceController.readAll( request, response );
        for(int i = 0; i< 8; i++ ){
            assertEqualsDevice( devices.get(i), devicesRead.get(i), true );
        }
    }

    @Test 
    public void update() throws Exception {
        Response response = new Response();
        Request request   = makeRequest(HttpMethod.PUT, "/api/devices/index.json");
        Device device     = makeDevice( 1, "test", GOOD_MAC, DeviceType.DESKTOP );
        deviceRepository.insertNonIdentity( device );

        device.setName( "test2" );
        device.setMac("112211aa22dd");
        device.setType(DeviceType.TABLET);

        request.addHeader( "deviceId", "1" );
        request.setBody( new ByteBufferBackedChannelBuffer( processor.serialize(device) ) );
        deviceController.update( request, response );
        Device read = deviceRepository.read( 1 );

        assertEqualsDevice( device, read, true );
    }

    @Test
    public void delete() throws Exception {
        Response response = new Response();
        Request request   = makeRequest(HttpMethod.DELETE, "/api/devices/index.json");
        Device device     = makeDevice( 1, "test", GOOD_MAC, DeviceType.DESKTOP );
        deviceRepository.insertNonIdentity( device );

        request.addHeader( "deviceId", "1" );
        deviceController.delete( request, response );

        List<Device> devices = deviceRepository.selectAll();
        assertEquals( "Devices Size:", devices.size(), 0 );
    }
}