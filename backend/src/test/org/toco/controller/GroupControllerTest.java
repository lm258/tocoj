/**
 * © Copyright Lewis Maitland 2015
 * This file is part of Toco.
 * 
 * Toco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Toco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Toco.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.toco.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.toco.Helper.*;

import org.junit.*;
import org.junit.rules.ExpectedException;
import org.restexpress.serialization.SerializationProcessor;
import org.restexpress.serialization.json.JacksonJsonProcessor;
import org.toco.controller.*;
import org.toco.persistence.*;
import org.toco.service.*;
import org.toco.domain.*;
import java.sql.*;
import org.restexpress.*;
import org.toco.Constants;
import org.toco.Constants.*;
import org.sormula.Database;
import org.toco.Constants;
import org.jboss.netty.buffer.ByteBufferBackedChannelBuffer;
import java.util.*;
import org.sormula.Table;
import org.jboss.netty.handler.codec.http.HttpMethod;
import com.strategicgains.syntaxe.ValidationException;

public class GroupControllerTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private static Database database;
    private static SerializationProcessor processor = new JacksonJsonProcessor();
    private static GroupController groupController;
    private static GroupRepository groupRepository;

    @BeforeClass
    public static void beforeClass() throws Exception {
        Class.forName("org.sqlite.JDBC");
        Connection connection = DriverManager.getConnection("jdbc:sqlite:test.db");
        database = new Database( connection );
        groupRepository = new GroupRepository( database );
        GroupService groupService = new GroupService( groupRepository );
        groupController = new GroupController( groupService );
    }

    @AfterClass
    public static void afterClass() {
        database.close();
    }

    @Before
    public void beforeEach() {
        thrown.none();
    }


    @After
    public void afterEach() throws Exception {
        groupRepository.deleteAll();
    }

    @Test 
    public void validateName() {
        thrown.expect( ValidationException.class );
        Response response = new Response();
        Request request   = makeRequest(HttpMethod.POST, "/api/groups/index.json");
        Group group       = makeGroup( null );

        request.setBody( new ByteBufferBackedChannelBuffer( processor.serialize(group) ) );
        groupController.create( request, response );
    }

    @Test
    public void create() throws Exception {
        Response response = new Response();
        Request request   = makeRequest(HttpMethod.POST, "/api/groups/index.json");
        Group group       = makeGroup("test");

        request.setBody( new ByteBufferBackedChannelBuffer( processor.serialize(group) ) );
        Group after = groupController.create( request, response );

        assertEqualsGroup( group, after, false );
    }

    @Test
    public void createDuplicate() throws Exception {
        thrown.expect( RuntimeException.class );
        Response response = new Response();
        Request request   = makeRequest(HttpMethod.POST, "/api/groups/index.json");
        Group group       = makeGroup("test");

        /*Create Group*/
        request.setBody( new ByteBufferBackedChannelBuffer( processor.serialize(group) ) );
        groupController.create( request, response );
        request.setBody( new ByteBufferBackedChannelBuffer( processor.serialize(group) ) );
        groupController.create( request, response );
    }

    @Test
    public void update() throws Exception {
        Response response = new Response();
        Request request   = makeRequest(HttpMethod.PUT, "/api/groups/1.json");
        Group group       = makeGroup(1, "test");
        groupRepository.insertNonIdentity( group );
        group.setName( "new_name" );

        request.addHeader( "groupId", "1");
        request.setBody( new ByteBufferBackedChannelBuffer( processor.serialize(group) ) );
        groupController.update( request, response );
        Group afterGroup = groupRepository.read( 1 );

        assertEqualsGroup( group, afterGroup, true );
    }

    @Test
    public void delete() throws Exception {
        Response response = new Response();
        Request request   = makeRequest(HttpMethod.DELETE, "/api/groups/index.json");
        Group group       = makeGroup(1, "test");
        groupRepository.insertNonIdentity( group );

        request.addHeader( "groupId", "1");
        request.setBody( new ByteBufferBackedChannelBuffer( processor.serialize(group) ) );
        groupController.delete( request, response );

        List<Group> groups = groupRepository.selectAll();
        assertEquals( "Groups Size", groups.size(), 0 );
    }

    @Test
    public void read() throws Exception {
        Response response = new Response();
        Request request   = makeRequest(HttpMethod.GET, "/api/groups/index.json");
        Group group       = makeGroup(1, "test");
        groupRepository.insertNonIdentity( group );

        request.addHeader( "groupId", "1");
        Group after = groupController.read( request, response );

        assertEqualsGroup( group, after, true );
    }

    @Test
    public void readMultiple() throws Exception {
        Response response = new Response();
        Request request   = makeRequest(HttpMethod.GET, "/api/groups/index.json");

        List<Group> groups     = new ArrayList<Group>();
        List<Group> groupsRead;

        for(int i=0;i<20;i++){
            groups.add( makeGroup( "test"+i ) );
        }
        groupRepository.saveAll( groups );

        groupsRead = groupController.readAll( request, response );
        for(int i=0; i<20; i++){
            assertEqualsGroup( groups.get(i), groupsRead.get(i), true );
        }
    }
}