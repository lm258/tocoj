/**
 * © Copyright Lewis Maitland 2015
 * This file is part of Toco.
 * 
 * Toco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Toco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Toco.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.toco.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.toco.Helper.*;

import org.junit.*;
import org.junit.rules.ExpectedException;
import org.restexpress.serialization.SerializationProcessor;
import org.restexpress.serialization.json.JacksonJsonProcessor;
import org.toco.controller.*;
import org.toco.persistence.*;
import org.toco.service.*;
import org.toco.domain.*;
import org.toco.mediaserver.*;
import java.sql.*;
import org.restexpress.*;
import org.toco.Constants;
import org.toco.Constants.*;
import org.sormula.Database;
import org.jboss.netty.handler.codec.http.HttpMethod;
import org.jboss.netty.buffer.ByteBufferBackedChannelBuffer;
import java.util.*;
import org.sormula.Table;
import org.restexpress.common.query.*;
import com.strategicgains.syntaxe.ValidationException;
import org.toco.mediaserver.DeviceHandler;
import org.fourthline.cling.support.model.item.Item;

public class PictureContentControllerTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private static Database database;
    private static SerializationProcessor processor = new JacksonJsonProcessor();
    private static DeviceRepository deviceRepository;
    private static UserRepository userRepository;
    private static PictureRepository pictureRepository;
    private static GroupRepository groupRepository;
    private static DeviceHandler deviceHandler;

    private static final String ADULT_MAC = "a12211221122";
    private static final String CHILD_MAC = "c12211221122";

    @BeforeClass
    public static void beforeClass() throws Exception {
        Class.forName("org.sqlite.JDBC");
        Connection connection = DriverManager.getConnection("jdbc:sqlite:test.db");
        database = new Database( connection );
        deviceRepository  = new DeviceRepository( database );
        userRepository    = new UserRepository( database );
        pictureRepository = new PictureRepository( database );
        groupRepository   = new GroupRepository( database );
        deviceHandler     = new DeviceHandler( database );

        /*Add our groups*/
        groupRepository.insertNonIdentityAll(
            new ArrayList<Group>(){{
                add(makeGroup(1, "adult_g"));
                add(makeGroup(2, "common_g"));
                add(makeGroup(3, "child_g"));
            }}
        );

        /*Add our devices*/
        deviceRepository.insertNonIdentityAll(
            new ArrayList<Device>(){{
                add(makeDevice(1, "adult_d",  ADULT_MAC, DeviceType.DESKTOP));
                add(makeDevice(2, "child_d",  CHILD_MAC, DeviceType.DESKTOP));
            }}
        );

        /*Create two test users with devices*/
        User adult = makeUser(
            "adult",
            "password",
            "realname",
            UserType.USER,
            new ArrayList<Device>(){{
                add(makeDevice(1, "adult_d",  ADULT_MAC, DeviceType.DESKTOP));
            }},
            new ArrayList<Group>(){{
                add(makeGroup(1, "adult_g"));
                add(makeGroup(2, "common_g")); 
            }}
        );

        User child = makeUser(
            "child",
            "password",
            "realname",
            UserType.USER,
            new ArrayList<Device>(){{
                add(makeDevice(2, "child_d",  CHILD_MAC, DeviceType.DESKTOP));
            }},
            new ArrayList<Group>(){{ 
                add(makeGroup(3, "child_g"));
                add(makeGroup(2, "common_g"));
            }}
        );

        /*Create adult picture*/
        pictureRepository.create(
            makePicture(
                "adult_p",
                null,
                new ArrayList<Group>(){{
                    add(makeGroup(1, "adult_g"));
                }}
            )
        );

        /*Create common picture*/
        pictureRepository.create(
            makePicture(
                "common_p",
                null,
                new ArrayList<Group>(){{
                    add(makeGroup(2, "common_g"));
                }}
            )
        );

        /*Create child picture*/
        pictureRepository.create(
            makePicture(
                "child_p",
                null,
                new ArrayList<Group>(){{
                    add(makeGroup(3, "child_g"));
                }}
            )
        );

        /*Add the two users*/
        userRepository.deleteAll();
        deviceRepository.deleteAll();

        /*Create the */
        userRepository.create(child);
        userRepository.create(adult);
    }

    @AfterClass
    public static void afterClass() throws Exception {
        groupRepository.deleteAll();
        userRepository.deleteAll();
        deviceRepository.deleteAll();
        database.close();
    }

    @Before
    public void beforeEach() {
        thrown.none();
    }

    @After
    public void afterEach() throws Exception {
        //deviceRepository.deleteAll();
    }

    @Test
    public void getChildFromMAC() {
        User child = deviceHandler.findUserByMAC(CHILD_MAC);
        assertEquals( child.getUsername(), "child" );
    }

    @Test 
    public void getChildAdultMac() {
        User child = deviceHandler.findUserByMAC(ADULT_MAC);
        assertNotEquals( child.getUsername(), "child" );
    }

    @Test 
    public void getChildGroups() {
        User child    = deviceHandler.findUserByMAC(CHILD_MAC);
        Integer[] ids = userGroupsAsArray( child );

        assertEquals( "Child group one:", "child_g",  child.getGroups().get(1).getName() );
        assertEquals( "Child group two:", "common_g", child.getGroups().get(0).getName() );
        assertEquals( "Child group length: ", 2, ids.length );
    }

    @Test
    public void getAdultFromMAC() {
        User adult = deviceHandler.findUserByMAC(ADULT_MAC);
        assertEquals( adult.getUsername(), "adult" );
    }

    @Test 
    public void getAdultChildMAC() {
        User adult = deviceHandler.findUserByMAC(CHILD_MAC);
        assertNotEquals( adult.getUsername(), "adult" );
    }

    @Test 
    public void getAdultGroups() {
        User adult    = deviceHandler.findUserByMAC(ADULT_MAC);
        Integer[] ids = userGroupsAsArray( adult );

        assertEquals( "Adult group one:", "adult_g",  adult.getGroups().get(0).getName() );
        assertEquals( "Adult group two:", "common_g", adult.getGroups().get(1).getName() );
        assertEquals( "Adult group length: ", 2, ids.length );
    }

    @Test 
    public void getAllPictures() {
        List<Picture> pictures = pictureRepository.readAll( null, null, null );

        /*Make sure we have 3 pictures*/
        assertEquals( "Pictures:", 3, pictures.size() );

        /*Make sure adult picture and groups are correct*/
        assertEquals( "Pictures Adult:", "adult_p",   pictures.get(0).getName() );
        Picture adult = pictureRepository.read( pictures.get(0).getId() );
        assertEquals( "Adult Groups: ",    1, adult.getGroups().size() );
        assertEquals( "Adult Group Name:", "adult_g", adult.getGroups().get(0).getName() );

        /*Make sure common picture and groups are correct*/
        assertEquals( "Pictures Common:", "common_p",   pictures.get(1).getName() );
        Picture common = pictureRepository.read( pictures.get(1).getId() );
        assertEquals( "Common Groups: ",    1, common.getGroups().size() );
        assertEquals( "Common Group Name:", "common_g", common.getGroups().get(0).getName() );

        /*Make sure child picture and groups are correct*/
        assertEquals( "Pictures Child:", "child_p",   pictures.get(2).getName() );
        Picture child = pictureRepository.read( pictures.get(2).getId() );
        assertEquals( "Child Groups: ",    1, child.getGroups().size() );
        assertEquals( "Child Group Name:", "child_g", child.getGroups().get(0).getName() );
    }

    @Test 
    public void getChildContent() {

        /*Get child and content ids*/
        User child    = deviceHandler.findUserByMAC(CHILD_MAC);
        Integer[] ids = userGroupsAsArray( child );

        assertEquals( "Child group length: ", 2, ids.length );

        /*Query container for pictures*/
        QueryFilter filterObj = new QueryFilter();
        filterObj.addCriteria( "groups", FilterOperator.CONTAINS, ids );
        List<Picture> pictures = pictureRepository.readAll( filterObj, null, null );

        assertEquals( "Content list size:", 2, pictures.size() );
        assertEquals( "Content item name:", "child_p",  pictures.get(1).getName() );
        assertEquals( "Content item name:", "common_p", pictures.get(0).getName() );
    }

    @Test 
    public void getAdultContent() {

        /*Get child and content ids*/
        User child    = deviceHandler.findUserByMAC(ADULT_MAC);
        Integer[] ids = userGroupsAsArray( child );

        assertEquals( "Adult group length: ", 2, ids.length );

        /*Query container for pictures*/
        QueryFilter filterObj = new QueryFilter();
        filterObj.addCriteria( "groups", FilterOperator.CONTAINS, ids );
        List<Picture> pictures = pictureRepository.readAll( filterObj, null, null );

        assertEquals( "Content list size:", 2, pictures.size() );
        assertEquals( "Content item name:", "adult_p",  pictures.get(0).getName() );
        assertEquals( "Content item name:", "common_p", pictures.get(1).getName() );
    }
}