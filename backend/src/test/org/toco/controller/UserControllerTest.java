/**
 * © Copyright Lewis Maitland 2015
 * This file is part of Toco.
 * 
 * Toco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Toco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Toco.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.toco.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.toco.Helper.*;

import org.junit.*;
import org.junit.rules.ExpectedException;
import org.restexpress.serialization.SerializationProcessor;
import org.restexpress.serialization.json.JacksonJsonProcessor;
import org.toco.controller.*;
import org.toco.persistence.*;
import org.toco.service.*;
import org.toco.domain.*;
import java.sql.*;
import org.restexpress.*;
import org.toco.Constants;
import org.toco.Constants.*;
import org.sormula.Database;
import org.jboss.netty.handler.codec.http.HttpMethod;
import org.jboss.netty.buffer.ByteBufferBackedChannelBuffer;
import java.util.*;
import org.sormula.Table;
import com.strategicgains.syntaxe.ValidationException;

public class UserControllerTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private static Database database;
    private static SerializationProcessor processor = new JacksonJsonProcessor();
    private static UserController userController;
    private static UserRepository userRepository;
    private static GroupRepository groupRepository;
    private static DeviceRepository deviceReposiory;
    private static Table<UserGroup> userGroupTable;

    @BeforeClass
    public static void beforeClass() throws Exception {
        Class.forName("org.sqlite.JDBC");
        Connection connection = DriverManager.getConnection("jdbc:sqlite:test.db");
        database = new Database( connection );
        deviceReposiory = new DeviceRepository( database );
        groupRepository = new GroupRepository( database );
        userRepository = new UserRepository( database );
        UserService userService = new UserService( userRepository );
        userController = new UserController( userService );
        userGroupTable = database.getTable(UserGroup.class);
    }

    @AfterClass
    public static void afterClass() {
        database.close();
    }


    @Before
    public void beforeEach() {
        thrown.none();
    }


    @After
    public void afterEach() throws Exception {
        userRepository.deleteAll();
        groupRepository.deleteAll();
        deviceReposiory.deleteAll();
        userGroupTable.deleteAll();
    }

    @Test
    public void validateUsername(){
        thrown.expect( ValidationException.class );
        Response response = new Response();
        Request request   = makeRequest(HttpMethod.POST, "/api/users/index.json");
        User user         = makeUser( null, "password", "realname", UserType.USER);

        /*Create User*/
        request.setBody( new ByteBufferBackedChannelBuffer( processor.serialize(user) ) );
        userController.create( request, response );
    }

    @Test
    public void validatePassword(){
        thrown.expect( ValidationException.class );
        Response response = new Response();
        Request request   = makeRequest(HttpMethod.POST, "/api/users/index.json");
        User user         = makeUser( "username", null, "realname", UserType.USER);

        /*Create User*/
        request.setBody( new ByteBufferBackedChannelBuffer( processor.serialize(user) ) );
        userController.create( request, response );
    }

    @Test
    public void validateRealname(){
        thrown.expect( ValidationException.class );
        Response response = new Response();
        Request request   = makeRequest(HttpMethod.POST, "/api/users/index.json");
        User user         = makeUser( "username", "password", null, UserType.USER);

        /*Create User*/
        request.setBody( new ByteBufferBackedChannelBuffer( processor.serialize(user) ) );
        userController.create( request, response );
    }

    @Test
    public void validateGroupChild(){
        thrown.expect( ValidationException.class );
        Response response  = new Response();
        Request request    = makeRequest(HttpMethod.POST, "/api/users/index.json");
        User user          = makeUser( "username", "password", null, UserType.USER);
        List<Group> groups = new ArrayList<Group>();
        groups.add( new Group() );
        user.setGroups( groups );

        /*Create User*/
        request.setBody( new ByteBufferBackedChannelBuffer( processor.serialize(user) ) );
        userController.create( request, response );
    }

    @Test
    public void validateDeviceChild(){
        thrown.expect( ValidationException.class );
        Response response    = new Response();
        Request request      = makeRequest(HttpMethod.POST, "/api/users/index.json");
        User user            = makeUser( "username", "password", null, UserType.USER);
        List<Device> devices = new ArrayList<Device>();
        devices.add( new Device() );
        user.setDevices( devices );

        /*Create User*/
        request.setBody( new ByteBufferBackedChannelBuffer( processor.serialize(user) ) );
        userController.create( request, response );
    }

    @Test
    public void read() throws Exception {
        Response response = new Response();
        Request request   = makeRequest(HttpMethod.GET, "/api/users/1.json");
        User user         = makeUser( 1, "username", "password", "realname", UserType.USER);
        userRepository.insertNonIdentity( user );

        /*Create User*/
        request.addHeader("userId", "1" );
        User userAfter = userController.read( request, response );
        assertEqualsUser( user, userAfter, true );
    }

    @Test
    public void createDuplicate() throws Exception {
        thrown.expect( RuntimeException.class );
        Response response = new Response();
        Request request   = makeRequest(HttpMethod.POST, "/api/users/1.json");
        User user = makeUser( "username", "password", "realname", UserType.USER);

        /*Create User*/
        request.setBody( new ByteBufferBackedChannelBuffer( processor.serialize(user) ) );
        userController.create( request, response );
        request.setBody( new ByteBufferBackedChannelBuffer( processor.serialize(user) ) );
        userController.create( request, response );
    }

    @Test
    public void createUserNoGroupDevices() throws Exception {
        Response response = new Response();
        Request request   = makeRequest(HttpMethod.POST,    "/api/users/index.json");

        User user = makeUser( "username", "password", "realname", UserType.USER);

        request.setBody( new ByteBufferBackedChannelBuffer( processor.serialize(user) ) );
        Integer userAfterId = userController.create( request, response ).getId();
        User userAfter      = userRepository.read( userAfterId );

        assertEqualsUser( user, userAfter, false );
    }

    @Test
    public void deleteUserNoGroupDevices() throws Exception {
        Response responseDelete = new Response();
        Request requestDelete   = makeRequest(HttpMethod.DELETE, "/api/users/1.json");

        User user = makeUser( 1, "username", "password", "realname", UserType.USER);
        userRepository.insertNonIdentity( user );

        /*Delete User*/
        requestDelete.addHeader("userId", "1" );
        userController.delete( requestDelete, responseDelete );

        /*Check data against returned etc*/
        List<User> users = userRepository.selectAll();
        assertEquals( "User Size:", users.size(), 0 );
    }

    @Test
    public void readMultipleUsers() throws Exception {
        Response responseRead   = new Response();
        Request requestRead     = makeRequest(HttpMethod.GET,    "/api/users/index.json");

        List<User>  users      = new ArrayList<User>();
        List<User>  usersRead  = new ArrayList<User>();

        for(int i = 0; i < 20; i++ ){
            users.add( makeUser( "username"+i, "password"+i, "realname"+i, UserType.USER) );
        }
        userRepository.saveAll( users );

        usersRead = userController.readAll( requestRead, responseRead );
        for(int i=0; i< 20; i++){
            assertEqualsUser( users.get(i), usersRead.get(i), false );
        }
    }

    @Test
    public void createUserBadGroupsIdNoDevices() throws Exception {
        thrown.expect( RuntimeException.class );
        Response response = new Response();
        Request request   = makeRequest(HttpMethod.POST, "/api/users/index.json");

        List<Group> groups = new ArrayList<Group>();
        for(int i = 0; i < 20; i++ ){
            groups.add( makeGroup("test"+i) );
        }
        User user = makeUser( "username", "password", "realname", UserType.USER);
        user.setGroups( groups );

        /*Create User*/
        request.setBody( new ByteBufferBackedChannelBuffer( processor.serialize(user) ) );
        userController.create( request, response );
    }

    @Test
    public void createUserGroupsNoIdAndDevices() throws Exception {
        thrown.expect( RuntimeException.class );
        Response response = new Response();
        Request request   = makeRequest(HttpMethod.PUT,    "/api/users/index.json");

        List<Group> groups   = new ArrayList<Group>();
        List<Device> devices = new ArrayList<Device>();
        for(int i = 0; i < 20; i++ ){
            groups.add( makeGroup("test" + i) );
        }
        groupRepository.insertAll(groups);
        for(int i = 0; i < 20; i++ ){ groups.get(i).setId(null); } /*Sormula gives them an ID*/

        for(int i = 0; i < 5; i ++){
            devices.add( makeDevice("test"+i, "11221122112" + (i+1), DeviceType.DESKTOP) );
        }

        User user = makeUser( "username", "password", "realname", UserType.USER);
        user.setGroups( groups );
        user.setDevices( devices );

        /*Create User*/
        request.setBody( new ByteBufferBackedChannelBuffer( processor.serialize(user) ) );
        userController.create( request, response ).getId();
    }

    @Test
    public void createUserGroupsAndDevices() throws Exception {
        Response response = new Response();
        Request request   = makeRequest(HttpMethod.PUT,    "/api/users/index.json");

        List<Group> groups   = new ArrayList<Group>();
        List<Device> devices = new ArrayList<Device>();
        for(int i = 0; i < 20; i++ ){
            groups.add( makeGroup( i+1, "test" + i) );
        }
        groupRepository.insertNonIdentityAll(groups);

        for(int i = 0; i < 5; i ++){
            devices.add( makeDevice("test"+i, "11221122112" + (i+1), DeviceType.DESKTOP) );
        }

        User user = makeUser( "username", "password", "realname", UserType.USER);
        user.setGroups( groups );
        user.setDevices( devices );

        /*Create User*/
        request.setBody( new ByteBufferBackedChannelBuffer( processor.serialize(user) ) );
        Integer userAfterId = userController.create( request, response ).getId();
        User userAfter      = userRepository.read( userAfterId );

        assertEquals( "Group Size",  userAfter.getGroups().size(), user.getGroups().size() );
        assertEquals( "Device Size", userAfter.getDevices().size(), user.getDevices().size() );

        /*Check data against returned etc*/
        assertEqualsUser( userAfter, user, false );

        /*Check groups*/
        for(int i = 0; i < 20; i++ ){
            assertEqualsGroup( user.getGroups().get(i), userAfter.getGroups().get(i), true );
        }

        /*Check MAC Address*/
        for(int i = 0; i < 5; i ++) {
            assertEqualsDevice( userAfter.getDevices().get(i), user.getDevices().get(i), false );
        }
    }

    @Test
    public void updateUserGroupsNoIdNoDevices() throws Exception {
        thrown.expect( RuntimeException.class );
        Response responseUpdate = new Response();
        Request requestUpdate   = makeRequest(HttpMethod.PUT,    "/api/users/index.json");

        List<Group> groups = new ArrayList<Group>();
        for(int i = 0; i < 20; i++ ){
            groups.add( makeGroup("test" + i) );
        }
        User user = makeUser( "username", "password", "realname", UserType.USER);
        user.setId(1);
        userRepository.insertNonIdentity( user );
        user.setGroups( groups );

        /*Create User*/
        requestUpdate.addHeader("userId", Integer.toString( user.getId() ) );
        requestUpdate.setBody( new ByteBufferBackedChannelBuffer( processor.serialize(user) ) );
        userController.update( requestUpdate, responseUpdate );
    }

    @Test
    public void updateUserGroupsAndDevicesThenDelete() throws Exception {
        Response responseUpdate = new Response();
        Request requestUpdate   = makeRequest(HttpMethod.PUT,    "/api/users/index.json");
        Response responseDelete = new Response();
        Request requestDelete   = makeRequest(HttpMethod.DELETE, "/api/users/1.json");

        List<Group> groups   = new ArrayList<Group>();
        List<Device> devices = new ArrayList<Device>();
        for(int i = 0; i < 20; i++ ){
            groups.add( makeGroup("test"+i) );
        }
        groupRepository.insertAll(groups);

        for(int i = 0; i < 5; i ++){
            devices.add( makeDevice(i+1, "test"+i, "11221122112"+(i+1), DeviceType.DESKTOP ) );
        }
        deviceReposiory.insertNonIdentityAll(devices);

        User userAfter;
        User user = makeUser( 1, "username", "password", "realname", UserType.USER );
        userRepository.insertNonIdentity( user );
        user.setGroups( groups );
        user.setDevices( devices );

        /*Create User*/
        requestUpdate.addHeader("userId", Integer.toString( user.getId() ) );
        requestUpdate.setBody( new ByteBufferBackedChannelBuffer( processor.serialize(user) ) );
        userController.update( requestUpdate, responseUpdate );
        userAfter = userRepository.read( 1 );

        assertEquals( "Group Size", userAfter.getGroups().size(), user.getGroups().size() );
        assertEquals( "Device Size", userAfter.getDevices().size(), user.getDevices().size() );

        /*Check data against returned etc*/
        assertEquals( user.getUsername(), userAfter.getUsername() );
        assertEquals( user.getRealname(), userAfter.getRealname() );
        assertEquals( user.getPassword(), userAfter.getPassword() );

        /*Check groups*/
        for(int i = 0; i < 20; i++ ){
            assertEqualsGroup( userAfter.getGroups().get(i), user.getGroups().get(i), false );
        }

        /*Check MAC Address*/
        for(int i = 0; i < 5; i ++) {
            assertEqualsDevice( userAfter.getDevices().get(i), user.getDevices().get(i), false );
        }

        /*Now Delete the user*/
        requestDelete.addHeader("userId", Integer.toString( user.getId() ) );
        userController.delete( requestDelete, responseDelete );
        List<UserGroup> userGroups = userGroupTable.selectAllCustom("WHERE userId = ?", new Object[]{ user.getId() });

        assertEquals( "UserGroup Size", userGroups.size(), 0 );
    }
}
