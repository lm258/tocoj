angular.module('Authentication', []);

var app = angular.module(
    'myApp', ['ui.router', 'ngCookies', 'UiControllers', 'Authentication', 'AppsControllers', 'UserControllers', 'DeviceControllers', 'GroupControllers', 'MoviesControllers', 'PicturesControllers', 'MusicsControllers']
);

app.config(function($stateProvider, $urlRouterProvider) {
    //
    // For any unmatched url, redirect to /state1
    $urlRouterProvider.otherwise("/");
    //
    // Now set up the states
    $stateProvider.
    state('login', {
        url: "/login",
        templateUrl: "modules/authentication/views/login.html",
        controller: 'LoginController',
        hideMenus: true
    }).
    state('apps', {
        url: "/",
        templateUrl: "modules/app/views/apps.html",
        controller: 'AppsController'
    }).
    state('users', {
        url: "/users",
        templateUrl: "modules/user/views/users.html",
        controller: 'UsersController'
    }).
    state('user', {
        url: "/user/{id}",
        templateUrl: "modules/user/views/user.html",
        controller: 'UserController'
    }).
    state('devices', {
        url: "/devices",
        templateUrl: "modules/device/views/devices.html",
        controller: 'DevicesController'
    }).
    state('device', {
        url: "/device/{id}",
        templateUrl: "modules/device/views/device.html",
        controller: 'DeviceController'
    }).
    state('groups', {
        url: "/groups",
        templateUrl: "modules/group/views/groups.html",
        controller: 'GroupsController'
    }).
    state('movies', {
        url: "/movies",
        templateUrl: "modules/movie/views/movies.html",
        controller: 'MoviesController'
    }).
    state('pictures', {
        url: "/pictures",
        templateUrl: "modules/picture/views/pictures.html",
        controller: 'PicturesController'
    }).
    state('music', {
        url: "/music",
        templateUrl: "modules/music/views/music.html",
        controller: 'MusicsController'
    });
});

app.run(
    [
        '$rootScope',
        '$state',
        '$cookieStore',
        '$http',
        function($rootScope, $state, $cookieStore, $http) {
            // keep user logged in after page refresh
            $rootScope.globals = $cookieStore.get('globals') || {};
            if ($rootScope.globals.currentUser) {
                $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata;
            }

            var globals = $rootScope.globals;

            //console.log($rootScope);

            $rootScope.$on('$stateChangeStart', function(event, toState, fromState) {
                if (toState.name !== 'login' && !globals.currentUser) {
                    event.preventDefault();
                    $state.transitionTo('login');
                }
            });
        }
    ]
);
