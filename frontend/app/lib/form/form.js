'use strict';

/*
 * This function is used to check for mobile devices
 * Code is from website http://detectmobilebrowsers.com/
 * The script was unlicensed
*/
function isMobile(){
    var mobile = false;
    (function(a,b){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))mobile=true;})(navigator.userAgent||navigator.vendor||window.opera,'http://detectmobilebrowser.com/mobile');
    return mobile;
}

/*Create the ui services module*/
var FormServices = angular.module( 'FormServices', ['ngResource', 'ui.bootstrap'] );

/*Declare our form types that Form will use*/
var formTypes = {
    "text" : function( model, binder ) {
        return "<input type='text' ng-model='" + model + "." + binder + "' class='form-control'/>";
    },
    "password" : function( model, binder ) {
        return "<input type='password' ng-model='" + model + "." + binder + "' class='form-control'/>";
    },
    "datepicker" : function( model, binder ) {
        if( isMobile() ){
            /*Inline date picker for mobile*/
            return "<p class='input-group' ng-controller='UiDatePicker' >" +
                "<datepicker ng-model='" + model + "." + binder + "' min-date='minDate' max-mode='day' show-weeks='true' class='well well-sm'></datepicker>"+
            "</p>";
        } else {
            /*Popup date picker for desktop*/
            return "<p class='input-group' ng-controller='UiDatePicker' >" +
                "<input type='text' class='form-control' datepicker-popup='dd-MMMM-yyyy' ng-model='" + model + "." + binder + "' is-open='opened' min-date='minDate' max-mode='day' close-text='Close' />"+
                    "<span class='input-group-btn'>" +
                        "<button type='button' class='btn btn-default' ng-click='open($event)'><i class='glyphicon glyphicon-calendar'></i></button>"+
                    "</span>"+
                "</p>";
        }
    },
    "timepicker" : function( model, binder ) {
        return "<timepicker ng-model='" + model + "." + binder + "' ng-change='changed()' hour-step='1' minute-step='15' ></timepicker>";
    },
    "select" : function( model, binder, opts ) {
        return "<select ng-model=\"" + model + "." + binder + "\" ng-init='options="+ JSON.stringify(opts.data) +"' ng-options='option.value as option.name for option in options' class='form-control'>"+
            "</select>";
    },
    "rate" : function( model, binder ) {
        return "<rating ng-model='" + model + "." + binder + "' max='10' ></rating>";
    },
    "directive" : function( model, binder, opts ) {
        return "<div "+ opts.directive +"></div>";
    },
    /**
     * Takes values in the format:
     * "options" : {
     *     "columns" : [ { "label" : "Name", "bind" : "name" } ],
     *     "aData"   : [ { "id" : 1, "name", "Name"}, { "id" : 2, "name" : "Name2" } ]
     * }
    */
    "table" : function( model, binder, opts ) {
        var table = "<table class='table'>";
        table += "<thead>";
        for(var i=0; i<opts.columns.length; i++){
            table += "<th>" + opts.columns[i].label + "</th>";
        }
        table += "</thead>";
        table += "<tbody>"
        for(var j=0; j< opts.aData.length; j++){
            table += "<tr>";
            for(var i=0; i<opts.columns.length; i++){
                table += "<td>{{ data." + opts.columns[i].bind + "}}</td>";
            }
            table += "</tr>";
        }
        table +="</tbody>";
        table += "</table>";
        return table;
    }
};

/*This function is used to generate our dynamic form*/
function generateForm( model, formJson ) {
    var form = "";
    for( var i = 0; i < formJson.length; i++ ){
        form += "<div class='row' >"+
            "<div class='col-md-4'>"+
                "<h5>" + formJson[i].label + "<br/><small>" + formJson[i]['help-text'] + "</small></h5>"+
            "</div>"+
            "<div class='col-md-8'>"+
                formTypes[ formJson[i].type ]( model, formJson[i].bind, formJson[i].options ) +
                "<span class='label label-danger'>{{ errors." + formJson[i].bind + "}}</span>" +
            "</div></div><br/>";
    }
    return form;
}

/*This model is used for creating dialogs in the system*/
FormServices.factory(
    'Form',
    [
        '$modal',
        '$compile',
        function( $modal, $compile ){
            var object = {};

            /*Create a confirm dialog*/
            object.open =  function( options ) {

                var formTemplate = $compile(
                    "<div class='modal-header'><h4>" + options.title + "</h4></div>"+
                    "<div class='modal-body'><form>" + 
                        generateForm( options.model, options.fields ) +
                    "</form></div>"+
                    "<div class='modal-footer'>" +
                        "<button class='btn btn-primary' ng-show='"+options.model+ ".id' ng-click='save("+ options.model +")' >Save</button>" +
                        "<button class='btn btn-primary' ng-show='!"+options.model+ ".id' ng-click='create("+ options.model +")' >Create</button>" +
                        "<button class='btn btn-default' ng-click='close()' >Cancel</button>" +
                    "</div>"
                )(options.scope);

                var obj = $modal.open({
                    template: formTemplate,
                    controller: options.controller,
                    resolve: options.resolve
                });

                obj.result.then(
                    options.submit
                );
            }

            /*Return the object*/
            return object;
        }
    ]
);