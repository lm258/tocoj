'use strict';

/*Create the apps controllers module*/
var AppsControllers = angular.module( 'AppsControllers', [ 'AppsServices' ] );

/*AppsController loads the apps*/
AppsControllers.controller(
    'AppsController',
    [
        '$scope',
        'App',
        function ($scope, App) {
            $scope.apps = App.query();
        }
    ]
);