'use strict';

/*Create the app services module*/
var AppsServices = angular.module( 'AppsServices', ['ngResource']);

/*App model is used to get the apps*/
AppsServices.factory(
    'App',
    [
        '$resource',
        function($resource){
            return $resource(
                '/api/apps/:id.json',
                {},
                {
                    query: { method:'GET', params:{ id:'index' }, isArray:true }
                }
            );
        }
    ]
);