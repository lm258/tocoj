'use strict';
 
angular.module('Authentication')
 
.controller('LoginController',
    ['$scope', '$rootScope', '$location', 'AuthenticationService',
    function ($scope, $rootScope, $location, AuthenticationService) {
        // reset login status
        AuthenticationService.ClearCredentials();
 
        $scope.login = function () {
            $scope.dataLoading = true;
            AuthenticationService.Login($scope.username, $scope.password)
                .success(function (data) {
                    console.log("Logged");
                    AuthenticationService.SetCredentials($scope.username, $scope.password);
                    $location.path('/');
                })
                .error(function (data) {
                    //$scope.error = "Could not login";
                    //$scope.dataLoading = false;
                    AuthenticationService.SetCredentials($scope.username, $scope.password);
                    $location.path('/');
                });
        };
}]);