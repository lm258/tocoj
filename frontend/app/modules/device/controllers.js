'use strict';

/*Create our devicecontrollers module*/
var DeviceControllers = angular.module( 'DeviceControllers', [ 'FormServices', 'DeviceDirectives', 'DeviceServices', 'UiServices' ]);

/*This controller handles multiple devices*/
DeviceControllers.controller(
    'DevicesController',
    [
        '$scope',
        '$modal',
        'Device',
        'Dialog',
        'Form',
        function ($scope, $modal, Device, Dialog, Form ) {

            /*Get our devices of the device service*/
            $scope.devices = Device.query();

            /**
             * This function is used to delete a device from the system
             * @params id the id of the device to delete
            */
            $scope.deleteDevice = function( id ) {

                /*Ask the device if they really want to delete the device*/
                Dialog.confirm(
                    {
                        title : 'Delete device?',
                        message : 'Are you sure you wish to delete this device?',
                        close : function( result ) {
                            if( result ) {
                                Device.delete( id ).
                                success( function(){
                                    /*Reload our devices from the system*/
                                    $scope.devices = Device.query();
                                }).
                                error( function() {
                                    /*Show dialog saying an error occured*/
                                    Dialog.error( { message : 'Could not delete device' } );
                                });
                            }
                        }
                    }
                );
            }

            /*This function is used to edit a device in the system*/
            $scope.editDevice = function( deviceId ) {

                /*Open the form responsible for editing a device*/
                Form.open({
                    "title" : "Edit Device",
                    "controller" : 'DeviceController',
                    "resolve" : { deviceId : function(){ return deviceId;} },
                    "model" : "device",
                    "scope" : $scope,
                    "fields" : [
                        {
                            "type" : "text",
                            "bind" : "name",
                            "label" : "Name",
                            "help-text" : "The name of the new device"
                        },
                        {
                            "type" : "text",
                            "bind" : "mac",
                            "label" : "MAC",
                            "help-text" : "The MAC of the new device"
                        },
                        {
                            "type" : "select",
                            "bind" : "type",
                            "label" : "Type",
                            "help-text" : "The type of the user",
                            "options" : {
                                "data" : [
                                    { "name" : "Desktop", "value" : "DESKTOP" },
                                    { "name" : "Tablet", "value" : "TABLET" },
                                    { "name" : "Mobile", "value" : "MOBILE" }
                                ]
                            }
                        },
                        {
                            "type" : "directive",
                            "bind" : "userId",
                            "label" : "User",
                            "help-text" : "The user associated with this device",
                            "options" : { "directive" : "device-user" }
                        }
                    ],
                    "submit" : function(){
                        $scope.devices = Device.query();
                    }
                });
            }

            /*This function is used to add a device in the system*/
            $scope.addDevice = function() {

                /*Open the modal responsible for editing a device*/
                Form.open({
                    "title" : "Add Device",
                    "controller" : 'DeviceController',
                    "resolve" : { deviceId : function(){ return undefined;} },
                    "model" : "device",
                    "scope" : $scope,
                    "fields" : [
                        {
                            "type" : "text",
                            "bind" : "name",
                            "label" : "Name",
                            "help-text" : "The name of the new device"
                        },
                        {
                            "type" : "text",
                            "bind" : "mac",
                            "label" : "MAC",
                            "help-text" : "The MAC of the new device"
                        },
                        {
                            "type" : "select",
                            "bind" : "type",
                            "label" : "Type",
                            "help-text" : "The type of the user",
                            "options" : {
                                "data" : [
                                    { "name" : "Desktop", "value" : "DESKTOP" },
                                    { "name" : "Tablet", "value" : "TABLET" },
                                    { "name" : "Mobile", "value" : "MOBILE" }
                                ]
                            }
                        },
                        {
                            "type" : "directive",
                            "bind" : "userId",
                            "label" : "User",
                            "help-text" : "The user associated with this device",
                            "options" : { "directive" : "device-user" }
                        }
                    ],
                    "submit" : function(){
                        $scope.devices = Device.query();
                    }
                });
            }
        }
    ]
);

/**
 * This controller handles a single device.
 * View is embedded inside a modal so this
 * controller handles its closing etc aswell.
*/
DeviceControllers.controller(
    'DeviceController',
    [
        '$scope',
        '$modalInstance',
        'Device',
        'deviceId',
        function($scope, $modalInstance, Device, deviceId) {

            /*If a deviceid is present then get our*/
            if( deviceId ) {
                $scope.device = Device.get( { id: deviceId } );
            } else {
                $scope.device = {};
            }

            /*This function is used to save a device*/
            $scope.save = function( device ) {
                Device.save( device ).error(
                    function( json ) {
                        $scope.errors = json.errors;
                    }
                ).success(
                    function() { $modalInstance.close( true ); }
                );
            }

            /*This function is used to create a new device*/
            $scope.create = function( device ) {
                Device.create( device ).error(
                    function( json ) {
                        $scope.errors = json.errors;
                    }
                ).success(
                    function() { $modalInstance.close( true ); }
                );
            }

            /*This function is used to close the modal*/
            $scope.close = function() {
                $modalInstance.close( false );
            };
        }
    ]
);