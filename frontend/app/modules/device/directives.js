var DeviceDirectives = angular.module( 'DeviceDirectives', [ 'UserServices' ]);

DeviceDirectives.directive( 'deviceUser', [ 'User', function( User ) {
  return {
    templateUrl: 'modules/device/views/deviceUser.html',
    link: function ($scope, element, attrs) {
        $scope.allUsers = User.query();
    }
  };
}]);