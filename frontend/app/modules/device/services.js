'use strict';

var DeviceServices = angular.module( 'DeviceServices', ['ngResource']);

DeviceServices.factory(
    'Device',
    [
        '$resource',
        '$http',
        function($resource, $http){
            var obj =  $resource(
                '/api/devices/:id.json',
                {},
                {
                    query: { method:'GET', params:{ id: 'index' }, isArray:true }
                }
            );

            /*This function is used to delete a device*/
            obj.delete = function(id) {
                return $http.delete( '/api/devices/'+ id +'.json' );
            }

            /*This function is used to save an existing device*/
            obj.save = function( device ) {
                return $http.put( '/api/devices/'+ device.id +'.json', device );
            }

            /*This function is used to create a new device*/
            obj.create = function( device ) {
                return $http.post( '/api/devices/index.json', device );
            }

            return obj;
        }
    ]
);