'use strict';

/*Create our groupcontrollers module*/
var GroupControllers = angular.module( 'GroupControllers', [ 'FormServices', 'GroupServices', 'UiServices' ]);

/*This controller handles multiple groups*/
GroupControllers.controller(
    'GroupsController',
    [
        '$scope',
        '$modal',
        'Group',
        'Dialog',
        'Form',
        function ($scope, $modal, Group, Dialog, Form ) {

            /*Get our groups of the group service*/
            $scope.groups = Group.query();

            /**
             * This function is used to delete a group from the system
             * @params id the id of the group to delete
            */
            $scope.deleteGroup = function( id ) {

                /*Ask the group if they really want to delete the group*/
                Dialog.confirm(
                    {
                        title : 'Delete group?',
                        message : 'Are you sure you wish to delete this group?',
                        close : function( result ) {
                            if( result ) {
                                Group.delete( id ).
                                success( function(){
                                    /*Reload our groups from the system*/
                                    $scope.groups = Group.query();
                                }).
                                error( function() {
                                    /*Show dialog saying an error occured*/
                                    Dialog.error( { message : 'Could not delete group' } );
                                });
                            }
                        }
                    }
                );
            }

            /*This function is used to edit a group in the system*/
            $scope.editGroup = function( groupId ) {

                /*Open the form responsible for editing a group*/
                Form.open({
                    "title" : "Edit Group",
                    "controller" : 'GroupController',
                    "resolve" : { groupId : function(){ return groupId;} },
                    "model" : "group",
                    "scope" : $scope,
                    "fields" : [
                        {
                            "type" : "text",
                            "bind" : "name",
                            "label" : "Name",
                            "help-text" : "The name of the new group"
                        }
                    ],
                    "submit" : function(){
                        $scope.groups = Group.query();
                    }
                });
            }

            /*This function is used to add a group in the system*/
            $scope.addGroup = function() {

                /*Open the modal responsible for editing a group*/
                Form.open({
                    "title" : "Add Group",
                    "controller" : 'GroupController',
                    "resolve" : { groupId : function(){ return undefined;} },
                    "model" : "group",
                    "scope" : $scope,
                    "fields" : [
                        {
                            "type" : "text",
                            "bind" : "name",
                            "label" : "Name",
                            "help-text" : "The name of the new group"
                        }
                    ],
                    "submit" : function(){
                        $scope.groups = Group.query();
                    }
                });
            }
        }
    ]
);

/**
 * This controller handles a single group.
 * View is embedded inside a modal so this
 * controller handles its closing etc aswell.
*/
GroupControllers.controller(
    'GroupController',
    [
        '$scope',
        '$modalInstance',
        'Group',
        'groupId',
        function($scope, $modalInstance, Group, groupId) {

            /*If a groupid is present then get our*/
            if( groupId ) {
                $scope.group = Group.get( { id: groupId } );
            } else {
                $scope.group = {};
            }

            /*This function is used to save a group*/
            $scope.save = function( group ) {
                Group.save( group ).error(
                    function( json ) {
                        $scope.errors = json.errors;
                    }
                ).success(
                    function() { $modalInstance.close( true ); }
                );
            }

            /*This function is used to create a new group*/
            $scope.create = function( group ) {
                Group.create( group ).error(
                    function( json ) {
                        $scope.errors = json.errors;
                    }
                ).success(
                    function() { $modalInstance.close( true ); }
                );
            }

            /*This function is used to close the modal*/
            $scope.close = function() {
                $modalInstance.close( false );
            };
        }
    ]
);