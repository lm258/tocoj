'use strict';

var GroupServices = angular.module( 'GroupServices', ['ngResource']);

GroupServices.factory(
    'Group',
    [
        '$resource',
        '$http',
        function($resource, $http){
            var obj =  $resource(
                '/api/groups/:id.json',
                {},
                {
                    query: { method:'GET', params:{ id: 'index' }, isArray:true }
                }
            );

            /*This function is used to delete a user*/
            obj.delete = function(id) {
                return $http.delete( '/api/groups/'+ id +'.json' );
            }

            /*This function is used to save an existing user*/
            obj.save = function( user ) {
                return $http.put( '/api/groups/'+ user.id +'.json', user );
            }

            /*This function is used to create a new user*/
            obj.create = function( user ) {
                return $http.post( '/api/groups/index.json', user );
            }

            return obj;
        }
    ]
);