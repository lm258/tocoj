'use strict';

/*Create our moviecontrollers module*/
var MovieControllers = angular.module( 'MoviesControllers', [ 'FormServices', 'MovieServices', 'MovieDirectives', 'UiServices' ]);

/*This controller handles multiple movies*/
MovieControllers.controller(
    'MoviesController',
    [
        '$scope',
        '$modal',
        'Movie',
        'Dialog',
        'Form',
        function ($scope, $modal, Movie, Dialog, Form ) {

            /*Get our movies of the movie service*/
            $scope.movies = Movie.query();

            /**
             * This function is used to delete a movie from the system
             * @params id the id of the movie to delete
            */
            $scope.deleteMovie = function( id ) {

                /*Ask the movie if they really want to delete the movie*/
                Dialog.confirm(
                    {
                        title : 'Delete movie?',
                        message : 'Are you sure you wish to delete this movie?',
                        close : function( result ) {
                            if( result ) {
                                Movie.delete( id ).
                                success( function(){
                                    /*Reload our movies from the system*/
                                    $scope.movies = Movie.query();
                                }).
                                error( function() {
                                    /*Show dialog saying an error occured*/
                                    Dialog.error( { message : 'Could not delete movie' } );
                                });
                            }
                        }
                    }
                );
            }

            /*This function is used to edit a movie in the system*/
            $scope.editMovie = function( movieId ) {

                /*Open the form responsible for editing a movie*/
                Form.open({
                    "title" : "Edit Movie",
                    "controller" : 'MovieController',
                    "resolve" : { movieId : function(){ return movieId;} },
                    "model" : "movie",
                    "scope" : $scope,
                    "fields" : [
                        {
                            "type" : "text",
                            "bind" : "title",
                            "label" : "Title",
                            "help-text" : "The title of the movie"
                        },
                        {
                            "type" : "text",
                            "bind" : "plot",
                            "label" : "Plot",
                            "help-text" : "The plot of the movie"
                        },
                        {
                            "type" : "text",
                            "bind" : "genre",
                            "label" : "Genre",
                            "help-text" : "The genre of the movie"
                        },
                        {
                            "type" : "text",
                            "bind" : "director",
                            "label" : "Director",
                            "help-text" : "The director of the movie"
                        },
                        {
                            "type" : "directive",
                            "bind" : "groups",
                            "label" : "Groups",
                            "help-text" : "The user associated with this movie",
                            "options" : { "directive" : "movie-group" }
                        }
                    ],
                    "submit" : function(){
                        $scope.movies = Movie.query();
                    }
                });
            }
        }
    ]
);

/**
 * This controller handles a single movie.
 * View is embedded inside a modal so this
 * controller handles its closing etc aswell.
*/
MovieControllers.controller(
    'MovieController',
    [
        '$scope',
        '$modalInstance',
        'Movie',
        'movieId',
        function($scope, $modalInstance, Movie, movieId) {

            /*If a movieid is present then get our*/
            if( movieId ) {
                $scope.movie = Movie.get( { id: movieId } );
            } else {
                $scope.movie = {};
            }

            /*This function is used to save a movie*/
            $scope.save = function( movie ) {
                Movie.save( movie ).error(
                    function( json ) {
                        $scope.errors = json.errors;
                    }
                ).success(
                    function() { $modalInstance.close( true ); }
                );
            }

            /*This function is used to create a new movie*/
            $scope.create = function( movie ) {
                Movie.create( movie ).error(
                    function( json ) {
                        $scope.errors = json.errors;
                    }
                ).success(
                    function() { $modalInstance.close( true ); }
                );
            }

            /*This function is used to close the modal*/
            $scope.close = function() {
                $modalInstance.close( false );
            };
        }
    ]
);