var MovieDirectives = angular.module( 'MovieDirectives', [  ]);

MovieDirectives.directive( 'movieGroup', [ 'Group', function( Group ) {
  return {
    templateUrl: 'modules/movie/views/movieGroup.html',
    link: function ($scope, element, attrs) {
        $scope.allGroups = Group.query();

        /*This function is used to remove a group from the movie*/
        $scope.removeGroup = function ( groupObj ) {
            $scope.movie.groups = $scope.movie.groups.filter(
                function( group ){
                    return !(group.id==groupObj.id);
                }
            );
        }

        /*This function is used to add a group to the movie*/
        $scope.addGroup = function ( group ) {
            if( $scope.movie.groups != undefined ) {
                $scope.movie.groups.push( group );
            } else {
                $scope.movie.groups = [];
                $scope.movie.groups.push( group );
            }
        }
    }
  };
}]);