'use strict';

var MovieServices = angular.module( 'MovieServices', ['ngResource']);

MovieServices.factory(
    'Movie',
    [
        '$resource',
        '$http',
        function($resource, $http){
            var obj =  $resource(
                '/api/movies/:id.json',
                {},
                {
                    query: { method:'GET', params:{ id: 'index' }, isArray:true }
                }
            );

            /*This function is used to delete a user*/
            obj.delete = function(id) {
                return $http.delete( '/api/movies/'+ id +'.json' );
            }

            /*This function is used to save an existing user*/
            obj.save = function( user ) {
                return $http.put( '/api/movies/'+ user.id +'.json', user );
            }

            /*This function is used to create a new user*/
            obj.create = function( user ) {
                return $http.post( '/api/movies/index.json', user );
            }

            return obj;
        }
    ]
);