'use strict';

/*Create our musiccontrollers module*/
var MusicControllers = angular.module( 'MusicsControllers', [ 'FormServices', 'MusicServices', 'UiServices', 'MusicDirectives' ]);

/*This controller handles multiple musics*/
MusicControllers.controller(
    'MusicsController',
    [
        '$scope',
        '$modal',
        'Music',
        'Dialog',
        'Form',
        function ($scope, $modal, Music, Dialog, Form ) {

            /*Get our musics of the music service*/
            $scope.musics = Music.query();

            /*This function is used to edit a music in the system*/
            $scope.editMusic = function( musicId ) {

                /*Open the form responsible for editing a music*/
                Form.open({
                    "title" : "Edit Song",
                    "controller" : 'MusicController',
                    "resolve" : { musicId : function(){ return musicId;} },
                    "model" : "music",
                    "scope" : $scope,
                    "fields" : [
                        {
                            "type" : "text",
                            "bind" : "title",
                            "label" : "Title",
                            "help-text" : "The title of the song"
                        },
                        {
                            "type" : "text",
                            "bind" : "artist",
                            "label" : "Artist",
                            "help-text" : "The artist of the song"
                        },
                        {
                            "type" : "text",
                            "bind" : "genre",
                            "label" : "Genre",
                            "help-text" : "Genres of the song, can be a comma seperated list"
                        },
                        {
                            "type" : "text",
                            "bind" : "year",
                            "label" : "Year",
                            "help-text" : "Year of the song"
                        },
                        {
                            "type" : "directive",
                            "bind" : "groups",
                            "label" : "Groups",
                            "options" : { "directive" : "music-group" },
                            "help-text" : "Groups associated with this song"
                        }
                    ],
                    "submit" : function(){
                        $scope.musics = Music.query();
                    }
                });
            }
        }
    ]
);

/**
 * This controller handles a single music.
 * View is embedded inside a modal so this
 * controller handles its closing etc aswell.
*/
MusicControllers.controller(
    'MusicController',
    [
        '$scope',
        '$modalInstance',
        'Music',
        'musicId',
        function($scope, $modalInstance, Music, musicId) {

            /*If a musicid is present then get our*/
            if( musicId ) {
                $scope.music = Music.get( { id: musicId } );
            } else {
                $scope.music = {};
            }

            /*This function is used to save a music*/
            $scope.save = function( music ) {
                Music.save( music ).error(
                    function( json ) {
                        $scope.errors = json.errors;
                    }
                ).success(
                    function() { $modalInstance.close( true ); }
                );
            }

            /*This function is used to create a new music*/
            $scope.create = function( music ) {
                Music.create( music ).error(
                    function( json ) {
                        $scope.errors = json.errors;
                    }
                ).success(
                    function() { $modalInstance.close( true ); }
                );
            }

            /*This function is used to close the modal*/
            $scope.close = function() {
                $modalInstance.close( false );
            };
        }
    ]
);