var MusicDirectives = angular.module( 'MusicDirectives', [  ]);

MusicDirectives.directive( 'musicGroup', [ 'Group', function( Group ) {
  return {
    templateUrl: 'modules/music/views/musicGroup.html',
    link: function ($scope, element, attrs) {
        $scope.allGroups = Group.query();

        /*This function is used to remove a group from the music*/
        $scope.removeGroup = function ( groupObj ) {
            $scope.music.groups = $scope.music.groups.filter(
                function( group ){
                    return !(group.id==groupObj.id);
                }
            );
        }

        /*This function is used to add a group to the music*/
        $scope.addGroup = function ( group ) {
            if( $scope.music.groups != undefined ) {
                $scope.music.groups.push( group );
            } else {
                $scope.music.groups = [];
                $scope.music.groups.push( group );
            }
        }
    }
  };
}]);