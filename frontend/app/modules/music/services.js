'use strict';

var MusicServices = angular.module( 'MusicServices', ['ngResource']);

MusicServices.factory(
    'Music',
    [
        '$resource',
        '$http',
        function($resource, $http){
            var obj =  $resource(
                '/api/music/:id.json',
                {},
                {
                    query: { method:'GET', params:{ id: 'index' }, isArray:true }
                }
            );

            /*This function is used to delete a music*/
            obj.delete = function(id) {
                return $http.delete( '/api/music/'+ id +'.json' );
            }

            /*This function is used to save an existing music*/
            obj.save = function( music ) {
                return $http.put( '/api/music/'+ music.id +'.json', music );
            }

            /*This function is used to create a new music*/
            obj.create = function( music ) {
                return $http.post( '/api/music/index.json', music );
            }

            return obj;
        }
    ]
);