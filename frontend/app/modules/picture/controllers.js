'use strict';

/*Create our picturecontrollers module*/
var PictureControllers = angular.module( 'PicturesControllers', [ 'FormServices', 'PictureServices', 'PictureDirectives', 'UiServices' ]);

/*This controller handles multiple pictures*/
PictureControllers.controller(
    'PicturesController',
    [
        '$scope',
        '$modal',
        'Picture',
        'Dialog',
        'Form',
        function ($scope, $modal, Picture, Dialog, Form ) {

            /*Get our pictures of the picture service*/
            $scope.pictures = Picture.query();

            /**
             * This function is used to delete a picture from the system
             * @params id the id of the picture to delete
            */
            $scope.deletePicture = function( id ) {

                /*Ask the picture if they really want to delete the picture*/
                Dialog.confirm(
                    {
                        title : 'Delete picture?',
                        message : 'Are you sure you wish to delete this picture?',
                        close : function( result ) {
                            if( result ) {
                                Picture.delete( id ).
                                success( function(){
                                    /*Reload our pictures from the system*/
                                    $scope.pictures = Picture.query();
                                }).
                                error( function() {
                                    /*Show dialog saying an error occured*/
                                    Dialog.error( { message : 'Could not delete picture' } );
                                });
                            }
                        }
                    }
                );
            }

            /*This function is used to edit a picture in the system*/
            $scope.editPicture = function( pictureId ) {

                /*Open the form responsible for editing a picture*/
                Form.open({
                    "title" : "Edit Picture",
                    "controller" : 'PictureController',
                    "resolve" : { pictureId : function(){ return pictureId;} },
                    "model" : "picture",
                    "scope" : $scope,
                    "fields" : [
                        {
                            "type" : "text",
                            "bind" : "name",
                            "label" : "Name",
                            "help-text" : "The name of the this picture"
                        },
                        {
                            "type" : "text",
                            "bind" : "description",
                            "label" : "Description",
                            "help-text" : "The description of the this picture"
                        },
                        {
                            "type" : "datepicker",
                            "bind" : "date",
                            "label" : "Date",
                            "help-text" : "The date of the this picture"
                        },
                        {
                            "type" : "directive",
                            "bind" : "groups",
                            "label" : "Groups",
                            "options" : { "directive" : "picture-group" },
                            "help-text" : "Groups associated with this user"
                        }
                    ],
                    "submit" : function(){
                        $scope.pictures = Picture.query();
                    }
                });
            }
        }
    ]
);

/**
 * This controller handles a single picture.
 * View is embedded inside a modal so this
 * controller handles its closing etc aswell.
*/
PictureControllers.controller(
    'PictureController',
    [
        '$scope',
        '$modalInstance',
        'Picture',
        'pictureId',
        function($scope, $modalInstance, Picture, pictureId) {

            /*If a pictureid is present then get our*/
            if( pictureId ) {
                $scope.picture = Picture.get( { id: pictureId } );
            } else {
                $scope.picture = {};
            }

            /*This function is used to save a picture*/
            $scope.save = function( picture ) {
                Picture.save( picture ).error(
                    function( json ) {
                        $scope.errors = json.errors;
                    }
                ).success(
                    function() { $modalInstance.close( true ); }
                );
            }

            /*This function is used to create a new picture*/
            $scope.create = function( picture ) {
                Picture.create( picture ).error(
                    function( json ) {
                        $scope.errors = json.errors;
                    }
                ).success(
                    function() { $modalInstance.close( true ); }
                );
            }

            /*This function is used to close the modal*/
            $scope.close = function() {
                $modalInstance.close( false );
            };
        }
    ]
);