var PictureDirectives = angular.module( 'PictureDirectives', [  ]);

PictureDirectives.directive( 'pictureGroup', [ 'Group', function( Group ) {
  return {
    templateUrl: 'modules/picture/views/pictureGroup.html',
    link: function ($scope, element, attrs) {
        $scope.allGroups = Group.query();

        /*This function is used to remove a group from the picture*/
        $scope.removeGroup = function ( groupObj ) {
            $scope.picture.groups = $scope.picture.groups.filter(
                function( group ){
                    return !(group.id==groupObj.id);
                }
            );
        }

        /*This function is used to add a group to the picture*/
        $scope.addGroup = function ( group ) {
            if( $scope.picture.groups != undefined ) {
                $scope.picture.groups.push( group );
            } else {
                $scope.picture.groups = [];
                $scope.picture.groups.push( group );
            }
        }
    }
  };
}]);