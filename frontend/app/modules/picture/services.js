'use strict';

var PictureServices = angular.module( 'PictureServices', ['ngResource']);

PictureServices.factory(
    'Picture',
    [
        '$resource',
        '$http',
        function($resource, $http){
            var obj =  $resource(
                '/api/pictures/:id.json',
                {},
                {
                    query: { method:'GET', params:{ id: 'index' }, isArray:true }
                }
            );

            /*This function is used to delete a picture*/
            obj.delete = function(id) {
                return $http.delete( '/api/pictures/'+ id +'.json' );
            }

            /*This function is used to save an existing picture*/
            obj.save = function( picture ) {
                return $http.put( '/api/pictures/'+ picture.id +'.json', picture );
            }

            /*This function is used to create a new picture*/
            obj.create = function( picture ) {
                return $http.post( '/api/pictures/index.json', picture );
            }

            return obj;
        }
    ]
);