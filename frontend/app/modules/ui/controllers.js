'user strict';

/*Create our ui controllers module*/
var UiControllers = angular.module( 'UiControllers', [ 'UserServices', 'ui.bootstrap' ] );

/*This is our dialog controller handler*/
UiControllers.controller(
    'UiDialogController',
    [
        '$scope',
        '$modalInstance',
        'dialog',
        function ($scope,$modalInstance, dialog ) {
            $scope.dialog = dialog;

            $scope.confirm = function() {
                $modalInstance.close( true );
            };

            $scope.close = function() {
                $modalInstance.close( false );
            };
        }
    ]
);

/*Controller used for controllung the date picking form element*/
UiControllers.controller(
    'UiDatePicker',
    [
        '$scope',
        function ($scope) {
            
            $scope.today = function() {
                $scope.dt = new Date();
            };
            $scope.today();
            
            $scope.clear = function () {
                $scope.dt = null;
            };

            $scope.minDate = new Date();

            $scope.open = function($event) {
                $event.preventDefault();
                $event.stopPropagation();

                $scope.opened = true;
            };
        }
    ]
);