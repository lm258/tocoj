'use strict';

/*Create the ui services module*/
var UiServices = angular.module( 'UiServices', ['ngResource', 'ui.bootstrap'] );

/*This model is used for creating dialogs in the system*/
UiServices.factory(
    'Dialog',
    [
        '$modal',
        function( $modal ){
            var object = {};

            /*Create our confirmation dialog function*/
            object.confirm = function( opts ) {
                /*Create a confirm dialog*/
                var obj = $modal.open({
                    templateUrl: 'modules/ui/views/confirm.html',
                    controller: 'UiDialogController',
                    resolve: {
                        dialog: function(){
                            return { 
                                'title' : opts.title,
                                'message' : opts.message
                            }; 
                        }
                    }
                }).result.then( opts.close );
            }

            /*Create our error dialog function*/
            object.error = function( opts ) {
                /*Create a confirm dialog*/
                var obj = $modal.open({
                    templateUrl: 'modules/ui/views/error.html',
                    controller: 'UiDialogController',
                    resolve: {
                        dialog: function(){
                            return { 
                                'title' : "System Error",
                                'message' : opts.message
                            }; 
                        }
                    }
                });
            }

            /*Return the object*/
            return object;
        }
    ]
);