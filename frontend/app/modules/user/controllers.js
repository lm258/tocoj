'use strict';

/*Create our usercontrollers module*/
var UserControllers = angular.module( 'UserControllers', [ 'FormServices', 'UserServices', 'UiServices', 'UserDirectives' ]);

/*This controller handles multiple users*/
UserControllers.controller(
    'UsersController',
    [
        '$scope',
        '$modal',
        'User',
        'Dialog',
        'Form',
        function ($scope, $modal, User, Dialog, Form ) {

            /*Get our users of the user service*/
            $scope.users = User.query();

            /**
             * This function is used to delete a user from the system
             * @params id the id of the user to delete
            */
            $scope.deleteUser = function( id ) {

                /*Ask the user if they really want to delete the user*/
                Dialog.confirm(
                    {
                        title : 'Delete user?',
                        message : 'Are you sure you wish to delete this user?',
                        close : function( result ) {
                            if( result ) {
                                User.delete( id ).
                                success( function(){
                                    /*Reload our users from the system*/
                                    $scope.users = User.query();
                                }).
                                error( function() {
                                    /*Show dialog saying an error occured*/
                                    Dialog.error( { message : 'Could not delete user' } );
                                });
                            }
                        }
                    }
                );
            }

            /*This function is used to edit a user in the system*/
            $scope.editUser = function( userId ) {

                /*Open the form responsible for editing a user*/
                Form.open({
                    "title" : "Edit User",
                    "controller" : 'UserController',
                    "resolve" : { userId : function(){ return userId;} },
                    "model" : "user",
                    "scope" : $scope,
                    "fields" : [
                        {
                            "type" : "text",
                            "bind" : "username",
                            "label" : "Username",
                            "help-text" : "The username of the new user"
                        },
                        {
                            "type" : "password",
                            "bind" : "password",
                            "label" : "Password",
                            "help-text" : "The password of the new user"
                        },
                        {
                            "type" : "text",
                            "bind" : "realname",
                            "label" : "Realname",
                            "help-text" : "The realname of the user"
                        },
                        {
                            "type" : "select",
                            "bind" : "type",
                            "label" : "Type",
                            "help-text" : "The type of the user",
                            "options" : {
                                "data" : [
                                    { "name" : "Admin", "value" : "ADMIN" },
                                    { "name" : "User", "value" : "USER" }
                                ]
                            }
                        },
                        {
                            "type" : "directive",
                            "bind" : "devices",
                            "label" : "Devices",
                            "options" : { "directive" : "user-device" },
                            "help-text" : "Devices associated with this user"
                        },
                        {
                            "type" : "directive",
                            "bind" : "groups",
                            "label" : "Groups",
                            "options" : { "directive" : "user-group" },
                            "help-text" : "Groups associated with this user"
                        }
                    ],
                    "submit" : function(){
                        $scope.users = User.query();
                    }
                });
            }

            /*This function is used to add a user in the system*/
            $scope.addUser = function() {

                /*Open the modal responsible for editing a user*/
                Form.open({
                    "title" : "Edit User",
                    "controller" : 'UserController',
                    "resolve" : { userId : function(){ return undefined;} },
                    "model" : "user",
                    "scope" : $scope,
                    "fields" : [
                        {
                            "type" : "text",
                            "bind" : "username",
                            "label" : "Username",
                            "help-text" : "The username of the new user"
                        },
                        {
                            "type" : "password",
                            "bind" : "password",
                            "label" : "Password",
                            "help-text" : "The password of the new user"
                        },
                        {
                            "type" : "text",
                            "bind" : "realname",
                            "label" : "Realname",
                            "help-text" : "The realname of the user"
                        },
                        {
                            "type" : "select",
                            "bind" : "type",
                            "label" : "Type",
                            "help-text" : "The type of the user",
                            "options" : {
                                "data" : [
                                    { "name" : "Admin", "value" : "ADMIN" },
                                    { "name" : "User", "value" : "USER" }
                                ]
                            }
                        },
                        {
                            "type" : "directive",
                            "bind" : "devices",
                            "label" : "Devices",
                            "options" : { "directive" : "user-device" },
                            "help-text" : "Devices associated with this user"
                        },
                        {
                            "type" : "directive",
                            "bind" : "groups",
                            "label" : "Groups",
                            "options" : { "directive" : "user-group" },
                            "help-text" : "Groups associated with this user"
                        }
                    ],
                    "submit" : function(){
                        $scope.users = User.query();
                    }
                });
            }
        }
    ]
);

/**
 * This controller handles a single user.
 * View is embedded inside a modal so this
 * controller handles its closing etc aswell.
*/
UserControllers.controller(
    'UserController',
    [
        '$scope',
        '$modalInstance',
        'User',
        'userId',
        function($scope, $modalInstance, User, userId) {

            /*If a userid is present then get our*/
            if( userId ) {
                $scope.user = User.get( { id: userId } );
            } else {
                $scope.user = {};
            }

            /*This function is used to save a user*/
            $scope.save = function( user ) {
                User.save( user ).error(
                    function( json ) {
                        $scope.errors = json.errors;
                    }
                ).success(
                    function() { $modalInstance.close( true ); }
                );
            }

            /*This function is used to create a new user*/
            $scope.create = function( user ) {
                User.create( user ).error(
                    function( json ) {
                        $scope.errors = json.errors;
                    }
                ).success(
                    function() { $modalInstance.close( true ); }
                );
            }

            /*This function is used to close the modal*/
            $scope.close = function() {
                $modalInstance.close( false );
            };
        }
    ]
);