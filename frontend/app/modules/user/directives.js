var UserDirectives = angular.module( 'UserDirectives', [ 'DeviceServices' ]);

UserDirectives.directive( 'userDevice', [ 'Device', function( Device ) {
  return {
    templateUrl: 'modules/user/views/userDevice.html',
    link: function ($scope, element, attrs) {
        $scope.allDevices = Device.query();

        /*This function is used to remove a device from the user*/
        $scope.removeDevice = function ( deviceObj ) {
            $scope.user.devices = $scope.user.devices.filter(
                function( device ){
                    return !(device.id==deviceObj.id);
                }
            );
        }

        /*This function is used to add a device to the user*/
        $scope.addDevice = function ( device ) {
            if( $scope.user.devices != undefined ){
                $scope.user.devices.push( device );
            } else {
                $scope.user.devices = [];
                $scope.user.devices.push( device );
            }
        }
    }
  };
}]);

UserDirectives.directive( 'userGroup', [ 'Group', function( Group ) {
  return {
    templateUrl: 'modules/user/views/userGroup.html',
    link: function ($scope, element, attrs) {
        $scope.allGroups = Group.query();

        /*This function is used to remove a group from the user*/
        $scope.removeGroup = function ( groupObj ) {
            $scope.user.groups = $scope.user.groups.filter(
                function( group ){
                    return !(group.id==groupObj.id);
                }
            );
        }

        /*This function is used to add a group to the user*/
        $scope.addGroup = function ( group ) {
            if( $scope.user.groups != undefined ) {
                $scope.user.groups.push( group );
            } else {
                $scope.user.groups = [];
                $scope.user.groups.push( group );
            }
        }
    }
  };
}]);