'use strict';

var UserServices = angular.module( 'UserServices', ['ngResource']);

UserServices.factory(
    'User',
    [
        '$resource',
        '$http',
        function($resource, $http){
            var obj =  $resource(
                '/api/users/:id.json',
                {},
                {
                    query: { method:'GET', params:{ id: 'index' }, isArray:true }
                }
            );

            /*This function is used to delete a user*/
            obj.delete = function(id) {
                return $http.delete( '/api/users/'+ id +'.json' );
            }

            /*This function is used to save an existing user*/
            obj.save = function( user ) {
                return $http.put( '/api/users/'+ user.id +'.json', user );
            }

            /*This function is used to create a new user*/
            obj.create = function( user ) {
                return $http.post( '/api/users/index.json', user );
            }

            return obj;
        }
    ]
);