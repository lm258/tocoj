\chapter{Background}
\definecolor{light-gray}{gray}{0.95}
\lstset{
    backgroundcolor=\color{light-gray},
    basicstyle=\scriptsize,
    frame=single,
    keywordstyle=\color{blue},
    showstringspaces=false
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%            DAAP Section           % 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\sectionl{DAAP}
DAAP (Digital Audio Access Protocol) is a proprietary specification by Apple Inc for iTunes.
iTunes is a media management software tool that can be used for media sharing.
DAAP is used in conjunction with ZeroConf for network service discovery \cite{rumsey_future_2008}.
Network service discovery allows devices attached to a local network to find other device services.
A network attached printer could have a print job and spooler service.
iTunes media sharing and DAAP after version 10 are only compatible with devices distributed by Apple Inc \cite{_itunes_2010}.\\

A device attached to the local network discovers services by broadcasting a ZeroConf mDNS query.
The mDNS query contains a PTR (Pointer Record) record which specifies the type of service to look for.
The query will return mDNS names for each device that contains that service.
An mDNS name can then be resolved to a network address to begin communication.
Figure-\ref{mdns} shows an example PTR record for a printer.
The printers PTR record holds the service type ``printer'' and the mDNS name ``MySharedPrinter''.
Devices using mDNS to query for the PTR record ``printer'' could then discover the printer.

\begin{figure}[ht]
    \caption{MDNS Search \label{mdns}}
    \lstinputlisting{appendix/daap_pairing.txt}
\end{figure}

Apple Inc has various service types for each device that can be discovered using mDNS.
The service type appletv-itunes is used to discover the Apple Incs iTunes media server.
Using mDNS a device can discover the network location of the iTunes media server.
Devices can then login to the media server to view and stream media.
Login is achieved by sending a pairing code to the URL \textbf{daap://itunes-network-location/login}.
The iTunes media server registers the device using the pairing code and returns a session id.
The session id is used in conjunction with a username and password for all correspondence with the media server.
A URL is a human readable address containing the protocol, location and port for a resource on a network \cite{tim_uniform_1994}.\\

Registered devices can query the media server to find view available media.
A request is sent to the URL \textbf{daap://itunes-network-location/databases} returning the avdb response.
Figure-\ref{avdb} shows the structure of the avdb response returned from iTunes \cite{_andytinycat/daapdocs_????}.
The avdb response contains a list of databases including the name, id and number of items for each database.

\begin{figure}[ht]
    \caption{iTunes avdb Response\label{avdb}}
    \lstinputlisting{appendix/avdb.txt}
\end{figure}

Media items are stored in iTunes databases.
The database-id from Figure-\ref{avdb} can be used to query the media server for media items in each database.
This is achieved by sending a request to the URL \textbf{daap://itunes-network-location/databases/database-id/items}.
Adding the type variable on the request URL can be used to filter media items by type.
\textbf{daap://itunes-network-location/databases/database-id/items?type=music} would return only music items from the database.

\begin{figure}[ht]
    \caption{iTunes Media Item Response\label{apso}}
    \lstinputlisting{appendix/apso.txt}
\end{figure}

The iTunes media server responds with a list of media items as shown in Figure-\ref{apso}.
Each media item in the list contains a name, type and id.
The media item id is used for streaming a media item from the media server.
Streaming is initiated when a user device requests the URL \textbf{daap://itunes-network-location/databases/database-id/items/item-id.extension} from the media server.

\newpage
The DAAP protocol can be used to provide context driven media sharing.
Users authenticated with the system can be restricted to only media suitable for the user.
An example of this is the parental controls feature of the iTunes media server.
Devices querying the media server can view only media items appropriate for that users age.
iTunes automatically filters media items based on the rating such as \textbf{U}, \textbf{PG}, \textbf{18}.
Children would only be able to access \textbf{U} media items when parental control is enabled.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%            UPnP Section           % 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{UPnP}
UPnP (Universal Plug and Play) is a technology created by the UPnP forum.
The UPnP forum is a collection of multi-national technology companies such as Intel, Cisco, LG and ZTE \cite{_upnp_2015}.
UPnP allows devices to dynamically discover and communicate without configuration \cite{upnp_forum_what_2015}.\\

Discovery allows devices to find available services on the same network.
Services are functionality provided by devices.
A UPnP enabled printer could have a spooler and print job service.\\

UPnP discovery is possible using a protocol called SSDP (Simple Service Discovery Protocol).
SSDP can notify devices of services being created or destroyed.
SSDP NOTIFY is broadcast over the network and would notify devices to the change in services.
Searching for services is achieved by broadcasting an SSDP M-SEARCH.
Devices respond to an M-SEARCH request with UPnP device information.
An SSDP search would be similar to Figure~\ref{ssdp-search}:

\lstdefinelanguage{XML}
{
  morestring=[b]",
  morestring=[s]{>}{<},
  morecomment=[s]{<?}{?>},
  stringstyle=\color{Black},
  identifierstyle=\color{Blue},
  keywordstyle=\color{Cyan},
  morekeywords={xmlns,version,type}% list your attributes here
}
\begin{figure}[ht]
    \caption{SSDP Search \label{ssdp-search}}
    \lstinputlisting{appendix/ssdp.txt}
\end{figure}

Devices that receive the M-SEARCH would respond as in Figure~\ref{ssdp-response}.

\begin{figure}[ht]
    \caption{SSDP Response \label{ssdp-response}}
    \lstinputlisting{appendix/ssdp_response.txt}
\end{figure}

\clearpage
In the UPnP specification each device is described using an XML document.
Using the ``LOCATION'' field in the SSDP response, an XML document describing a device can be retrieved.
The XML document is used by the client device as instructions for communication.\\

An example XML document in Figure~\ref{binary-light} describes a binary light device.

\begin{figure}[ht]
    \caption{UPnP Binary Light \label{binary-light}}
    \lstinputlisting[language=Xml]{appendix/binary_light.xml}
\end{figure}

DeviceType tags show the UPnP device type; in this case BinaryLight v1 (``urn:schemas-upnp-org:device:BinaryLight:1'').
Additional information such as the model name, friendly name and manufacturer are typically set per product.
UDN (Unique Device Name) is a unique persistent name per device.
If multiple BinaryLights were available each would have a unique UDN.\\

ServiceList is used to enumerate each service the device provides.
Services have a service type, service id, SCPDURL, control and event URLs.
The SCPDURL is a URL that is used to fetch the XML document containing more information about that service.\\

Event and Control URLs are used as handles for communication and control of the device.
ControlPoints are UPnP devices that are used to control other UPnP devices.
For example a ControlPoint controlling the BinaryLight would post an XML document to the control URL.
The BinaryLight would then parse the incoming XML document and respond accordingly (Turn light on).\\
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                 END               % 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     UPnP MediaRender Section      % 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newpage
UPnP MediaRender and MediaServer are device specifications used for media discovery and streaming.
MediaRenderer devices are used for playback and can be considered client devices.
MediaServers are used for media hosts and can be considered server devices.

\subsection{MediaRenderer}
Devices such as smart phones using a ControlPoint can push content to a MediaRenderer for playback.
A MediaRenderer device could be a smart TV or home entertainment system.
Similar to the BinaryLight MediaRender and MediaServer devices are described using XML.
Below is an example of a MediaRenderers service list:

\begin{figure}[ht]
\caption{MediaRenderer Service List \label{media-renderer}}
\lstinputlisting[language=Xml]{appendix/media_renderer_sub.xml}
\end{figure}

Each service is responsible for a different aspect of functionality a MediaRenderer device provides.
RenderingControl is responsible for playback settings such as volume, contrast, sharpness, brightness and mute.
AVTransport controls the current media item and can be used to fetch the duration, current position or play a new item.
The ConnectionManager service is used for capability matching and connection status between devices.\\

%%% MediaServer Part %%%
\newpage
\subsection{MediaServer}
MediaServers are used for returning the list of available media to ControlPoint devices.
The MediaServer specification only handles the list of media and not the streaming.
Streaming is handled separately using a HTTP server.
Figure~\ref{media-server} shows the  XML description for MediaServer services.
Similar to the MediaRenderer the ConnectionManager service is used for capability matching and connection status between devices.
ContentDirectory provides functionality for searching and browsing the MediaServers available media.

\begin{figure}[ht]
\caption{MediaServer Service List \label{media-server}}
\lstinputlisting[language=Xml]{appendix/mediaserver.xml}
\end{figure}

Devices querying the MediaServer can search or browse a tree of media items.
The MediaServer responds with an XML tree which can be used to get the file location for each media item.
The XML tag \textit{\textless{res}\textgreater} holds the file location for each media item.
A response tree from the MediaServer would be formatted similar to Figure~\ref{media-server-response}.

\begin{figure}[ht]
\caption{MediaServer Server Response \label{media-server-response}}
\lstinputlisting[language=Xml]{appendix/mediaserver_response.xml}
\end{figure}

\subsection{Media Sharing and Security Issues}
Media sharing in UPnP uses both the MediaServer and MediaRenderer devices.
A user device would typically be both a UPnP MediaRenderer and UPnP ControlPoint.
UPnP ControlPoints are used for interacting with device services.
Figure~\ref{media-sharing} is a sequence diagram showing how each device is used in a typical media sharing scenario.
\begin{figure}[ht]
    \caption{MediaServer \& MediaRenderer sharing \label{media-sharing}}
    \center \includegraphics[width=0.65\textwidth]{img/figure5.eps}
\end{figure}

Below is a breif summary of each step to help match \textbf{Figure-\ref{media-sharing}}
\begin{itemize}
    \item User device ControlPoint asks MediaServer for media items
    \item MediaServer responds with available media list
    \item User device gets file location from \textit{\textless{res}\textgreater} tag of item in returned list
    \item User ControlPoint instructs user MediaRenderer to play file
    \item HTTP server will respond with the file contents(Video, Image, Sound)
    \item User device will now be playing media item
\end{itemize}

The MediaRenderer and MediaServer devices do not offer any authorization or user handling.
Different users' using MediaRenderer and MediaServer for media sharing would be handled the same.
This would typically mean every device connected to the MediaServer would have access to all media.
This security flaw is evident across most UPnP devices \cite{mccune_trust_2011}.\\

Extensions to UPnP have been proposed to solve this security flaw.
Two of these technologies will be discussed; User Profile in \textbf{Section-\ref{sec:UPnP User Profiling}} and DeviceProtection in \textbf{Section-\ref{sec:UPnP DeviceProtection}}.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                 END               % 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newpage
\sectionl{UPnP User Profiling}
UPnP user profile (\textit{UpnP-UP}) is an extension of the base UPnP protocol adding user authentication \cite{sales_upnp_2010}.
User authentication allows each user of the system to have an associated user name and password for login.
UPnP-UP introduces the UPServer device with authentication and profile services for managing user sessions.
UPnP-UP can be used in conjunction with the MediaServer and MediaRenderer devices to provide context driven media sharing.\\

A device is required to authenticate with the UPServer.
Once authentication has been successful the connecting device receives an authentication key.
The authentication key can be used for retrieving profile information, authentication and terminating a user session.
Profile information for a user could be associated media, favourite room temperature or any other user specific information.\\

UPnP-UP requires all devices involved in user interactions to implement the UPnP-UP authentication services.
Client devices and MediaServers would both be required to use UPnP-UP.
A user device could have unrestricted access if the device they were using was not UPnP-UP compatible.
This is the default behaviour of the UPnP-UP specification as to remain compatible with existing systems.\\

Figure~\ref{upnp-up-media-sharing} shows how UPnP-UP could be used to provide context driven media sharing.
\begin{figure}[ht]
    \caption{UPnP-UP Media Sharing \label{upnp-up-media-sharing}}
    \center \includegraphics[width=0.8\textwidth]{img/figure6.eps}
\end{figure}

\newpage
\sectionl{UPnP DeviceProtection}
UPnP DeviceProtection is a security device specification which was an official addition to UPnP in 2011. 
DeviceProtection was developed by the UPnP forum to directly address security flaws in UPnP \cite{vic_deviceprotection:1_2011}.
DeviceProtection similar to UPnP-UP provides user handling, authentication and user access control.\\

Users' provide a user name and password to the DeviceProtection service for login.
After a successful login the DeviceProtection device notifies all connected devices of the authenticated user.
Devices notified of an authenticated user will allow that user access to its services.
An administrator is able to control which devices the user will automatically be authenticated with after login.
This requires the administrator to configure each device using DeviceProtection.\\

Figure~\ref{upnp-device-protection} shows the distributed nature of DeviceProtection with multiple devices.
Each device in the diagram would be using the DeviceProtection specification.
\begin{figure}[ht]
    \caption{UPnP-UP DeviceProtection \label{upnp-device-protection}}
    \center \includegraphics[width=0.6\textwidth]{img/figure14.eps}
\end{figure}


Roles are used to differentiate between the level of access each user should have.
Default roles for DeviceProtection are public, basic and admin.
Vendors can however specify additional roles.
A MediaServer could have a role per content rating such as: U, PG, 12, 18.
Users' would then be assigned roles by age thus only having access to media their roles permit.\\