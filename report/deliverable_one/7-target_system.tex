\chapter{Implemented System}

Toco Media System is an implementation of the UPnP-DP specification proposed in \textbf{Section-\ref{sec:UPnP Device Profiling}}.
The system will be a demonstration of a UPnP-DP context driven media system.
An administrative web interface will be added to the system for configuration of devices and media.

\section{General Requirements}
Toco Media System has two different roles: \textit{Administrator}, \textit{User}.
Users' are considered media consumers and can only view media on the system.
Administrators are a superset of users thus can do everything a user can do plus more.
Extra privileges for an administrator include configuring the system and controlling user access to media.
Administrators configure the system through an administrative web interface.
Users' can only view media which an administrator has granted them access to.
Media access is determined by media groups.
Users and media items are assigned to media groups.
Below is a list of general requirements for each role:

\subsection{Administrator}
\begin{itemize}
    \item Add/edit/delete users and administrators
    \item Add/edit/delete MediaRenderer devices
    \item Add/edit/delete media groups
    \item Add/edit/delete media
    \item Assign users to media groups
    \item Assign MediaRender devices to users
    \item Assign media to media groups
\end{itemize}

\subsection{User}
\begin{itemize}
    \item Browse media items on MediaServer
    \item Stream media from the MediaServer to user's MediaRenderer device
\end{itemize}

\section{System Overview}
Toco Media System consists of two major components to form the overall system.
The Web Server component is used for streaming files and handling the administrative web interface.
The UPnP MediaServer component handles UPnP actions such as browsing media associated to users.
Each component operates independently only sharing information through a common database.

\begin{figure}[ht]
	\center \includegraphics[width=0.7\textwidth]{img/figure2.eps}
\end{figure}

The database is used to store user information and media known to Toco Media System.
User information stored in the database includes each users real name, username, password, devices and media groups.
Real names are stored so administrators can easily identify each user in the system.
Storing user device and media group information is used to associate device access to media.
Using a database allows the system to retain information after shutdown.\\


The Web Server is used for handling the web interface and streaming media files.
MediaRenderers browse Toco Media Systems MediaServer component for media items.
Each media item contains a web URL pointing to a media file on the web server.
The MediaRenderer device can then use the web URL to request a media file.
The web server will begin streaming a media file when a MediaRenderer requests a file.\\

Media files are stored locally on the physical computer Toco Media System is installed on.
Administrators need to \textit{Refresh} Toco Media System to discover new media files.
Newly discovered media is added automatically to the system with no media group.
This prevents users' from having unintended access to new media on the system.

\newpage
\section{Security Requirements}
Toco Media System is required to be secure and honor user access to media.
MediaRenderer devices browsing the system can only access assigned media.
Children assigned child content groups can only view content suitable for children.\\
Key security requirements have been listed below:

\begin{itemize}
    \item Media should only be available to users who are associated to it
    \item Unrecognized devices should have access to either no content or guest content
    \item Authentication is required for logging into the web interface of the system
    \item Log access to the web interface and setting changes
    \item New content added to storage device of Toco will have no group when first discovered
    \item Log access to content and MediaRenderer connections
    \item Notify administrator of sensitive content access or setting changes
\end{itemize}

\section{Security Overview}

\begin{figure}[ht]
    \caption{Media Group Overview \label{user-group-device-ovr}}
    \center \includegraphics[width=0.45\textwidth]{img/figure15.eps}
\end{figure}

Figure-\ref{user-group-device-ovr} shows how media groups form the basis of media access control in Toco Media System.
Users in the system contain a list of devices.
Media groups contain lists of both users and media items.
User devices querying the system can only view media items contained in the users media groups.
Media groups allow for both refined and bulk media access control.
Adults can create groups for each content rating such as \textbf{U}, \textbf{PG} and \textbf{18}.
Users can then be assigned these media groups to gain access.
However refined groups can also be created for example the group ``Ben \& Holly's Little Kingdom'' for a particular show.

Figure-\ref{security-overview} shows an overview of Toco Media Systems security methods.
The UPnP Interface identifies a device using the devices MAC address.
Assigning a MAC address to a user allows the system to restrict a device to that users media.
MediaRenderer devices requesting a file from the web server are also identified using the MAC address.
This allows the web server to also restrict media file requests.

\begin{figure}[ht]
    \caption{Security Overview \label{security-overview}}
    \center \includegraphics[width=0.8\textwidth]{img/figure9.eps}
\end{figure}

The web interface requires a username and password for authentication.
Users can only access the web interface if they are assigned the ``Administrator'' role.
Roles are used to prevent ordinary users from changing access to media.
Using roles allows a parent to create a child user who has read only access to the system.
Parents who have the administrator role can add new users, media and devices.\\

Logs and notifications are used for tracking access to content and the web interface.
Log entries are created for UPnP queries, file requests and web interface access.
Username, date, time, MAC address and the request are contained in the log web interface entry.
MAC address, date, time and the request are stored in the UPnP and file request log entry.
Log entries allow administrators to view historic access to the system and identify security breaches.\\

Notifications are a suggested feature of Toco Media System.
Administrators could enable notifications for settings or sensitive media access.
Users accessing the sensitive media would trigger a notification.
Settings that have notifications enabled when changed would also trigger a notification.
A notification would typically involve sending an email to administrators.\\

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%% User Interface %%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{User Interface}
The user interface for Toco Media System is an interactive web application.
Designing the user interface as a web application allows Toco Media System to be configured remotely.
Essentially administrators can configure the system from any connected device.
Responsive web design techniques allow the web interface to remain fully compatible with mobile devices.\\

\begin{figure}[ht]
    \caption{Web Interface Dashboard\label{dashboard}}
    \center \includegraphics[width=0.6\textwidth]{appendix/user_interface/main-screen.png}
\end{figure}

Figure-\ref{dashboard} shows the web interface dashboard of Toco Media System.
The interface is divided into subcomponents each handling a specific piece of configuration.
Subcomponents for manipulating Users, Devices, Groups and media can be accessed by clicking the dashboard icons.
The users subcomponent is shown in Figure-\ref{ang-users}.
In the users subcomponent new users can be created or existing ones modified and removed.
The red trash button beneath each user is used remove that user.
The grey edit icon can be used to edit a user.

\begin{figure}[ht]
    \caption{Web Interface Users\label{ang-users}}
    \center \includegraphics[width=0.6\textwidth]{appendix/user_interface/view-users.png}
\end{figure}

Figure-\ref{ang-edit} shows the dialog form for editing a device.
Dialog forms existing for manipulating Users, Devices, Groups and media items.
Clicking the ``Add User'' button is used to open an empty User dialog form.
Each subcomponent contains an ``Add'' button.
For example the Group subcomponent contains a ``Add Group'' button.\\

Dialog forms contain an input field for each property that can be configured.
Saving the dialog form will create the new User, Device or Group in the system.
This allows Users, Devices. Groups and media to be created through the web interface.

\begin{figure}[ht]
    \caption{Web Interface Edit Device\label{ang-edit}}
    \center \includegraphics[width=0.6\textwidth]{appendix/user_interface/edit-device.png}
\end{figure}

Editing Users, Devices, Groups and media is also achieved using dialog forms.
Editing a User would show the Edit User dialog form.
Each input field in the form would be populated with the existing Users properties.
Saving the dialog form would then update the Users details in the system.\\

Validation errors are used to alert of incorrect input.
Invalid details such as a malformed MAC address show a validation error.
Validation errors are displayed under the invalid input field in red.
All data is validated by the system before being saved into the database.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%% User Interface %%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%
% This section describes the user interface with included
% interface diagrams. First subsection is talking about
% the user management interface.
%
\newpage
\section{Technologies Used}

Different technologies and tools were used during the development of Toco Media System.
Using frameworks and existing UPnP libraries allowed for quicker development and UPnP conformity.
Each technology used is listed below:

\subsection{Git}
Git is a distributed version control system.
Commits allow iterations of work to be stored in a Git repository.
Git can then be used to show the changes between commits and revert to earlier versions.
Using Git allowed the source code and report of the project to be controlled and managed.
A remote version of the repository was stored on the Git provider ``BitBucket''.
BitBucket provides a graphical user interface for visual management of repositories.

\subsection{Sormula}
Sormula is a data persistence library for Java \cite{_jeff43017_????}.
Data persistence allows objects to be stored and persist after program execution.
Sormula automatically maps Java objects to tables in a database.
The User object would be mapped to the users table.
Using Sormula was beneficial as database queries were automatically generated.
Automatic database queries made changing, fetching and creating objects simple.
Updating the properties of an object using Sormula does not require the rewriting of database queries.
Sormula is database independent so the database provider could also be changed without changing the code.

\begin{figure}[ht]
    \caption{Sormula Data Persistence\label{sormula}}
    \center \includegraphics[width=0.5\textwidth]{img/figure19.eps}
\end{figure}

Figure-\ref{sormula} shows how Sormula was integrated into the system.
Repository classes were created for each object type.
Repositories can be passed objects for saving or queried to return objects.
For example the UserRepository is used for saving/retrieving User objects.
Service classes were used to provide a further level of abstraction to database interaction.
Service classes extend repository classes providing validation of models.
A User model with an invalid username would throw a ValidationException when handled using the UserSerivce.


\clearpage
\subsection{Cling}
Cling is a UPnP Java library \cite{_4thline/cling_2015}.
Cling fully conforms to the UPnP standard and provides classes for each UPnP device and service.
The MediaServer, ContentDirectory and ConnectionManager Cling classes were used in the systems UPnP component.
The MediaServer class provides a model for the UPnP MediaServer device.
The ContentDirectory and ConnectionManager classes were passed to the MediaServer class to use as services.\\

ConnectionManager is a service used for managing connections to the MediaServer.
ContentDirectory is a service used for managing queries for media items in the MediaServer.
Client devices querying the MediaServer for media items are handled using the ContentDirectory class browse function.

\begin{figure}[ht]
    \caption{ContentDirectory Service Class Overview \label{user-group-device}}
    \center \includegraphics[width=0.75\textwidth]{img/figure16.eps}
\end{figure}

Figure-\ref{user-group-device} shows the class hierarchy used for the ContentDirectory service.
The Cling AbstractContentDirectory class was extended to override the browse function.
The extended browse function uses UPnP-DP MAC detection to detect the device making the browse request.
Groups for the detected device are then retreived from the database and passed to MediaContainers.\\

MediaContainers were created for each content type such as Video, Music and Pictures.
A device browsing the ``Music'' folder would be handled using the music MediaContainer.
The MediaContainer class uses the DatabaseRepository class to fetch media associations from the database.
Media associations are retreived using the detected device groups.
Thus only media that has been assigned to the detected device is returned.
Only the ContentDirectory class was extened, the MediaServer and ConnectionManager classes were not changed.

\clearpage
\subsection{RESTExpress}
RESTExpress is a micro web services framework for Java \cite{_restexpress/restexpress_????}.
RESTExpress is used as the web framework for the systems web server component.
Classes for modelling a web server and handling user requests are provided in RESTExpress.
The web server class is used for receiving MediaRenderer streaming and web interface requests.\\

\begin{figure}[ht]
    \caption{RESTExpress Web Server\label{restexpress}}
    \center \includegraphics[width=0.7\textwidth]{img/figure18.eps}
\end{figure}

Figure-\ref{restexpress} shows a general overview of the web server component.
Controllers are used to fetch, update, create and delete each model.
Controllers for each model such as Groups, Devices and Media were created.
Requests to the web server class are mapped to specific controller classes using the request URL.
Web server requests with the URL \textbf{``http://webserver/users/*''} would be mapped to the UserController.\\

Sending a DELETE web request to the URL \textbf{``http://webserver/users/userid''} would delete the user with id userid.
Similarly a GET web request to the URL \textbf{``http://webserver/users/userid''} would fetch the user with id userid.
A GET web request to the URL \textbf{``http://webserver/users''} returns a list of all users each with a user id.
Each controller follows the same paradigm.
For example a GET web request to the URL \textbf{``http://webserver/devices''} would return all devices.\\

Controller classes use the Sormula based service classes for database interaction.
The service class was used for validating, deleting, fetching, updating and creating models in the database.
Each controller used a service class; for example the UserController used the UserService.
A GET web request to \textbf{``http://webserver/users/userid''} would result in the UserController querying the UserService for a user with id userid.
The User object returned from the UserService is then sent as a web response.
Invalid user data sent to the UserController would cause the UserService to throw a ValidationException.
The ValidationException is used to return a web response to the user alerting them to the invalid input.



\clearpage
\subsection{AngularJS \& Bootstrap-UI}
AngularJS is a JavaScript framework for web applications.
Modules can be added to the framework to provide extra functionality.
Bootstrap-UI is an angular user interface module.
Bootstrap-UI provides various web components such as time pickers, buttons and forms.
AngularJS and Bootstrap-UI are used to provide a robust web framework for the administrator interface.\\

Each model used in the web interface such as Users, Groups and Devices are mapped to angular services.
Using angular services allows for a seamless connection between JavaScript objects and persistent Java objects.\\

Figure-\ref{angular} is on overview of the connection between angular service objects and persistent Java objects.
An angular service ModelService would make web queries to the URL ``http://toco/model/''.
Each query would then be handled by the web components ModelController.
The web component ModelController can create, read, update and delete Models in the database.
Essentially this gives the AngularJS ModelService control of database Model objects from the web interface.\\

\begin{figure}[ht]
    \caption{Overview of web interface\label{angular}}
    \center \includegraphics[width=1.05\textwidth]{img/figure20.eps}
\end{figure}

AngularJS controllers and Bootstrap-UI forms provide user interface handling.
AngularJS controllers are used for providing Boostrap-UI dialog boxes to the user.
Dialog boxes can be used for manipulating and viewing models.
An edit model dialog box could contain a text field for the model name.
A user can then change the name and save the dialog box.
Saving the dialog box causes the AngularJS ModelController to save the model using the AngularJS ModelService.
The model is then sent to web server and handled using the web components ModelController.
The web component would then update the model in the database and return an appropriate response.

\newpage
\subsectionl{UPnP Inspector}
UPnP Inspector is a UPnP debug utility allowing UPnP devices to be discovered and explored.
UPnP devices available on a local network are shown in the UPnP Inspector tool.
Figure-\ref{upnp-inspector} is the UPnP Inspector window displaying discovered UPnP devices.

\begin{figure}[ht]
    \caption{UPnP Inspector Discover Devices\label{upnp-inspector}}
    \center \includegraphics[width=0.45\textwidth]{appendix/upnp-inspector.png}
\end{figure}

Each device can be be manipulated and viewed within the UPnP Inspector window.
UPnP Inspector was used to browse Toco Media Systems UPnP component.
Figure-\ref{upnp-inspector2} shows UPnP Inspector after browsing Toco Media System.
Browsing the system allows UPnP Inspector to view each media item.
This functionality was used to debug media item retrieval from the database and also for testing.
The UPnP component browsing results were compared to that of the Plex MediaServer.
Comparing the output of each MediaServer was used to confirm that Toco Media System was responding correctly to UPnP requests.

\begin{figure}[ht]
    \caption{UPnP Inspector Browse Toco Media Server\label{upnp-inspector2}}
    \center \includegraphics[width=0.45\textwidth]{appendix/upnp-inspector2.png}
\end{figure}