\chapterl{Testing Strategy}
\section{Strategies}
Different types of testing strategies have been used throughout the development process of the Toco Media System.
Each testing strategy covered different levels of the system such as the UPnP interface, web component Controllers and user interface.
Testing the implementation of the designed specification was used to highlight possible issues, weaknesses and re-assure correctness.

\section{Unit Testing}
Unit testing was used to automatically test the core functionality of the system.
JUnit for Java was the library used to provide the automatic unit testing.
Each piece of functionality tested using unit tests are listed below:

\begin{itemize}
    \item Validating/Adding/Editing/Deleting users
    \item Validating/Adding/Editing/Deleting content
    \item Validating/Adding/Editing/Deleting devices
    \item Assigning users devices
    \item Validating/Adding/Editing/Deleting content groups
    \item Assigning users to content groups
    \item Assigning content to content groups
    \item Testing visibility of content after having been assigned to groups
\end{itemize}

Unit tests were created for administrative functionality only.
UPnP unit tests were not created, however this part of the system was verified during integration testing.

\section{Integration Testing}
Integration testing was used to verify the UPnP interface of the system.
A utility called ``UPnP Inspector'' (Section-\ref{sec:UPnP Inspector}) allows the inspection of UPnP responses.
Each response is checked to make sure that the correct media is displayed for different queries.

\begin{description}
    \itemld{INTEGRATION-TEST-1}{Correct user media list}{
        Users should only have access to media they are assigned to.
        UserA is created and assigned GroupA and DeviceA.
        GroupA contains \textit{PictureA}, \textit{MovieA}, \textit{SongA}.
        UPnP Inspector will browse the system acting as DeviceA.
        Other assets \textit{PictureB}, \textit{MovieB}, \textit{SongB} are on the system but not assigned to UserA.
        DeviceA should only have access to \textit{PictureA}, \textit{MovieA}, \textit{SongA}.
    }
    \itemld{INTEGRATION-TEST-2}{Correct guest media list}{
        Any device querying the system that is not assigned a user is treated as Guest.
        A content group GroupG is assigned to Guest.
        GroupG contains \textit{PictureG}, \textit{MovieG}, \textit{SongG}.
        Other assets \textit{PictureB}, \textit{MovieB}, \textit{SongB} are on the system but not assigned to GroupG.
        UPnP Inspector will browse the system acting as an unknown device.
        UPnP Inspector should only have access to \textit{PictureG}, \textit{MovieG}, \textit{SongG}.
    }
    \itemld{INTEGRATION-TEST-3}{Media playback}{
        A DeviceA with Kodi media system installed will act as a MediaRenderer.
        DeviceA will browse Toco Media System to retrieve a list of media.
        Each media item should play back correctly through the MediaRenderer device.
    }
    \itemld{INTEGRATION-TEST-4}{Media list updating}{
        Changing users access to media should be reflected through the Toco Media System.
        UserX is assigned GroupA and the UPnP Inspector device.
        GroupA contains the items \textit{PictureA}, \textit{MovieA}, \textit{SongA}.
        GroupB contains the items \textit{PictureB}, \textit{MovieB}, \textit{SongB}.
        UPnP Inspector should only have access to media in GroupA.
        UserX then has GroupA changed to GroupB.
        After this change the UPnP Inspector device will now only have access to media in GroupB.\\
    }
\end{description}
\newpage

\section{Scenario Testing}
Scenarios were created to test different use cases and possible security loop holes in the system. 

\scenario{SCENARIO-TEST-1}{Cross device playback security}{
    UPnP MediaRenderer devices can be instructed to play media items by other devices.
    The media items can be stored locally or on a UPnP MediaServer.
    This feature of UPnP may allow a parent to instruct a child's device to play adult media.
    The purpose of this scenario is to verify the system will consider this possibility and deny playback.

    \begin{figure}[ht]
    \caption{\label{scenario-1}}
    \center \includegraphics[width=0.5\textwidth]{img/figure11.eps}
    \end{figure}

    Equipment:
    \begin{itemize}
        \item Laptop with Toco Media System installed, adult and child content
        \item MediaRenderer device as child
        \item UPnP Inspector device as adult
    \end{itemize}

    The following steps are then executed:
    \begin{enumerate}
        \item UPnP Inspector sends browse request to the MediaServer
        \item MediaServer responds with adult and child media
        \item UPnP Inspector instructs MediaRenderer device to play child media
        \item Child content should play successfully on the MediaRenderer device
        \item UPnP Inspector instructs MediaRenderer device to play adult media
        \item MediaRenderer device should fail to play media
    \end{enumerate}
}

\scenario{SCENARIO-TEST-2}{Playing media from cached listings}{
    UPnP MediaRenderer devices can cache media listings from the MediaServer.
    Essentially this means a MediaRender device could have an out of date media list.
    This would create a scenario where updates to media might not have an immediate effect.
    Therefore a child could have access to media items a parent has actually unassigned from them.
    \begin{figure}[ht]
    \caption{\label{scenario-1}}
    \center \includegraphics[width=0.5\textwidth]{img/figure12.eps}
    \end{figure}

    Equipment:
    \begin{itemize}
        \item Laptop with Toco Media System installed and media items
        \item MediaRenderer device as child
    \end{itemize}

    The following steps are then executed:
    \begin{enumerate}
        \item MediaRenderer device queries MediaServer for media list
        \item Item list is now visible on MediaRenderer
        \item MediaServer media list is updated removing child access
        \item MediaRenderer device is not refreshed and plays cached item
        \item Cached item will not play
    \end{enumerate}
}

\chapter{Testing Results}

Results were captured for each testing strategy in Chapter-\ref{ch:Testing Strategy}.
Each set of results are presented using screenshots and report files.
This chapter aims to show the Toco Media System and underlying UPnP-DP specification are functionally correct.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%          Unit Test          %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Unit Testing}

JUnit is a unit testing framework for Java.
``Ant'' is a Java build automation system.
JUnit in conjunction with ``Ant'' was used for unit test automation during Toco Media System development.
Using ``Ant'' allowed execution of all unit tests with the Linux command ``ant test''.
``ant test'' generates text reports detailing the execution (pass/failure) of tests.\\

Unit tests are grouped together into Java classes.
Each test class contains functions that are executed as a single unit test.
The JUnit annotation ``@Test'' was used to tell JUnit which functions are unit tests.
Figure-\ref{junit-simple-test} shows the validateName unit test.
\begin{figure}[ht]
    \caption{JUnit validateName Test \label{junit-simple-test}}
    \lstinputlisting[language=Java]{appendix/test/sample_unit_test.txt}
\end{figure}

The validateName test is used to check the validation of group names.
In this particular instance an invalid group name was passed to the GroupController.
This test is expected to fail with the exception ``ValidationException''.\\

Each unit test in all tests follow a similar paradigm to the validateName test.
Valid and invalid data is generated and sent to each controller.
The controllers actual output is then checked against the expected output.
If actual output differed from the expected output the unit test would fail.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%          Unit Test          %%
%%           Results           %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newpage
Figure-\ref{junit-test-output} shows the output after issuing ``ant test'' in a linux terminal.
``Tests run'' show the amount of unit tests executed for each test class.
No errors/failures were encountered (Unit testing passed).
\begin{figure}[ht]
    \caption{JUnit Test Output \label{junit-test-output}}
    \lstinputlisting{appendix/test/junit-output.txt}
\end{figure}

JUnit produces a textual report file for each test class.
The report file contains a list containing each unit test and its result.
Figures 7.3 to 7.8 are the report file produced by JUnit.
Each report is listed to show the executed unit tests.
No errors/failures were encountered (Unit testing passed).

\begin{figure}[ht]
    \caption{GroupController Test Output \label{junit-groupcontroller-output}}
    \lstinputlisting{appendix/test/TEST-org.toco.controller.GroupControllerTest.txt}
\end{figure}

\begin{figure}[ht]
    \caption{DeviceController Test Output \label{junit-devicecontroller-output}}
    \lstinputlisting{appendix/test/TEST-org.toco.controller.DeviceControllerTest.txt}
\end{figure}

\begin{figure}[ht]
    \caption{UserController Test Output \label{junit-usercontroller-output}}
    \lstinputlisting{appendix/test/TEST-org.toco.controller.UserControllerTest.txt}
\end{figure}

\begin{figure}[ht]
    \caption{MusicContentController Test Output \label{junit-musiccontroller-output}}
    \lstinputlisting{appendix/test/TEST-org.toco.controller.MusicContentControllerTest.txt}
\end{figure}

\begin{figure}[ht]
    \caption{MovieContentController Test Output \label{junit-moviecontroller-output}}
    \lstinputlisting{appendix/test/TEST-org.toco.controller.MovieContentControllerTest.txt}
\end{figure}

\begin{figure}[ht]
    \caption{PictureContentController Test Output \label{junit-picturecontroller-output}}
    \lstinputlisting{appendix/test/TEST-org.toco.controller.PictureContentControllerTest.txt}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%     Integration Test        %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\clearpage
\section{Integration Testing}

\scenario{INTEGRATION-TEST-1}{Correct user media list}{
    Users should only have access to media they are assigned to.
    UserA is created and assigned GroupA and DeviceA.
    GroupA contains \textit{PictureA}, \textit{MovieA}, \textit{SongA}.
    UPnP Inspector will browse the system acting as DeviceA.
    Other assets \textit{PictureB}, \textit{MovieB}, \textit{SongB} are on the system but not assigned to UserA.
    DeviceA should only have access to \textit{PictureA}, \textit{MovieA}, \textit{SongA}.\\

    Figure-\ref{test1-user-groups-devices} shows the Toco Media System web interface.
    UserA is being assigned to GroupA and the UPnP Inspector DeviceA.
    \begin{figure}[ht]
        \caption{Toco Media System Interface\label{test1-user-groups-devices}}
        \center \includegraphics[width=0.5\textwidth]{appendix/integration/test1/user_groups.png}
    \end{figure}

    Figure-\ref{test1-inspector-result} shows UPnP Inspector browsing the Toco Media System.
    As Expected UPnP Inspector only has access to items in GroupA.
    \begin{figure}[ht]
        \caption{UPnP Inspector \label{test1-inspector-result}}
        \center \includegraphics[width=0.5\textwidth]{appendix/integration/test1/inspector.png}
    \end{figure}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%   INTEGRATION-TEST-2  %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\clearpage
\scenario{INTEGRATION-TEST-2}{Correct guest media list}{
    Any device querying the system that is not assigned a user is treated as Guest.
    A content group GroupG is assigned to Guest.
    GroupG contains \textit{PictureG}, \textit{MovieG}, \textit{SongG}.
    Other assets \textit{PictureB}, \textit{MovieB}, \textit{SongB} are on the system but not assigned to GroupG.
    UPnP Inspector will browse the system acting as an unknown device.
    UPnP Inspector should only have access to \textit{PictureG}, \textit{MovieG}, \textit{SongG}.\\

    Figure-\ref{test2-groups} shows the Toco Media System web interface.
    The guest user is assigned to GroupG.
    \begin{figure}[ht]
        \caption{Toco Media System Interface\label{test2-groups}}
        \center \includegraphics[width=0.5\textwidth]{appendix/integration/test2/groups.png}
    \end{figure}

    Figure-\ref{test2-inspector-result} shows UPnP Inspector browsing the Toco Media System.
    As Expected UPnP Inspector only has access to items in GroupG.
    \begin{figure}[ht]
        \caption{UPnP Inspector \label{test2-inspector-result}}
        \center \includegraphics[width=0.5\textwidth]{appendix/integration/test2/inspector.png}
    \end{figure}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%   INTEGRATION-TEST-3  %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\clearpage
\scenario{INTEGRATION-TEST-3}{Media playback}{
    A DeviceA with Kodi media system installed will act as a MediaRenderer.
    Kodi media system is an open source UPnP compatable MediaRenderer device \cite{kodi}.
    The MediaRenderer DeviceA will browse Toco Media System to retrieve a list of media.
    Each media item should play correctly through the MediaRenderer DeviceA.\\

    Figure-\ref{test3-movie} shows the Kodi MediaRenderer device browsing Toco Media System.
    Only content assigned to the Kodi MediaRenderer device is available.
    \begin{figure}[ht]
        \caption{Video Media Items in Kodi \label{test3-movie}}
        \center \includegraphics[width=0.7\textwidth]{appendix/integration/test3/movie.png}
    \end{figure}

    Figure-\ref{test3-playback} shows the Kodi MediaRenderer device successfully playing the file.
    \begin{figure}[ht]
        \caption{Media playback in Kodi\label{test3-playback}}
        \center \includegraphics[width=0.7\textwidth]{appendix/integration/test3/xbmc-playback.png}
    \end{figure}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%   INTEGRATION-TEST-4  %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\clearpage
\scenario{INTEGRATION-TEST-4}{Media list updating}{
    Changing users access to media should be reflected through the Toco Media System.
    UserX is assigned GroupA and the UPnP Inspector device.
    GroupA contains the items \textit{PictureA}, \textit{MovieA}, \textit{SongA}.
    GroupB contains the items \textit{PictureB}, \textit{MovieB}, \textit{SongB}.
    UPnP Inspector should only have access to media in GroupA.
    UserX then has GroupA changed to GroupB.
    After this change the UPnP Inspector device will now only have access to media in GroupB.\\

    Figure-\ref{test4-groupA} shows the media UPnP Inspector has access to before UserX is assigned GroupB.
    \begin{figure}[ht]
        \caption{UPnP Inspector Media Items\label{test4-groupA}}
        \center \includegraphics[width=0.5\textwidth]{appendix/integration/test4/groupA.png}
    \end{figure}

    Figure-\ref{test4-groupB} shows the media UPnP Inspector has access to after UserX is assigned GroupB.
    \begin{figure}[ht]
        \caption{UPnP Inspector Media Items \label{test4-groupB}}
        \center \includegraphics[width=0.5\textwidth]{appendix/integration/test4/groupB.png}
    \end{figure}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%     SCENERIO-TEST-1   %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Scenario Testing}
\scenario{SCENARIO-TEST-1}{Cross device playback security}{
    UPnP MediaRenderer devices can be instructed to play media items by other devices.
    The media items can be stored locally or on a UPnP MediaServer.
    This feature of UPnP may allow a parent to instruct a child's device to play adult media.
    The purpose of this scenario is to verify the system will consider this possibility and deny playback.

    Figure-\ref{scenario-test1-adult-groups} shows what media items the UPnP Inspector device can access.
    \begin{figure}[ht]
        \caption{UPnP Inspector Adult Device \label{scenario-test1-adult-groups}}
        \center \includegraphics[width=0.4\textwidth]{appendix/scenario/test1/adult_groups.png}
    \end{figure}

    Figure-\ref{scenario-test1-items} shows what media items the childs MediaRenderer device can access.
    \begin{figure}[ht]
        \caption{Kodi MediaRenderer Media Items \label{scenario-test1-items}}
        \center \includegraphics[width=0.55\textwidth]{appendix/scenario/test1/child_groups.png}
    \end{figure}

    Figure-\ref{scenario-test1-log} shows the error log of the child MediaRenderer.
    The MediaRenderer encountered a 403(Access Denied) error when attempting to play restricted adult media.
    \begin{figure}[ht]
        \caption{MediaRenderer Log \label{scenario-test1-log}}
        \lstinputlisting{appendix/scenario/test1/kodi.log}
    \end{figure}
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%     SCENERIO-TEST-2   %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\clearpage
\scenario{SCENARIO-TEST-2}{Playing media from cached listings}{
    UPnP MediaRenderer devices can cache media listings from the MediaServer.
    Essentially this means a MediaRender device could have an out of date media list.
    This would create a scenario where updates to media might not have an immediate effect.
    Therefore a child could have access to media items a parent has actually unassigned from them.\\

    Figure-\ref{scenario-test2-child-group} shows the Toco Media System web interface.
    The User using the Kodi MediaRenderer device is now only assigned to GroupA.
    \begin{figure}[ht]
        \caption{Toco Media System Edit User\label{scenario-test2-child-group}}
        \center \includegraphics[width=0.7\textwidth]{appendix/scenario/test2/child_group.png}
    \end{figure}

    Figure-\ref{test2-playback-failed} shows the MediaRenderer displaying cached items from GroupA and GroupB.
    Media items from GroupB can no longer be played through the Kodi MediaRenderer as the device is assigned to GroupA.
    \begin{figure}[ht]
        \caption{Kodi Expected Playback Failure \label{test2-playback-failed}}
        \center \includegraphics[width=0.7\textwidth]{appendix/scenario/test2/play_old_item.png}
    \end{figure}
}